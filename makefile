.SILENT:
main:
	flex src/gocompiler.l
	yacc -d -v --debug --verbose src/gocompiler.y
	mv lex.yy.c src
	mv y.tab.c src
	mv y.tab.h src
	gcc -g -Wall -o gocompiler src/*.c
	mkdir -p build
	mv src/lex.yy.c build
	mv src/y.tab.c build
	mv src/y.tab.h build
	cp gocompiler tests
	mv gocompiler build

clean:
	rm gocompiler
	rm src/lex.yy.c
	rm build
