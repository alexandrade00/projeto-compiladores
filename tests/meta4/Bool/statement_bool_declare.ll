@x = common dso_local global i8 0

define dso_local i32 @main() #0 {
  store i8 1, i8* @x
  store i8 0, i8* @x
  ret i32 0
}