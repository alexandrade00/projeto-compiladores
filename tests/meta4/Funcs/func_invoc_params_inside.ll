@x = common global i32 0


define void @fcd(i32, i32) {
  %3 = alloca i32
  %4 = alloca i32
  store i32 %0, i32* %3 
  store i32 %1, i32* %4 
  store i32 1, i32* @x,
  call void @c(i32 2, i32 3)
  ret void
}

declare void @c(i32, i32)
