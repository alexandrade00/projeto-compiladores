@x = common global i32 0

define void @a() {
  ret void
}

define i32 @b() #0 {
  ret i32 1
}

define zeroext i1 @c() #0 {
  ret i1 true
}

define void @d(i32, float) {
  %3 = alloca i32
  %4 = alloca float
  store i32 %0, i32* %3
  store float %1, float* %4
  ret void
}

define i32 @e(i32, float) {
  %3 = alloca i32
  %4 = alloca float
  store i32 %0, i32* %3
  store float %1, float* %4
  ret i32 1
}

define zeroext i1 @f(i32, float) {
  %3 = alloca i32
  %4 = alloca float
  store i32 %0, i32* %3
  store float %1, float* %4
  ret i1 true
}

define void @fcd() {
  store i32 1, i32* @x
  call void @a()
  %1 = call i32 @b()
  %2 = call zeroext i1 @c()
  call void @d(i32 1, float 1.500000e+00)
  %3 = call i32 @e(i32 1, float 1.500000e+00)
  %4 = call zeroext i1 @f(i32 1, float 1.500000e+00)
  ret void
}