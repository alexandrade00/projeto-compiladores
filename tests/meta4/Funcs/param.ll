define void @d(i32, float) {
  %3 = alloca i32
  %4 = alloca float
  store i32 %0, i32* %3
  store float %1, float* %4
  ret void
}

