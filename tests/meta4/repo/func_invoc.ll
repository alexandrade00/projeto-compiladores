; ModuleID = 'tests/meta4/Funcs/func_invoc.c'
source_filename = "tests/meta4/Funcs/func_invoc.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@x = common dso_local global i32 0, align 4

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @a() #0 {
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @b() #0 {
  ret i32 1
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local zeroext i1 @c() #0 {
  ret i1 true
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @d(i32, float) #0 {
  %3 = alloca i32, align 4
  %4 = alloca float, align 4
  store i32 %0, i32* %3, align 4
  store float %1, float* %4, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @e(i32, float) #0 {
  %3 = alloca i32, align 4
  %4 = alloca float, align 4
  store i32 %0, i32* %3, align 4
  store float %1, float* %4, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local zeroext i1 @f(i32, float) #0 {
  %3 = alloca i32, align 4
  %4 = alloca float, align 4
  store i32 %0, i32* %3, align 4
  store float %1, float* %4, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @fcd() #0 {
  store i32 1, i32* @x, align 4
  call void @a()
  %1 = call i32 @b()
  %2 = call zeroext i1 @c()
  call void @d(i32 1, float 1.500000e+00)
  %3 = call i32 @e(i32 1, float 1.500000e+00)
  %4 = call zeroext i1 @f(i32 1, float 1.500000e+00)
  ret void
}

attributes #0 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
