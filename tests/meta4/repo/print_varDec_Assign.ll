@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [8 x i8] c"%0.8lf\0A\00"
@print_strings = internal constant [4 x i8] c"%s\0A\00"
@print_bool_true = internal constant [6 x i8] c"true\0A\00"
@print_bool_false = internal constant [7 x i8] c"false\0A\00"
@.str.1 = internal constant [32 x i8] c"Print123\25\25d\25\25f\25\25.08fTesting\25\25s\0A\00"
@gi = common global i32 0
@gb = common global i1 0
@gs = internal constant [1 x i8] c"\00"
@gf = common global double 0.000000e+00

define i32 @main(i32 %argc, i8** %argv) {
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i1
    store i1 0, i1* %2
    %3 = alloca double
    store double 0.0, double* %3
    %4 = load i1, i1* %2
    store i1 %4, i1* @gb
    %5 = load i1, i1* @gb
    store i1 %5, i1* @gb
    %6 = load i1, i1* @gb
    store i1 %6, i1* %2
    %7 = load i32, i32* %1
    store i32 %7, i32* @gi
    %8 = load i32, i32* %1
    store i32 %8, i32* @gi
    %9 = load i32, i32* @gi
    store i32 %9, i32* %1
    %10 = load double, double* %3
    store double %10, double* @gf
    %11 = load double, double* @gf
    store double %11, double* @gf
    %12 = load double, double* @gf
    store double %12, double* %3
    %13 = add i32 0, 2
    %14 = sub i32 0, %13
    store i32 %14, i32* %1
    %15 = fadd double 0.0, 2.00000000
    %16 = fsub double 0.0, %15
    store double %16, double* @gf
    %17 = load double, double* @gf
    %18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %17)
    %19 = load double, double* @gf
    %20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %19)
    %21 = load double, double* @gf
    %22 = fsub double 0.0, %21
    %23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %22)
    %24 = fadd double 0.0, 1.10000000
    store double %24, double* @gf
    %25 = load double, double* @gf
    %26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %25)
    %27 = load double, double* @gf
    %28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %27)
    %29 = load double, double* @gf
    %30 = fsub double 0.0, %29
    %31 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %30)
    %32 = load i32, i32* %1
    %33 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %32)
    %34 = load i32, i32* %1
    %35 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %34)
    %36 = load i32, i32* %1
    %37 = sub i32 0, %36
    %38 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %37)
    %39 = add i32 0, 1
    %40 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %39)
    %41 = load i32, i32* @gi
    %42 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %41)
    %43 = load i32, i32* %1
    store i32 %43, i32* @gi
    %44 = load i32, i32* @gi
    %45 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %44)
    %46 = fadd double 0.0, 0.00000000
    store double %46, double* %3
    %47 = load double, double* %3
    %48 = fsub double 0.0, %47
    %49 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %48)
    %50 = fadd double 0.0, 1.90000000
    %51 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %50)
    %52 = load double, double* @gf
    %53 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %52)
    %54 = load double, double* %3
    store double %54, double* @gf
    %55 = load double, double* @gf
    %56 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %55)
    %57 = fadd double 0.0, 0.00000000
    %58 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %57)
    %59 = load i1, i1* @gb
    %60 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i1 %59)
    %61 = add i32 0, 0
    %62 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %61)
    %63 = add i32 0, 0
    %64 = sub i32 0, %63
    %65 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %64)
    %66 = fadd double 0.0, 0.00000000
    %67 = fsub double 0.0, %66
    %68 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %67)
    %69 = load i32, i32* %1
    %70 = sub i32 0, %69
    %71 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %70)
    %72 = load i1, i1* @gb
    %73 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i1 %72)
    %74 = load i1, i1* %2
    %75 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i1 %74)
    %76 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.1, i64 0, i64 0))
    ret i32 0
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
