@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [8 x i8] c"%0.8lf\0A\00"
@print_strings = internal constant [4 x i8] c"%s\0A\00"
@.str.1 = internal constant [17 x i8] c"Factorial of 5:\0A\00"
@.str.2 = internal constant [34 x i8] c"Factorial of parseArgs argument:\0A\00"
@.str.3 = internal constant [17 x i8] c"No return here!\0A\00"
@global = common global i32 0

define i32 @main(i32 %argc, i8** %argv) {
    %1 = alloca i32
    %2 = call i32 @teste()
    store i32 %2, i32* %1
    %3 = load i32, i32* %1
    %4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %3)
    call void @void()
    %5 = alloca i32
    %6 = add i32 0, 5
    store i32 %6, i32* @global
    %7 = alloca i32
    %8 = getelementptr i8*, i8** %argv, i32 1
    %9 = load i8*, i8** %8
    %10 = call i32 @atoi(i8* %9)
    store i32 %10, i32* %7
    %11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i64 0, i64 0))
    %12 = load i32, i32* @global
    %13 = call i32 @factorial(i32 %12)
    store i32 %13, i32* %5
    %14 = load i32, i32* %5
    %15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %14)
    %16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.2, i64 0, i64 0))
    %17 = load i32, i32* %7
    %18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %17)
    %19 = load i32, i32* %7
    %20 = call i32 @factorial(i32 %19)
    store i32 %20, i32* %5
    %21 = load i32, i32* %5
    %22 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %21)
    %23 = call i32 @teste()
    %24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %23)
    ret i32 0
}

define void @void() {
    %1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.3, i64 0, i64 0))
    ret void
}

define i32 @factorial(i32 %n) {
    %1 = alloca i32
    store i32 %n, i32* %1
    %2 = load i32, i32* %1
    %3 = add i32 0, 1
    %4 = icmp eq i32 %2, %3
    br i1 %4, label %then1, label %else1

then1:
    %5 = add i32 0, 1
    ret i32 %5
else1:
    br label %ifcont1

ifcont1:
    %6 = load i32, i32* %1
    %7 = load i32, i32* %1
    %8 = add i32 0, 1
    %9 = sub i32 %7, %8
    %10 = call i32 @factorial(i32 %9)
    %11 = mul nsw i32 %6, %10
    ret i32 %11
}

define i32 @teste() {
    %1 = add i32 0, 123
    ret i32 %1
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
