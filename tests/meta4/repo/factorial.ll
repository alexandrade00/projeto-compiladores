@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [5 x i8] c"%lf\0A\00"

define i32 @factorial(i32 %n) {
    %1 = alloca i32
    store i32 %n, i32* %1
    %2 = load i32, i32* %1
    %3 = add i32 0, 0
    %4 = icmp eq i32 %2, %3
    br i1 %4, label %then1, label %else1

then1:
    %5 = add i32 0, 1
    ret i32 %5
    br label %ifcont1

else1:
    br label %ifcont1

ifcont1:
    %6 = load i32, i32* %1
    %7 = load i32, i32* %1
    %8 = add i32 0, 1
    %9 = sub i32 %7, %8
    %10 = call i32 @factorial(i32 %9)
    %11 = mul nsw i32 %6, %10
    ret i32 %11
}

define void @main(i32 %argc, i8** %argv) {
    %1 = alloca i32
    %2 = getelementptr i8*, i8** %argv, i32 1
    %3 = load i8*, i8** %2
    %4 = call i32 @atoi(i8* %3)
    store i32 %4, i32* %1
    %5 = load i32, i32* %1
    %6 = call i32 @factorial(i32 %5)
    %7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %6)
    ret void
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
