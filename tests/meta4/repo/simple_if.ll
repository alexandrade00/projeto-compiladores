@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [8 x i8] c"%0.8lf\0A\00"
@print_strings = internal constant [4 x i8] c"%s\0A\00"
@print_bool_true = internal constant [6 x i8] c"true\0A\00"
@print_bool_false = internal constant [7 x i8] c"false\0A\00"
@.str.1 = internal constant [10 x i8] c"trueBool\0A\00"
@.str.2 = internal constant [17 x i8] c"Do not pressint\0A\00"
@.str.3 = internal constant [17 x i8] c"or Not trueBool\0A\00"
@.str.4 = internal constant [20 x i8] c"one not equals two\0A\00"
@.str.5 = internal constant [20 x i8] c"and for floats too\0A\00"
@.str.6 = internal constant [18 x i8] c"Can't print this\0A\00"

define i32 @main(i32 %argc, i8** %argv) {
    %1 = alloca i1
    store i1 0, i1* %1
    %2 = load i1, i1* %1
    %3 = xor i1 %2, true
    store i1 %3, i1* %1
    %4 = load i1, i1* %1
    br i1 %4, label %then1, label %else1

then1:
    %5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i64 0, i64 0))
    %6 = load i1, i1* %1
    %7 = xor i1 %6, true
    br i1 %7, label %then2, label %else2

then2:
    %8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.2, i64 0, i64 0))
    br label %ifcont2

else2:
    %9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.3, i64 0, i64 0))
    %10 = add i32 0, 1
    %11 = add i32 0, 2
    %12 = icmp ne i32 %10, %11
    br i1 %12, label %then3, label %else3

then3:
    %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.4, i64 0, i64 0))
    %14 = fadd double 0.0, 11.01200000
    %15 = fadd double 0.0, 2000.00000000
    %16 = fcmp olt double %14, %15
    br i1 %16, label %then4, label %else4

then4:
    %17 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.5, i64 0, i64 0))
    br label %ifcont4

else4:
    br label %ifcont4

ifcont4:
    br label %ifcont3

else3:
    br label %ifcont3

ifcont3:
    br label %ifcont2

ifcont2:
    br label %ifcont1

else1:
    %18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.6, i64 0, i64 0))
    br label %ifcont1

ifcont1:
    ret i32 0
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
