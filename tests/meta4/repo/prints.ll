@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [5 x i8] c"%lf\0A\00"
@.str.1 = internal constant [11 x i8] c"sdjfhighi\0A\00"
@.str.2 = internal constant [8 x i8] c"sdjfhi\t tab\0A\00"

@.str.3 = internal constant [5 x i8] c"sdjfhi\t"d\ newline\0A\00"
@.str.4 = internal constant [8 x i8] c"sdjfhi\t"d:\\ barra \\\0A\00"
@.str.5 = internal constant [9 x i8] c"sdjfhi\\f feed\0A\00"
@.str.6 = internal constant [8 x i8] c"sdjfhi\\"d:\r carriage\0A\00"
@.str.7 = internal constant [6 x i8] c"sdjfhi\\"d\"aspazita\0A\00"

@.str.8 = internal constant [24 x i8] c"���'�\f varios \t  \\ \ \"\0A\00"
@.str.9 = internal constant [40 x i8] c"cena do printf %%%%s %%%%d %%%%l %%%%x\0A\00"
@.str.10 = internal constant [1020 x i8] c"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis blandit tellus. Curabitur ornare elementum mollis. Pellentesque eleifend tempor justo in facilisis. Phasellus ut mattis ex. Etiam et imperdiet nisl. Nam sit amet justo ante. Proin semper varius luctus. Quisque a lorem neque. Ut nec augue eu sapien interdum euismod at nec urna. Nulla et nunc sodales, pulvinar erat in, sagittis quam. Duis vulputate augue finibus ligula mattis congue. Maecenas condimentum nunc ante, non facilisis est aliquam sed. Donec vel ex nec est pharetra aliquam a quis dui. Aenean ac quam hendrerit, suscipit orci et, vestibulum quam. Donec volutpat hendrerit mi sed placerat. Donec dignissim sapien feugiat felis placerat cursus. Suspendisse mattis magna non risus ornare, a auctor nisl consequat. Nam viverra risus ac dui pharetra, id posuere massa mattis. Nullam at pellentesque neque. Curabitur tempus molestie mi ac ornare. Aenean tortor nisl, condimentum eget ligula a, consequat bibendum justo. Donec nec viverra massa. \0A\00"
@b = common global i8 0
@a = common global i32 0

define void @main(i32 %argc, i8** %argv) {
@a = common global i32 0
    %1 = alloca double
    %2 = add i32 0, 10
    store i32 %2, i32* @a
    %3 = fadd double 0.0, 10.123320
    store double %3, double* %1
    %4 = load double, double* %1
    %5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @print_floats, i64 0, i64 0), double %4)
    %6 = load i32, i32* @a
    %7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @print_floats, i64 0, i64 0), double %6)
    %8 = fadd double 0.0, 1.000003
    %9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @print_floats, i64 0, i64 0), double %8)
    %10 = fadd double 0.0, 1.478347
    %11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @print_floats, i64 0, i64 0), double %10)
    %12 = add i32 0, 0
    %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @print_floats, i64 0, i64 0), double %12)
    %14 = add i32 0, 0
    %15 = sub i32 0, %14
    %16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %15)
    %17 = fadd double 0.0, 0.000000
    %18 = fsub double 0.0, %17
    %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @print_floats, i64 0, i64 0), double %18)
    %20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i64 0, i64 0))
    %21 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i64 0, i64 0))
    %22 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.3, i64 0, i64 0))
    %23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.4, i64 0, i64 0))
    %24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.5, i64 0, i64 0))
    %25 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.6, i64 0, i64 0))
    %26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.7, i64 0, i64 0))
    %27 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.8, i64 0, i64 0))
    %28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.9, i64 0, i64 0))
    %29 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([1020 x i8], [1020 x i8]* @.str.10, i64 0, i64 0))
    ret void
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
