@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [8 x i8] c"%0.8lf\0A\00"
@print_strings = internal constant [4 x i8] c"%s\0A\00"
@print_bool_true = internal constant [6 x i8] c"true\0A\00"
@print_bool_false = internal constant [7 x i8] c"false\0A\00"

define i32 @main(i32 %argc, i8** %argv) {
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i32
    store i32 0, i32* %2
    %3 = add i32 0, 1
    store i32 %3, i32* %2
    %4 = getelementptr i8*, i8** %argv, i32 1
    %5 = load i8*, i8** %4
    %6 = call i32 @atoi(i8* %5)
    store i32 %6, i32* %1
    %7 = load i32, i32* %1
    %8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %7)
    %9 = getelementptr i8*, i8** %argv, i32 2
    %10 = load i8*, i8** %9
    %11 = call i32 @atoi(i8* %10)
    store i32 %11, i32* %1
    %12 = load i32, i32* %1
    %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %12)
    %14 = load i32, i32* %2
    %15 = getelementptr i8*, i8** %argv, i32 %14
    %16 = load i8*, i8** %15
    %17 = call i32 @atoi(i8* %16)
    store i32 %17, i32* %1
    %18 = load i32, i32* %1
    %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %18)
    %20 = getelementptr i8*, i8** %argv, i32 4
    %21 = load i8*, i8** %20
    %22 = call i32 @atoi(i8* %21)
    store i32 %22, i32* %1
    %23 = load i32, i32* %1
    %24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %23)
    %25 = add i32 0, 3
    store i32 %25, i32* %2
    %26 = load i32, i32* %2
    %27 = getelementptr i8*, i8** %argv, i32 %26
    %28 = load i8*, i8** %27
    %29 = call i32 @atoi(i8* %28)
    store i32 %29, i32* %1
    %30 = load i32, i32* %1
    %31 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %30)
    ret i32 0
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
