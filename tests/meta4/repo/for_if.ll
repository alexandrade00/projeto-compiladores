@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [4 x i8] c"%f\0A\00"
@.str.1 = internal constant [19 x i8] c"2 to the power of\0A\00"
@.str.2 = internal constant [4 x i8] c"is\0A\00"
@.str.3 = internal constant [2 x i8] c"\0A\00"

define void @main(i32 %argc, i8** %argv) {
    %1 = alloca i32
    %2 = alloca i32
    %3 = add i32 0, 0
    store i32 %3, i32* %1
    %4 = add i32 0, 0
    store i32 %4, i32* %2
    br label %loop_param1

loop_param1:
    %5 = load i32, i32* %1
    %6 = add i32 0, 14
    %7 = icmp slt i32 %5, %6
    br i1 %7, label %loop1, label %loop_cont1

loop1:
    %8 = load i32, i32* %1
    %9 = add i32 0, 0
    %10 = icmp eq i32 %8, %9
    br i1 %10, label %then1, label %else1

then1:
    %11 = add i32 0, 1
    store i32 %11, i32* %2
    br label %ifcont1

else1:
    %12 = load i32, i32* %2
    %13 = add i32 0, 2
    %14 = mul nsw i32 %12, %13
    store i32 %14, i32* %2
    br label %ifcont1

ifcont1:
    %15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.1, i64 0, i64 0))
    %16 = load i32, i32* %1
    %17 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %16)
    %18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i64 0, i64 0))
    %19 = load i32, i32* %2
    %20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %19)
    %21 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.3, i64 0, i64 0))
    %22 = load i32, i32* %1
    %23 = add i32 0, 1
    %24 = add i32 %22, %23
    store i32 %24, i32* %1
    br label %loop_param1

loop_cont1:
    ret void
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
