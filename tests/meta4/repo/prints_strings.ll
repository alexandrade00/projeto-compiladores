@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [8 x i8] c"%0.8lf\0A\00"
@print_strings = internal constant [4 x i8] c"%s\0A\00"
@print_bool_true = internal constant [6 x i8] c"true\0A\00"
@print_bool_false = internal constant [7 x i8] c"false\0A\00"
@.str.1 = internal constant [11 x i8] c"sdjfhighi\0A\00"
@.str.2 = internal constant [7 x i8] c"\09 tab\0A\00"
@.str.3 = internal constant [11 x i8] c"\0A newline\0A\00"
@.str.4 = internal constant [11 x i8] c"\5C barra \5C\0A\00"
@.str.5 = internal constant [8 x i8] c"\0C feed\0A\00"
@.str.6 = internal constant [12 x i8] c"\0D carriage\0A\00"
@.str.7 = internal constant [11 x i8] c"\22aspazita\0A\00"
@.str.8 = internal constant [19 x i8] c"\0C varios \09  \5C \0A \22\0A\00"
@.str.9 = internal constant [32 x i8] c"cena do printf \25\25s \25\25d \25\25l \25\25x\0A\00"
@.str.10 = internal constant [1020 x i8] c"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis blandit tellus. Curabitur ornare elementum mollis. Pellentesque eleifend tempor justo in facilisis. Phasellus ut mattis ex. Etiam et imperdiet nisl. Nam sit amet justo ante. Proin semper varius luctus. Quisque a lorem neque. Ut nec augue eu sapien interdum euismod at nec urna. Nulla et nunc sodales, pulvinar erat in, sagittis quam. Duis vulputate augue finibus ligula mattis congue. Maecenas condimentum nunc ante, non facilisis est aliquam sed. Donec vel ex nec est pharetra aliquam a quis dui. Aenean ac quam hendrerit, suscipit orci et, vestibulum quam. Donec volutpat hendrerit mi sed placerat. Donec dignissim sapien feugiat felis placerat cursus. Suspendisse mattis magna non risus ornare, a auctor nisl consequat. Nam viverra risus ac dui pharetra, id posuere massa mattis. Nullam at pellentesque neque. Curabitur tempus molestie mi ac ornare. Aenean tortor nisl, condimentum eget ligula a, consequat bibendum justo. Donec nec viverra massa. \0A\00"
@b = common global i1 0
@a = common global i32 0

define i32 @main(i32 %argc, i8** %argv) {
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca double
    store double 0.0, double* %2
    %3 = add i32 0, 10
    store i32 %3, i32* %1
    %4 = fadd double 0.0, 10.12332000
    store double %4, double* %2
    %5 = load double, double* %2
    %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %5)
    %7 = load i32, i32* %1
    %8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %7)
    %9 = fadd double 0.0, 1.00000300
    %10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %9)
    %11 = fadd double 0.0, 1.47834700
    %12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %11)
    %13 = add i32 0, 0
    %14 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %13)
    %15 = add i32 0, 0
    %16 = sub i32 0, %15
    %17 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %16)
    %18 = fadd double 0.0, 0.00000000
    %19 = fsub double 0.0, %18
    %20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %19)
    %21 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i64 0, i64 0))
    %22 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.2, i64 0, i64 0))
    %23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.3, i64 0, i64 0))
    %24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.4, i64 0, i64 0))
    %25 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.5, i64 0, i64 0))
    %26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.6, i64 0, i64 0))
    %27 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.7, i64 0, i64 0))
    %28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.8, i64 0, i64 0))
    %29 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.9, i64 0, i64 0))
    %30 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([1020 x i8], [1020 x i8]* @.str.10, i64 0, i64 0))
    ret i32 0
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
