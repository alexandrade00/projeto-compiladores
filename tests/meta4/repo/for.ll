@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [4 x i8] c"%f\0A\00"
@.str.1 = internal constant [19 x i8] c"2 to the power of\0A\00"
@.str.2 = internal constant [4 x i8] c"is\0A\00"
@.str.3 = internal constant [2 x i8] c"\0A\00"

define void @main(i32 %argc, i8** %argv) {
    %1 = alloca i32
    %2 = alloca i32
    %3 = add i32 0, 0
    store i32 %3, i32* %1
    %4 = add i32 0, 1
    store i32 %4, i32* %2
    br label %loop_param1

loop_param1:
    %5 = load i32, i32* %1
    %6 = add i32 0, 14
    %7 = icmp slt i32 %5, %6
    br i1 %7, label %loop1, label %loop_cont1

loop1:
    %8 = load i32, i32* %1
    %9 = add i32 0, 1
    %10 = add i32 %8, %9
    store i32 %10, i32* %1
    %11 = load i32, i32* %2
    %12 = add i32 0, 2
    %13 = mul nsw i32 %11, %12
    store i32 %13, i32* %2
    %14 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.1, i64 0, i64 0))
    %15 = load i32, i32* %1
    %16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %15)
    %17 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i64 0, i64 0))
    %18 = load i32, i32* %2
    %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %18)
    %20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.3, i64 0, i64 0))
    br label %loop_param1

loop_cont1:
    ret void
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
