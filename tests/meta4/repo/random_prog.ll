@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [8 x i8] c"%0.8lf\0A\00"
@print_strings = internal constant [4 x i8] c"%s\0A\00"
@print_bool_true = internal constant [6 x i8] c"true\0A\00"
@print_bool_false = internal constant [7 x i8] c"false\0A\00"
@.str.1 = internal constant [16 x i8] c"CANT PRINT....\0A\00"
@.str.2 = internal constant [14 x i8] c"random print\0A\00"
@.str.3 = internal constant [3 x i8] c">\0A\00"
@.str.4 = internal constant [9 x i8] c"if true\0A\00"
@.str.5 = internal constant [19 x i8] c"no prints for you\0A\00"
@.str.6 = internal constant [9 x i8] c"Pinting\0A\00"
@.str.7 = internal constant [57 x i8] c"if you print this, you might get 10 points... or not...\0A\00"
@.str.8 = internal constant [51 x i8] c"mooshak is located @ 1st floor @ dei. just saying\0A\00"
@.str.9 = internal constant [2 x i8] c"\0A\00"
@.str.10 = internal constant [48 x i8] c"  /\5C/\5C   ___   ___  ___| |__   __ _| | __     \0A\00"
@.str.11 = internal constant [46 x i8] c" /    \5C / _ \5C / _ \5C/ __| '_ \5C / _` | |/ /   \0A\00"
@.str.12 = internal constant [45 x i8] c"/ /\5C/\5C \5C (_) | (_) \5C__ \5C | | | (_| |   <   \0A\00"
@.str.13 = internal constant [43 x i8] c"\5C/    \5C/\5C___/ \5C___/|___/_| |_|\5C__,_|_|\5C_\5C\0A\00"
@.str.14 = internal constant [2 x i8] c"\0A\00"
@ia = common global i32 0
@ib = common global i32 0
@ic = common global i32 0
@id = common global i32 0
@ie = common global i32 0
@fa = common global double 0.000000e+00
@fb = common global double 0.000000e+00
@fc = common global double 0.000000e+00
@fd = common global double 0.000000e+00
@fe = common global double 0.000000e+00
@ba = common global i1 0
@bb = common global i1 0
@bc = common global i1 0
@bd = common global i1 0
@sa = internal constant [1 x i8] c"\00"
@sb = internal constant [1 x i8] c"\00"
@sc = internal constant [1 x i8] c"\00"

define void @fca() {
    ret void
}

define i32 @fcb() {
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = add i32 0, 132
    %3 = load i32, i32* @ia
    %4 = add i32 %2, %3
    store i32 %4, i32* %1
    %5 = load i32, i32* %1
    store i32 %5, i32* @ia
    %6 = load i32, i32* %1
    %7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %6)
    %8 = load i32, i32* %1
    ret i32 %8
}

define double @fcd() {
    %1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i64 0, i64 0))
    %2 = fadd double 0.0, 1.00000000
    %3 = fadd double 0.0, 0.00700000
    %4 = fadd double %2, %3
    ret double %4
}

define i8* @fce() {
    %1 = alloca i8
    store i8 0, i8* %1
    %2 = alloca i8*
    store i8* %1, i8** %2
    %3 = load i8*, i8** %2
    %4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_strings, i64 0, i64 0), i8* %3)
    %5 = load i8*, i8** %2
    ret i8* %5
}

define i1 @fcf() {
    %1 = alloca i1
    store i1 0, i1* %1
    %2 = add i32 0, 1
    %3 = add i32 0, 1
    %4 = icmp eq i32 %2, %3
    store i1 %4, i1* %1
    %5 = load i1, i1* %1
    %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i1 %5)
    %7 = load i1, i1* %1
    ret i1 %7
}

define i32 @fcg(i32 %iafcg, double %fafcg) {
    %1 = call double @fcd()
    %2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %1)
    %3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i64 0, i64 0))
    %4 = alloca double
    store double %fafcg, double* %4
    %5 = load double, double* %4
    %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %5)
    %7 = call double @fcd()
    %8 = load double, double* %4
    %9 = fcmp ogt double %7, %8
    br i1 %9, label %then1, label %else1

then1:
    call void @fca()
    %10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.4, i64 0, i64 0))
    %11 = alloca i32
    store i32 %iafcg, i32* %11
    %12 = load i32, i32* %11
    ret i32 %12
else1:
    br label %ifcont1

ifcont1:
    %13 = call i32 @fcb()
    ret i32 %13
}

define double @fch(i32 %iafch, double %fafch, i8* %safch) {
    %1 = call i32 @fcb()
    %2 = call double @fcd()
    %3 = call i32 @fcg(i32 %1, double %2)
    %4 = alloca i8*
    store i8* %safch, i8** %4
    %5 = load i8*, i8** %4
    %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_strings, i64 0, i64 0), i8* %5)
    %7 = load i1, i1* @bb
    br i1 %7, label %then1, label %else1

then1:
    %8 = load i32, i32* @ia
    %9 = load i32, i32* @ib
    %10 = add i32 %8, %9
    %11 = alloca i32
    store i32 %10, i32* %11
    %12 = load double, double* @fa
    %13 = alloca double
    store double %12, double* %13
    %14 = load double, double* %13
    store double %14, double* @fb
    %15 = load i32, i32* %11
    store i32 %15, i32* @id
    br label %ifcont1

else1:
    br label %ifcont1

ifcont1:
    %16 = fadd double 0.0, 0.01000000
    ret double %16
}

define double @fci(i32 %iafci, i32 %ibfci, double %fafci, double %fbfci, i8* %safci) {
    %1 = call i32 @fcb()
    %2 = call double @fcd()
    %3 = call i32 @fcg(i32 %1, double %2)
    %4 = call double @fcd()
    %5 = call i8* @fce()
    %6 = call double @fch(i32 %3, double %4, i8* %5)
    %7 = alloca i32
    store i32 %ibfci, i32* %7
    %8 = load i32, i32* %7
    %9 = add i32 0, 12
    %10 = add i32 %8, %9
    %11 = load i32, i32* @ic
    %12 = add i32 %10, %11
    %13 = call i32 @fcb()
    %14 = alloca i32
    store i32 %13, i32* %14
    %15 = call i1 @fcf()
    br i1 %15, label %then1, label %else1

then1:
    %16 = alloca i8*
    store i8* %safci, i8** %16
    %17 = load i8*, i8** %16
    %18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_strings, i64 0, i64 0), i8* %17)
    %19 = load i32, i32* @ia
    %20 = load double, double* @fa
    %21 = getelementptr [1 x i8], [1 x i8]* @sa, i32 1, i32 1
    %22 = call double @fch(i32 %19, double %20, i8* %21)
    ret double %22
else1:
    %23 = call double @fcd()
    ret double %23
    br label %ifcont1

ifcont1:
    ret double 0.0
}

define i32 @fcj(i32 %iafcj, i32 %ibfcj, double %fafcj, double %fbfcj, i1 %bafcj) {
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i32
    store i32 0, i32* %2
    %3 = load i32, i32* @ia
    %4 = load i32, i32* @ib
    %5 = add i32 %3, %4
    %6 = load i32, i32* @ic
    %7 = load i32, i32* @id
    %8 = mul nsw i32 %6, %7
    %9 = load i32, i32* @ie
    %10 = srem i32 %8, %9
    %11 = sub i32 %5, %10
    store i32 %11, i32* %1
    %12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.6, i64 0, i64 0))
    %13 = load i32, i32* %1
    %14 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %13)
    %15 = load i32, i32* %1
    %16 = load i32, i32* %1
    %17 = call i32 @fcb()
    %18 = load i32, i32* %2
    %19 = call i32 @fcb()
    %20 = load double, double* @fb
    %21 = getelementptr [1 x i8], [1 x i8]* @sa, i32 1, i32 1
    %22 = call double @fch(i32 %19, double %20, i8* %21)
    %23 = load double, double* @fd
    %24 = getelementptr [1 x i8], [1 x i8]* @sb, i32 1, i32 1
    %25 = call double @fci(i32 %17, i32 %18, double %22, double %23, i8* %24)
    %26 = call i32 @fcg(i32 %16, double %25)
    %27 = icmp sgt i32 %15, %26
    br i1 %27, label %then1, label %else1

then1:
    %28 = load i1, i1* @ba
    br i1 %28, label %then2, label %else2

then2:
    %29 = fadd double 0.0, 0.01000000
    store double %29, double* @fc
    br label %ifcont2

else2:
    %30 = load double, double* @fc
    %31 = load double, double* @fe
    %32 = fadd double %30, %31
    store double %32, double* @fc
    %33 = alloca i1
    store i1 %bafcj, i1* %33
    %34 = load i1, i1* %33
    br i1 %34, label %then3, label %else3

then3:
    br label %loop_param1

loop_param1:
    %35 = call i1 @fcf()
    br i1 %35, label %loop1, label %loop_cont1

loop1:
    %36 = load i32, i32* %1
    %37 = call i32 @fcb()
    %38 = add i32 0, 12
    %39 = sdiv i32 %37, %38
    %40 = add i32 %36, %39
    %41 = load i32, i32* @ia
    %42 = alloca i32
    store i32 %iafcj, i32* %42
    %43 = load i32, i32* %42
    %44 = add i32 %41, %43
    %45 = load double, double* @fa
    %46 = call i32 @fcg(i32 %44, double %45)
    %47 = alloca i32
    store i32 %ibfcj, i32* %47
    %48 = load i32, i32* %47
    store i32 %48, i32* %2
    %49 = alloca double
    store double %fafcj, double* %49
    %50 = load double, double* %49
    %51 = alloca double
    store double %50, double* %51
    br label %loop_param1

loop_cont1:
    br label %ifcont3

else3:
    br label %ifcont3

ifcont3:
    br label %ifcont2

ifcont2:
    br label %ifcont1

else1:
    br label %ifcont1

ifcont1:
    br label %loop_param2

loop_param2:
    %52 = load i1, i1* @bb
    br i1 %52, label %loop2, label %loop_cont2

loop2:
    %53 = load i1, i1* @bc
    br i1 %53, label %then4, label %else4

then4:
    %54 = load i1, i1* @bd
    %55 = load i1, i1* @ba
    %56 = and i1 %54, %55
    store i1 %56, i1* @bd
    %57 = load i1, i1* @ba
    %58 = call i1 @fcf()
    %59 = and i1 %57, %58
    %60 = load i1, i1* @bd
    %61 = load i1, i1* @bc
    %62 = xor i1 %61, true
    %63 = and i1 %60, %62
    %64 = or i1 %59, %63
    store i1 %64, i1* @bd
    br label %ifcont4

else4:
    br label %ifcont4

ifcont4:
    br label %loop_param2

loop_cont2:
    %65 = add i32 0, 101
    %66 = sub i32 0, %65
    ret i32 %66
}

define i32 @main(i32 %argc, i8** %argv) {
    %1 = add i32 0, 1
    store i32 %1, i32* @ia
    %2 = add i32 0, 1
    store i32 %2, i32* @ib
    %3 = add i32 0, 1
    store i32 %3, i32* @ic
    %4 = add i32 0, 1
    store i32 %4, i32* @id
    %5 = add i32 0, 1
    store i32 %5, i32* @ie
    %6 = fadd double 0.0, 0.10000000
    store double %6, double* @fa
    %7 = fadd double 0.0, 0.10000000
    store double %7, double* @fb
    %8 = fadd double 0.0, 0.10000000
    store double %8, double* @fc
    %9 = fadd double 0.0, 0.10000000
    store double %9, double* @fd
    %10 = fadd double 0.0, 0.10000000
    store double %10, double* @fe
    %11 = load double, double* @fa
    %12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %11)
    %13 = alloca i32
    store i32 0, i32* %13
    %14 = alloca i32
    store i32 0, i32* %14
    %15 = load i32, i32* @ia
    %16 = load i32, i32* @ib
    %17 = add i32 %15, %16
    store i32 %17, i32* %13
    br label %loop_param1

loop_param1:
    %18 = load i32, i32* @ia
    %19 = load i32, i32* @ib
    %20 = icmp sgt i32 %18, %19
    %21 = call i1 @fcf()
    %22 = and i1 %20, %21
    br i1 %22, label %loop1, label %loop_cont1

loop1:
    %23 = call i32 @fcb()
    %24 = call i32 @fcb()
    %25 = load double, double* @fe
    %26 = call i32 @fcg(i32 %24, double %25)
    %27 = load i32, i32* %13
    %28 = call i32 @fcb()
    %29 = call double @fcd()
    %30 = call i32 @fcg(i32 %28, double %29)
    %31 = add i32 0, 123
    %32 = load i32, i32* @ia
    %33 = call double @fcd()
    %34 = call i8* @fce()
    %35 = call double @fch(i32 %32, double %33, i8* %34)
    %36 = call double @fcd()
    %37 = getelementptr [1 x i8], [1 x i8]* @sc, i32 1, i32 1
    %38 = call double @fci(i32 %30, i32 %31, double %35, double %36, i8* %37)
    %39 = call double @fcd()
    %40 = call i1 @fcf()
    %41 = call i32 @fcj(i32 %26, i32 %27, double %38, double %39, i1 %40)
    %42 = icmp sgt i32 %23, %41
    br i1 %42, label %then1, label %else1

then1:
    %43 = add i32 0, 12
    %44 = load i32, i32* @ia
    %45 = load double, double* @fa
    %46 = fadd double 0.0, 100000000000.00000000
    %47 = load i1, i1* @ba
    %48 = call i32 @fcj(i32 %43, i32 %44, double %45, double %46, i1 %47)
    store i32 %48, i32* %14
    %49 = load i32, i32* %14
    %50 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %49)
    br label %ifcont1

else1:
    %51 = load i32, i32* @ia
    %52 = call i32 @fcb()
    store i32 %52, i32* %14
    %53 = load i32, i32* %14
    %54 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %53)
    br label %ifcont1

ifcont1:
    br label %loop_param1

loop_cont1:
    %55 = load i32, i32* %14
    %56 = add i32 0, 123
    %57 = add i32 0, 456
    %58 = call i32 @fcb()
    %59 = call double @fcd()
    %60 = fadd double 0.0, 123.32100000
    %61 = fadd double %59, %60
    %62 = call i32 @fcg(i32 %58, double %61)
    %63 = load i32, i32* @ia
    %64 = load i32, i32* @ib
    %65 = add i32 %63, %64
    %66 = load i32, i32* @ic
    %67 = add i32 0, 12
    %68 = sdiv i32 %66, %67
    %69 = sub i32 %65, %68
    %70 = load i32, i32* @id
    %71 = load i32, i32* @ie
    %72 = mul nsw i32 %70, %71
    %73 = call i32 @fcb()
    %74 = load double, double* @fa
    %75 = call i32 @fcg(i32 %73, double %74)
    %76 = srem i32 %72, %75
    %77 = load i32, i32* @ia
    %78 = load i32, i32* @ib
    %79 = add i32 %77, %78
    %80 = add i32 0, 123
    %81 = load double, double* @fa
    %82 = call double @fcd()
    %83 = fadd double 0.0, 100000000000.00000000
    %84 = fdiv double %82, %83
    %85 = fsub double %81, %84
    %86 = call double @fcd()
    %87 = call i8* @fce()
    %88 = call double @fci(i32 %79, i32 %80, double %85, double %86, i8* %87)
    %89 = add i32 0, 312
    %90 = add i32 0, 12
    %91 = add i32 %89, %90
    %92 = call double @fcd()
    %93 = fadd double 0.0, 10000000000000000000000.00000000
    %94 = fadd double %92, %93
    %95 = call i8* @fce()
    %96 = call double @fch(i32 %91, double %94, i8* %95)
    %97 = call i1 @fcf()
    %98 = load i1, i1* @ba
    %99 = and i1 %97, %98
    %100 = load i1, i1* @bb
    %101 = or i1 %99, %100
    %102 = call i32 @fcj(i32 %62, i32 %76, double %88, double %96, i1 %101)
    store i32 %102, i32* %13
    %103 = load i32, i32* %13
    %104 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), i32 %103)
    %105 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([57 x i8], [57 x i8]* @.str.7, i64 0, i64 0))
    %106 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.8, i64 0, i64 0))
    %107 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i64 0, i64 0))
    %108 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.10, i64 0, i64 0))
    %109 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.11, i64 0, i64 0))
    %110 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.12, i64 0, i64 0))
    %111 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str.13, i64 0, i64 0))
    %112 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i64 0, i64 0))
    ret i32 0
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
