@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [4 x i8] c"%f\0A\00"
@.str.1 = internal constant [12 x i8] c"sdjf%highi\0A\00"

define i32 @main(i32 %argc, i8** %argv) {
    %1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
    %2 = add nsw i32 0, 1
    ret i32 %2
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
