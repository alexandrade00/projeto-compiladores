@b = common dso_local global i32 0
@c = common dso_local global i32 0
@a = common dso_local global i32 0

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @factorial() {
  %1 = alloca i32
  ret void
}
