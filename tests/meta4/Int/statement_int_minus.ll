@b = common dso_local global i32 0
@c = common dso_local global i32 0
@a = common dso_local global i32 0

define dso_local i32 @fcd() #0 {
  %1 = load i32, i32* @b, align 4
  %2 = load i32, i32* @c, align 4
  %3 = sub nsw i32 %1, %2
  store i32 %3, i32* @a, align 4
  %4 = load i32, i32* @a, align 4
  ret i32 %4
}
