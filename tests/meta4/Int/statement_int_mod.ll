@b = common global i32 0
@c = common global i32 0
@a = common global i32 0

define void @factorial() {
  %1 = load i32, i32* @b
  %2 = load i32, i32* @c
  %3 = srem i32 %1, %2
  store i32 %3, i32* @a
  ret void
}