@b = common dso_local global i32 0
@c = common dso_local global i32 0
@a = common dso_local global i32 0

define dso_local void @factorial() {
  %1 = load i32, i32* @b
  %2 = load i32, i32* @c
  %3 = mul nsw i32 %1, %2
  store i32 %3, i32* @a
  ret void
}