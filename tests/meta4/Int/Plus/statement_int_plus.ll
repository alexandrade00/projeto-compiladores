@b = common global i32 0, align 4
@c = common global i32 0, align 4
@a = common global i32 0, align 4

define void @factorial() {
  %1 = load i32, i32* @b
  %2 = load i32, i32* @c
  %3 = add nsw i32 %1, %2
  store i32 %3, i32* @a
  ret void
}
