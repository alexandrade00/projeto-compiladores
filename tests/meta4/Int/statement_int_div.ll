@b = common dso_local global i32 0, align 4
@c = common dso_local global i32 0, align 4
@a = common dso_local global i32 0, align 4


define dso_local void @factorial() #0 {
  %1 = load i32, i32* @b, align 4
  %2 = load i32, i32* @c, align 4
  %3 = sdiv i32 %1, %2
  store i32 %3, i32* @a, align 4
  ret void
}
