@b = common dso_local global float 0.000000e+00
@c = common dso_local global float 0.000000e+00
@a = common dso_local global float 0.000000e+00

define dso_local void @factorial() #0 {
  %1 = load float, float* @b
  %2 = load float, float* @c
  %3 = fmul float %1, %2
  store float %3, float* @a
  ret void
}