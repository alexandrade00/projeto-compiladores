@b = common global float 0.000000e+00
@c = common global float 0.000000e+00
@a = common global float 0.000000e+00

define float @factorial() #0 {
  %1 = alloca float, align 4
  %2 = load float, float* %1, align 4
  ret float %2
}