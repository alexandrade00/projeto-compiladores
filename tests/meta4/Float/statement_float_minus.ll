@b = common global float 0.000000e+00
@c = common global float 0.000000e+00
@a = common global float 0.000000e+00

define void @factorial() {
  %1 = load float, float* @b
  %2 = load float, float* @c
  %3 = fsub float %1, %2
  store float %3, float* @a
  ret void
}
