@ia = common global i32 0
@ib = common global i32 0
@ic = common global i32 0
@id = common global i32 0
@ie = common global i32 0
@fa = common global float 0.000000e+00
@fb = common global float 0.000000e+00
@fc = common global float 0.000000e+00
@fd = common global float 0.000000e+00
@fe = common global float 0.000000e+00
@ba = common global i8 0
@bb = common global i8 0
@bc = common global i8 0
@bd = common global i8 0

define void @fca() {
  ret void
}

define i32 @fcb() {
  %1 = alloca i32
  store i32 132, i32* %1
  %2 = load i32, i32* %1
  ret i32 %2
}

define float @fcd() {
  ret float 0x3FF01CAC00000000
}

define zeroext i1 @fcf() {
  %1 = alloca i8, align 1
  %2 = load i8, i8* %1, align 1
  %3 = trunc i8 %2 to i1
  ret i1 %3
}