@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [5 x i8] c"%lf\0A\00"
@.str.1 = internal constant [13 x i8] c"A%%%%C%%%%EFG\0A\00"
@b = common global i1 0
@a = common global i32 0

define void @main(i32 %argc, i8** %argv) {
    %1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i64 0, i64 0))
    ret void
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
