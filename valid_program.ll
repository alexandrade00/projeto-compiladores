@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [8 x i8] c"%0.8lf\0A\00"
@print_strings = internal constant [4 x i8] c"%s\0A\00"
@ia = common global i32 0
@ib = common global i32 0
@ic = common global i32 0
@id = common global i32 0
@ie = common global i32 0
@fa = common global double 0.000000e+00
@fb = common global double 0.000000e+00
@fc = common global double 0.000000e+00
@fd = common global double 0.000000e+00
@fe = common global double 0.000000e+00
@ba = common global i1 0
@bb = common global i1 0
@bc = common global i1 0
@bd = common global i1 0
@sa = internal constant [1 x i8] c"\00"
@sb = internal constant [1 x i8] c"\00"
@sc = internal constant [1 x i8] c"\00"

define void @fca() {
    ret void
}

define i32 @fcb() {
    %1 = alloca i32
    %2 = add i32 0, 132
    store i32 %2, i32* %1
    %3 = load i32, i32* %1
    ret i32 %3
}

define double @fcd() {
    %1 = fadd double 0.0, 1.00000000
    %2 = fadd double 0.0, 0.00700000
    %3 = fadd double %1, %2
    ret double %3
}

define i8* @fce() {
    %1 = alloca i8*
    %2 = load i8*, i8** %1
    ret i8* %2
}

define i1 @fcf() {
    %1 = alloca i1
    %2 = load i1, i1* %1
    ret i1 %2
}

define i32 @fcg(i32 %iafcg, double %fafcg) {
    call void @fca()
    %1 = alloca i32
    store i32 %iafcg, i32* %1
    %2 = load i32, i32* %1
    ret i32 %2
}

define double @fch(i32 %iafch, double %fafch, i8* %safch) {
    %1 = call i32 @fcb()
    %2 = call double @fcd()
    %3 = call i32 @fcg(i32 %1, double %2)
    %4 = alloca i8*
    store i8* %safch, i8** %4
    %5 = load i8*, i8** %4
    %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_strings, i64 0, i64 0), i8* %5)
    %7 = load i1, i1* @bb
    br i1 %7, label %then1, label %else1

then1:
    %8 = load i32, i32* @ia
    %9 = load i32, i32* @ib
    %10 = add i32 %8, %9
    %11 = alloca i32
    store i32 %10, i32* %11
    %12 = load double, double* @fa
    %13 = alloca double
    store double %12, double* %13
    %14 = load double, double* %13
    store double %14, double* @fb
    %15 = load i32, i32* %11
    store i32 %15, i32* @id
    br label %ifcont1

else1:
    br label %ifcont1

ifcont1:
    %16 = fadd double 0.0, 0.01000000
    ret double %16
}

define double @fci(i32 %iafci, i32 %ibfci, double %fafci, double %fbfci, i8* %safci) {
    %1 = call i32 @fcb()
    %2 = call double @fcd()
    %3 = call i32 @fcg(i32 %1, double %2)
    %4 = call double @fcd()
    %5 = call i8* @fce()
    %6 = call double @fch(i32 %3, double %4, i8* %5)
    %7 = alloca i32
    store i32 %ibfci, i32* %7
    %8 = load i32, i32* %7
    %9 = add i32 0, 12
    %10 = add i32 %8, %9
    %11 = load i32, i32* @ic
    %12 = add i32 %10, %11
    %13 = call i32 @fcb()
    %14 = alloca i32
    store i32 %13, i32* %14
    %15 = call i1 @fcf()
    br i1 %15, label %then1, label %else1

then1:
    %16 = alloca i8*
    store i8* %safci, i8** %16
    %17 = load i8*, i8** %16
    %18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_strings, i64 0, i64 0), i8* %17)
    %19 = load i32, i32* @ia
    %20 = load double, double* @fa
    %21 = getelementptr [1 x i8], [1 x i8]* @sa, i32 1, i32 1
    %22 = call double @fch(i32 %19, double %20, i8* %21)
    ret double %22
else1:
    %23 = call double @fcd()
    ret double %23
    br label %ifcont1

ifcont1:
    ret double 0.0
}

define i32 @fcj(i32 %iafcj, i32 %ibfcj, double %fafcj, double %fbfcj, i1 %bafcj) {
    %1 = alloca i32
    %2 = alloca i32
    %3 = load i32, i32* @ia
    %4 = load i32, i32* @ib
    %5 = add i32 %3, %4
    %6 = load i32, i32* @ic
    %7 = load i32, i32* @id
    %8 = mul nsw i32 %6, %7
    %9 = load i32, i32* @ie
    %10 = srem i32 %8, %9
    %11 = sub i32 %5, %10
    store i32 %11, i32* %1
    %12 = load i32, i32* %1
    %13 = load i32, i32* %1
    %14 = call i32 @fcb()
    %15 = load i32, i32* %2
    %16 = call i32 @fcb()
    %17 = load double, double* @fb
    %18 = getelementptr [1 x i8], [1 x i8]* @sa, i32 1, i32 1
    %19 = call double @fch(i32 %16, double %17, i8* %18)
    %20 = load double, double* @fd
    %21 = getelementptr [1 x i8], [1 x i8]* @sb, i32 1, i32 1
    %22 = call double @fci(i32 %14, i32 %15, double %19, double %20, i8* %21)
    %23 = call i32 @fcg(i32 %13, double %22)
    %24 = icmp sgt i32 %12, %23
    br i1 %24, label %then1, label %else1

then1:
    %25 = load i1, i1* @ba
    br i1 %25, label %then2, label %else2

then2:
    %26 = fadd double 0.0, 0.01000000
    store double %26, double* @fc
    br label %ifcont2

else2:
    %27 = load double, double* @fc
    %28 = load double, double* @fe
    %29 = fadd double %27, %28
    store double %29, double* @fc
    %30 = alloca i1
    store i1 %bafcj, i1* %30
    %31 = load i1, i1* %30
    br i1 %31, label %then3, label %else3

then3:
    br label %loop_param1

loop_param1:
    %32 = call i1 @fcf()
    br i1 %32, label %loop1, label %loop_cont1

loop1:
    %33 = load i32, i32* %1
    %34 = call i32 @fcb()
    %35 = add i32 0, 12
    %36 = sdiv i32 %34, %35
    %37 = add i32 %33, %36
    %38 = load i32, i32* @ia
    %39 = alloca i32
    store i32 %iafcj, i32* %39
    %40 = load i32, i32* %39
    %41 = add i32 %38, %40
    %42 = load double, double* @fa
    %43 = call i32 @fcg(i32 %41, double %42)
    %44 = alloca i32
    store i32 %ibfcj, i32* %44
    %45 = load i32, i32* %44
    store i32 %45, i32* %2
    %46 = alloca double
    store double %fafcj, double* %46
    %47 = load double, double* %46
    %48 = alloca double
    store double %47, double* %48
    br label %loop_param1

loop_cont1:
    br label %ifcont3

else3:
    br label %ifcont3

ifcont3:
    br label %ifcont2

ifcont2:
    br label %ifcont1

else1:
    br label %ifcont1

ifcont1:
    br label %loop_param2

loop_param2:
    %49 = load i1, i1* @bb
    br i1 %49, label %loop2, label %loop_cont2

loop2:
    %50 = load i1, i1* @bc
    br i1 %50, label %then4, label %else4

then4:
    %51 = load i1, i1* @bd
    %52 = load i1, i1* @ba
    %53 = and i1 %51, %52
    store i1 %53, i1* @bd
    %54 = load i1, i1* @ba
    %55 = call i1 @fcf()
    %56 = and i1 %54, %55
    %57 = load i1, i1* @bd
    %58 = load i1, i1* @bc
    %59 = xor i1 %58, true
    %60 = and i1 %57, %59
    %61 = or i1 %56, %60
    store i1 %61, i1* @bd
    br label %ifcont4

else4:
    br label %ifcont4

ifcont4:
    br label %loop_param2

loop_cont2:
    %62 = add i32 0, 101
    %63 = sub i32 0, %62
    ret i32 %63
}

define i32 @main(i32 %argc, i8** %argv) {
    %1 = add i32 0, 1
    store i32 %1, i32* @ia
    %2 = add i32 0, 1
    store i32 %2, i32* @ib
    %3 = add i32 0, 1
    store i32 %3, i32* @ic
    %4 = add i32 0, 1
    store i32 %4, i32* @id
    %5 = add i32 0, 1
    store i32 %5, i32* @ie
    %6 = fadd double 0.0, 0.10000000
    store double %6, double* @fa
    %7 = fadd double 0.0, 0.10000000
    store double %7, double* @fb
    %8 = fadd double 0.0, 0.10000000
    store double %8, double* @fc
    %9 = fadd double 0.0, 0.10000000
    store double %9, double* @fd
    %10 = fadd double 0.0, 0.10000000
    store double %10, double* @fe
    %11 = alloca i32
    %12 = alloca i32
    br label %loop_param1

loop_param1:
    %13 = load i32, i32* @ia
    %14 = load i32, i32* @ib
    %15 = icmp sgt i32 %13, %14
    %16 = call i1 @fcf()
    %17 = and i1 %15, %16
    br i1 %17, label %loop1, label %loop_cont1

loop1:
    %18 = call i32 @fcb()
    %19 = call i32 @fcb()
    %20 = load double, double* @fe
    %21 = call i32 @fcg(i32 %19, double %20)
    %22 = load i32, i32* %11
    %23 = call i32 @fcb()
    %24 = call double @fcd()
    %25 = call i32 @fcg(i32 %23, double %24)
    %26 = add i32 0, 123
    %27 = load i32, i32* @ia
    %28 = call double @fcd()
    %29 = call i8* @fce()
    %30 = call double @fch(i32 %27, double %28, i8* %29)
    %31 = call double @fcd()
    %32 = getelementptr [1 x i8], [1 x i8]* @sc, i32 1, i32 1
    %33 = call double @fci(i32 %25, i32 %26, double %30, double %31, i8* %32)
    %34 = call double @fcd()
    %35 = call i1 @fcf()
    %36 = call i32 @fcj(i32 %21, i32 %22, double %33, double %34, i1 %35)
    %37 = icmp sgt i32 %18, %36
    br i1 %37, label %then1, label %else1

then1:
    %38 = add i32 0, 12
    %39 = load i32, i32* @ia
    %40 = load double, double* @fa
    %41 = fadd double 0.0, 100000000000.00000000
    %42 = load i1, i1* @ba
    %43 = call i32 @fcj(i32 %38, i32 %39, double %40, double %41, i1 %42)
    store i32 %43, i32* %12
    br label %ifcont1

else1:
    %44 = load i32, i32* @ia
    %45 = call i32 @fcb()
    store i32 %45, i32* %12
    br label %ifcont1

ifcont1:
    br label %loop_param1

loop_cont1:
    %46 = load i32, i32* %12
    %47 = add i32 0, 123
    %48 = add i32 0, 456
    %49 = call i32 @fcb()
    %50 = call double @fcd()
    %51 = fadd double 0.0, 123.32100000
    %52 = fadd double %50, %51
    %53 = call i32 @fcg(i32 %49, double %52)
    %54 = load i32, i32* @ia
    %55 = load i32, i32* @ib
    %56 = add i32 %54, %55
    %57 = load i32, i32* @ic
    %58 = add i32 0, 12
    %59 = sdiv i32 %57, %58
    %60 = sub i32 %56, %59
    %61 = load i32, i32* @id
    %62 = load i32, i32* @ie
    %63 = mul nsw i32 %61, %62
    %64 = call i32 @fcb()
    %65 = load double, double* @fa
    %66 = call i32 @fcg(i32 %64, double %65)
    %67 = srem i32 %63, %66
    %68 = load i32, i32* @ia
    %69 = load i32, i32* @ib
    %70 = add i32 %68, %69
    %71 = add i32 0, 123
    %72 = load double, double* @fa
    %73 = call double @fcd()
    %74 = fadd double 0.0, 100000000000.00000000
    %75 = fdiv double %73, %74
    %76 = fsub double %72, %75
    %77 = call double @fcd()
    %78 = call i8* @fce()
    %79 = call double @fci(i32 %70, i32 %71, double %76, double %77, i8* %78)
    %80 = add i32 0, 312
    %81 = add i32 0, 12
    %82 = add i32 %80, %81
    %83 = call double @fcd()
    %84 = fadd double 0.0, 10000000000000000000000.00000000
    %85 = fadd double %83, %84
    %86 = call i8* @fce()
    %87 = call double @fch(i32 %82, double %85, i8* %86)
    %88 = call i1 @fcf()
    %89 = load i1, i1* @ba
    %90 = and i1 %88, %89
    %91 = load i1, i1* @bb
    %92 = or i1 %90, %91
    %93 = call i32 @fcj(i32 %53, i32 %67, double %79, double %87, i1 %92)
    store i32 %93, i32* %11
    ret i32 0
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
