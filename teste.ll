@print_ints = internal constant [4 x i8] c"%d\0A\00"
@print_floats = internal constant [8 x i8] c"%0.8lf\0A\00"
@print_strings = internal constant [4 x i8] c"%s\0A\00"
@print_bool_true = internal constant [6 x i8] c"true\0A\00"
@print_bool_false = internal constant [7 x i8] c"false\0A\00"
@.str.1 = internal constant [7 x i8] c"all p\0A\00"
@.str.2 = internal constant [7 x i8] c"c not\0A\00"
@.str.3 = internal constant [7 x i8] c"b not\0A\00"
@.str.4 = internal constant [13 x i8] c"b and c not\0A\00"
@.str.5 = internal constant [7 x i8] c"a not\0A\00"
@.str.6 = internal constant [13 x i8] c"a and c not\0A\00"
@.str.7 = internal constant [13 x i8] c"a and b not\0A\00"
@.str.8 = internal constant [9 x i8] c"all not\0A\00"

define i32 @main(i32 %argc, i8** %argv) {
    %1 = add i32 0, 1
    %2 = add i32 0, 1
    %3 = sub i32 0, %2
    %4 = add i32 0, 1
    call void @flow(i32 %1, i32 %3, i32 %4)
    ret i32 0
}

define void @flow(i32 %a, i32 %b, i32 %c) {
    %1 = alloca i32
    store i32 %a, i32* %1
    %2 = alloca i32
    store i32 %b, i32* %2
    %3 = alloca i32
    store i32 %c, i32* %3
    %4 = load i32, i32* %1
    %5 = add i32 0, 0
    %6 = icmp sgt i32 %4, %5
    br i1 %6, label %then1, label %else1

then1:
    %7 = load i32, i32* %2
    %8 = add i32 0, 0
    %9 = icmp sgt i32 %7, %8
    br i1 %9, label %then2, label %else2

then2:
    %10 = load i32, i32* %3
    %11 = add i32 0, 0
    %12 = icmp sgt i32 %10, %11
    br i1 %12, label %then3, label %else3

then3:
    %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i64 0, i64 0))
    br label %ifcont3

else3:
    %14 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.2, i64 0, i64 0))
    br label %ifcont3

ifcont3:
    br label %ifcont2

else2:
    %15 = load i32, i32* %3
    %16 = add i32 0, 0
    %17 = icmp sgt i32 %15, %16
    br i1 %17, label %then4, label %else4

then4:
    %18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i64 0, i64 0))
    br label %ifcont4

else4:
    %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.4, i64 0, i64 0))
    br label %ifcont4

ifcont4:
    br label %ifcont2

ifcont2:
    br label %ifcont1

else1:
    %20 = load i32, i32* %2
    %21 = add i32 0, 0
    %22 = icmp sgt i32 %20, %21
    br i1 %22, label %then5, label %else5

then5:
    %23 = load i32, i32* %3
    %24 = add i32 0, 0
    %25 = icmp sgt i32 %23, %24
    br i1 %25, label %then6, label %else6

then6:
    %26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i64 0, i64 0))
    br label %ifcont6

else6:
    %27 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.6, i64 0, i64 0))
    br label %ifcont6

ifcont6:
    br label %ifcont5

else5:
    %28 = load i32, i32* %3
    %29 = add i32 0, 0
    %30 = icmp sgt i32 %28, %29
    br i1 %30, label %then7, label %else7

then7:
    %31 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.7, i64 0, i64 0))
    br label %ifcont7

else7:
    %32 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.8, i64 0, i64 0))
    br label %ifcont7

ifcont7:
    br label %ifcont5

ifcont5:
    br label %ifcont1

ifcont1:
    ret void
}


declare i32 @atoi(i8*)

declare i32 @printf(i8*, ...)
