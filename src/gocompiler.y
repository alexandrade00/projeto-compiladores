%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "functions.h"
    #include "semantics.h"
    #include "y.tab.h"
    #include "var.h"
    #include "code_generator.h"
    
    int yylex(void);
    void yyerror (const char *s);
    int errorFlag = 0;

    char *flag = "";

    //int yydebug=0;

    is_program* myprogram;
%}

%union {
    int num;
    float numf; 
    char *str;
    id_token* idToken;
    is_program* iProgram;
    is_decl* iDecl;
    is_declaration_list* iDeclList;
    is_vardecl* iVarDecl;
    is_varspec* iVarSpec;
    is_varspec_list* iVarSpecList;
    is_type* iType;
    is_funcdecl* iFuncDecl;
    is_param* iParam;
    is_param_aux* iParamAux;
    is_param_list* iParamList;
    is_param_list_aux* iParamListAux;
    is_type_func* iTypeFunc;
    is_funcbody* iFuncBody;
    is_vars_statements* iVarsStat;
    is_vars_statements_aux* iVarsStatAux;
    is_statement* iStat;
    is_statement_aux1* iStatAux1;
    is_statement_aux2* iStatAux2;
    is_statement_aux3* iStatAux3;
    is_statement_aux5* iStatAux5;
    is_funcinvoc* iFuncInvoc;
    is_funcinvoc_aux1* iFuncInvocAux1;
    is_funcinvoc_aux2* iFuncInvocAux2;
    is_expr1* IExpr1;
    is_expr2* IExpr2;
    is_expr3* IExpr3;
    is_expr4* IExpr4;
    is_expr5* IExpr5;
    is_expr6* IExpr6;
    is_expr_aux1* iExprAux1;
    is_only_expr* iOnlyExpr;
    is_parseargs* iParseArgs;
    is_keyword* iKeyword;
}

%token SEMICOLON
%token COMMA
%token BLANKID
%token<idToken>ASSIGN
%token<idToken>STAR
%token<idToken>DIV
%token<idToken>MINUS
%token<idToken>PLUS
%token<idToken>EQ
%token<idToken>GT
%token<idToken>GE
%token<idToken>LBRACE
%token<idToken>LT
%token<idToken>LE
%token<idToken>LPAR
%token<idToken>LSQ
%token<idToken>MOD
%token<idToken>NOT
%token<idToken>NE
%token<idToken>AND
%token<idToken>OR
%token RBRACE
%token RPAR
%token RSQ
%token PACKAGE
%token<idToken>RETURN
%token<str>ELSE
%token<idToken>FOR
%token<idToken>IF
%token VAR
%token<str>INT
%token<str>FLOAT32
%token<str>BOOL
%token<str>STRING
%token<idToken>PRINT
%token<idToken> PARSEINT
%token FUNC
%token CMDARGS
%token RESERVED
%token<idToken>ID
%token<idToken>INTLIT
%token<idToken>REALLIT
%token<idToken>STRLIT

%type<iProgram>Program
%type<iDeclList>Declarations
%type<iDecl>Declarations_aux
%type<iVarDecl>VarDeclaration
%type<iVarSpec>VarSpec
%type<iVarSpecList>VarSpec_aux1
%type<iType>Type
%type<iFuncDecl>FuncDeclaration
%type<iParamAux>FuncDeclaration_aux1
%type<iTypeFunc>FuncDeclaration_aux2
%type<iParam>Parameters
%type<iParamList>Parameters_aux1
%type<iParamListAux>Parameters_aux2
%type<iFuncBody>FuncBody
%type<iVarsStat>VarsAndStatements
%type<iVarsStatAux>VarsAndStatements_aux
%type<iStat>Statement
%type<iStatAux1>Statement_aux1
%type<iStatAux2>Statement_aux2
%type<iStatAux3>Statement_aux3
%type<iOnlyExpr>Statement_aux4
%type<iStatAux5>Statement_aux5
%type<iParseArgs>ParseArgs
%type<iFuncInvoc>FuncInvocation
%type<iFuncInvocAux1>FuncInvocation_aux1
%type<iFuncInvocAux2>FuncInvocation_aux2
%type<IExpr1>Expr_1
%type<IExpr2>Expr_2
%type<IExpr3>Expr_3
%type<IExpr4>Expr_4
%type<IExpr5>Expr_5
%type<IExpr6>Expr_6
%type<iKeyword>Expr_Or
%type<iKeyword>Expr_And
%type<iKeyword>Expr_Comp
%type<iKeyword>Expr_Add_Sub
%type<iKeyword>Expr_Mul_Div
%type<iKeyword>Expr_Signal
%type<iExprAux1>Expr_aux1

%start Start
%left OR
%left AND
%right EQ NE LT LE GT GE 
%left PLUS MINUS 
%left STAR DIV MOD

%%

Start: Program {;}

Program: PACKAGE ID SEMICOLON Declarations      {$$=myprogram=insert_program($4); if(errorFlag != 1) {check_program(myprogram); print_program(myprogram, 0); generate_program(myprogram);}}

Declarations:                                   {$$=NULL;}  
            | Declarations Declarations_aux     {$$=insert_declaration_list($1, $2);}
            ;

Declarations_aux: VarDeclaration SEMICOLON      {$$=insert_declaration($1, NULL);}
                | FuncDeclaration SEMICOLON     {$$=insert_declaration(NULL, $1);}
                | error                         {$$=NULL; errorFlag = 1;}
                ;

VarDeclaration: VAR VarSpec                     {$$=insert_vardecl($2);} 
              | VAR LPAR VarSpec SEMICOLON RPAR {$$=insert_vardecl($3);}
              ;

VarSpec: ID VarSpec_aux1 Type                   {$$=insert_varspec($1, $2, $3);}

VarSpec_aux1:                                   {$$=NULL;}            
           | VarSpec_aux1 COMMA ID              {$$=insert_varspec_list($1, $3);}
           ;

Type: INT                                       {$$=insert_type($1, "int");}
    | FLOAT32                                   {$$=insert_type($1, "float32");}
    | BOOL                                      {$$=insert_type($1, "bool");}
    | STRING                                    {$$=insert_type($1, "string");}
    ;

FuncDeclaration: FUNC ID LPAR FuncDeclaration_aux1 RPAR FuncDeclaration_aux2 FuncBody   {$$=insert_funcdecl($2, $4, $6, $7);}

FuncDeclaration_aux1:                           {$$=NULL;}
                    | Parameters                {$$=insert_param_aux($1);}
                    ;
    
FuncDeclaration_aux2:                           {$$=NULL;}
                    | Type                      {$$=insert_type_func($1);}
                    ;

Parameters: ID Type Parameters_aux1             {$$=insert_param($1, $2, $3);}

Parameters_aux1:                                    {$$=NULL;}                             
               | Parameters_aux1 Parameters_aux2    {$$=insert_param_list($1, $2);} 
               ;

Parameters_aux2: COMMA ID Type                  {$$=insert_param_list_aux($2, $3);}

FuncBody: LBRACE VarsAndStatements RBRACE       {$$=insert_funcbody($2);}

VarsAndStatements: VarsAndStatements VarsAndStatements_aux SEMICOLON    {$$=insert_vars_statements($1, $2);}
                 |                                                      {$$=NULL;}
                 ;

VarsAndStatements_aux:                          {$$=NULL;}
                     | VarDeclaration           {$$=insert_vars_statements_aux($1, NULL);}
                     | Statement                {$$=insert_vars_statements_aux(NULL, $1);}
                     ;

Statement: ID ASSIGN Expr_1                                         {$$=insert_statement($1, NULL, $3, NULL, NULL, NULL, NULL, NULL, "Assign", $2);}  
         | LBRACE Statement_aux1 RBRACE                             {$$=insert_statement(NULL, $2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);}
         | IF Expr_1 LBRACE Statement_aux1 RBRACE Statement_aux3    {$$=insert_statement(NULL, $4, $2, NULL, NULL, $6, NULL, NULL, "If", $1);}
         | FOR Statement_aux4 LBRACE Statement_aux1 RBRACE          {$$=insert_statement(NULL, $4, NULL, NULL, NULL, NULL, $2, NULL, "For", $1);}    
         | RETURN Statement_aux4                                    {$$=insert_statement(NULL, NULL, NULL, NULL, NULL, NULL, $2, NULL, "Return", $1);}                       
         | FuncInvocation                                           {$$=insert_statement(NULL, NULL, NULL, $1, NULL, NULL, NULL, NULL, NULL, NULL);}     
         | ParseArgs                                                {$$=insert_statement(NULL, NULL, NULL, NULL, $1, NULL, NULL, NULL, NULL, NULL);}
         | PRINT LPAR Statement_aux5 RPAR                           {$$=insert_statement(NULL, NULL, NULL, NULL, NULL, NULL, NULL, $3, "Print", $1);}
         | error                                                    {$$=NULL; errorFlag = 1;}
         ;                     

Statement_aux1:                                 {$$=NULL;}
               | Statement_aux1 Statement_aux2  {$$=insert_statement_aux1($1, $2);}   
               ;

Statement_aux2: Statement SEMICOLON             {$$=insert_statement_aux2($1);}

Statement_aux3:                                     {$$=NULL;}                                    
              | ELSE LBRACE Statement_aux1 RBRACE   {$$=insert_statement_aux3($3);}   
              ;

Statement_aux4:                                 {$$=NULL;}
              | Expr_1                          {$$=insert_only_expr($1);}
              ;

Statement_aux5: Expr_1                          {$$=insert_statement_aux5($1, NULL, -1);}
              | STRLIT                          {$$=insert_statement_aux5(NULL, $1, 3);}
              ;

ParseArgs: ID COMMA BLANKID ASSIGN PARSEINT LPAR CMDARGS LSQ Expr_1 RSQ RPAR  {$$=insert_parseargs($1, $9, $5);}
         | ID COMMA BLANKID ASSIGN PARSEINT LPAR error RPAR                   {$$=NULL; errorFlag = 1;}

FuncInvocation: ID LPAR FuncInvocation_aux1 RPAR    {$$=insert_funcinvoc($1, $3);}   
              | ID LPAR error RPAR                  {$$=NULL; errorFlag = 1;}
              ;

FuncInvocation_aux1:                            {$$=NULL;}
                   | Expr_1 FuncInvocation_aux2 {$$=insert_funcinvoc_aux1($1, $2);}      
                   ;

FuncInvocation_aux2:                                    {$$=NULL;}            
                   | FuncInvocation_aux2 COMMA Expr_1   {$$=insert_funcinvoc_aux2($1, $3);}
                   ;

Expr_1: Expr_1 Expr_Or Expr_2                   {$$=insert_expr1($1, $2, $3);}
      | Expr_2                                  {$$=insert_expr1(NULL, NULL, $1);}
      ;

Expr_2: Expr_2 Expr_And Expr_3                  {$$=insert_expr2($1, $2, $3);}
      | Expr_3                                  {$$=insert_expr2(NULL, NULL, $1);}
      ; 

Expr_3: Expr_3 Expr_Comp Expr_4                 {$$=insert_expr3($1, $2, $3);}
      | Expr_4                                  {$$=insert_expr3(NULL, NULL, $1);}
      ;  

Expr_4: Expr_4 Expr_Add_Sub Expr_5              {$$=insert_expr4($1, $2, $3);}
      | Expr_5                                  {$$=insert_expr4(NULL, NULL, $1);}
      ; 

Expr_5: Expr_5 Expr_Mul_Div Expr_6              {$$=insert_expr5($1, $2, $3);}
      | Expr_6                                  {$$=insert_expr5(NULL, NULL, $1);}
      ;

Expr_6: Expr_Signal Expr_6                      {$$=insert_expr6($2, $1, NULL);}
      | Expr_aux1                               {$$=insert_expr6(NULL, NULL, $1);}
      ;

Expr_Or: OR                                     {$$=insert_keyword($1, "Or");}

Expr_And: AND                                   {$$=insert_keyword($1, "And");}

Expr_Comp: LT                                   {$$=insert_keyword($1, "Lt");}
         | GT                                   {$$=insert_keyword($1, "Gt");}
         | EQ                                   {$$=insert_keyword($1, "Eq");}
         | NE                                   {$$=insert_keyword($1, "Ne");}
         | LE                                   {$$=insert_keyword($1, "Le");}
         | GE                                   {$$=insert_keyword($1, "Ge");}
         ;

Expr_Add_Sub: PLUS                              {$$=insert_keyword($1, "Add");}
            | MINUS                             {$$=insert_keyword($1, "Sub");}
            ;

Expr_Mul_Div: STAR                              {$$=insert_keyword($1, "Mul");}
            | DIV                               {$$=insert_keyword($1, "Div");}
            | MOD                               {$$=insert_keyword($1, "Mod");}
            ;

Expr_Signal: NOT                                {$$=insert_keyword($1, "Not");}
           | MINUS                              {$$=insert_keyword($1, "Minus");}
           | PLUS                               {$$=insert_keyword($1, "Plus");}
           ;
         
Expr_aux1: INTLIT                               {$$=insert_expr_aux1($1, NULL, NULL,1);}
         | REALLIT                              {$$=insert_expr_aux1($1, NULL, NULL,2);}
         | ID                                   {$$=insert_expr_aux1($1, NULL, NULL,0);}
         | FuncInvocation                       {$$=insert_expr_aux1(NULL, $1, NULL,3);}
         | LPAR Expr_1 RPAR                     {$$=insert_expr_aux1(NULL, NULL, $2,3);}
         | LPAR error RPAR                      {$$=NULL; errorFlag = 1;}
         ;


%%
int main(int argc, char **argv) {
    for (int i=0; i<argc; i++) {
        if (strcmp(argv[i], "-l")==0 || strcmp(argv[i], "-t")==0 || strcmp(argv[i], "-s")==0)
        flag = argv[i];
    }
	
    if (strcmp(flag, "-l")==0)
        while(yylex());
    else
        yyparse();

      //free_program(myprogram);
            

      return 0;
}
