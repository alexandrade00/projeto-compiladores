#include "semantics.h"
#include "structures.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "functions.h"
#include "var.h"

int func_num = 0;
char* func_name_semantics;
char* func_type_semantics;
sym_tables* all_tables;
bool error_flag = 0;
bool expr_error = 0;

void check_program(is_program *p) {
    if (p != NULL) {
        all_tables = (sym_tables*)malloc(sizeof(sym_tables));
        table_element** all_symtab = (table_element**)malloc(sizeof(table_element));
        all_tables->symtab = all_symtab;
        check_declaration_list(all_symtab, p->dlist);
        if (error_flag == 0)
            error_flag = unused_variables();
        else
            unused_variables();

        if (error_flag != 0)
            printf("\n");
        if (strcmp(flag, "-s") == 0)
            show_table(all_tables);

        //show_table(all_tables);
    }
}

void check_declaration_list(table_element** all_symtab, is_declaration_list* id) {

    if (id != NULL) {
        for (; id != NULL; id=id->next)
            check_declaration(all_symtab, id->decl);
    }

    sym_tables *temp = all_tables->next;

    for(; temp; temp = temp->next) {
        func_name_semantics = (*temp->func_header)->name->id;
        if ((*temp->func_header)->type != NULL)
            func_type_semantics = (*temp->func_header)->type;
        else
            func_type_semantics = "none";

        check_funcbody(temp->symtab, temp->fb);
    }
    
}

void check_declaration(table_element** all_symtab, is_decl* id) {

    if (id == NULL)
        return;
    
    check_vardecl(all_symtab, id->vd);
    check_funcdecl(all_symtab, id->fd);
    
}

void check_vardecl(table_element** all_symtab, is_vardecl* ivd) {

    if (ivd != NULL)
        check_varspec(all_symtab, ivd->varspec);
}

void check_varspec(table_element** all_symtab, is_varspec* ivs) {

    if (ivs != NULL) {
        check_token(all_symtab, ivs->id, ivs->type,0);
        check_varspec_list(all_symtab, ivs->vslist, ivs->type);
    }
}

void check_varspec_list(table_element** all_symtab, is_varspec_list* ivsl, is_type* type) {

    if (ivsl != NULL) {
        for (; ivsl != NULL; ivsl=ivsl->next)
            check_token(all_symtab, ivsl->id, type,0);
    }
}

void check_funcdecl(table_element** all_symtab, is_funcdecl* fd) {

    if (fd == NULL)
        return;
    
    func_num++;
    table_element** param_list;
    table_element** func_header = (table_element**)malloc(sizeof(table_element));
    *func_header = (table_element*)malloc(sizeof(table_element));
    
    if (fd->param != NULL) {
        param_list = (table_element**)malloc(sizeof(table_element));
        (*param_list) = (table_element*)malloc(sizeof(table_element));
        (*param_list)->name=fd->param->param->id;
        (*param_list)->type=fd->param->param->type->name;
        (*param_list)->params=NULL;
        (*param_list)->is_param=1;
        (*param_list)->is_used=1;
        (*param_list)->next = check_parameter_list(fd->param->param->pmlist);
        if (fd->type!= NULL) {
            if (check_func(all_symtab, fd->id, fd->type->type, param_list, func_type_semantics) == -1)
                return;
            (*func_header)->name=fd->id;
            (*func_header)->type=fd->type->type->name;
            (*func_header)->params=param_list;
        }
        else {
            if (check_func(all_symtab, fd->id, NULL, param_list, func_type_semantics) == -1)
                return;
            (*func_header)->name=fd->id;
            (*func_header)->params=param_list;
        }
    } else if (fd->id != NULL && fd->type != NULL) {
        if (check_func(all_symtab, fd->id, fd->type->type, NULL, func_type_semantics) == -1)
            return;
        (*func_header)->name=fd->id;
        (*func_header)->type=fd->type->type->name;
    }
    else if (fd->id != NULL) {
        if (check_func(all_symtab, fd->id, NULL, NULL, func_type_semantics) == -1)
            return;
        (*func_header)->name=fd->id;
    }
    
    table_element** symtab = (table_element**)malloc(sizeof(table_element));
    sym_tables* previous;
    sym_tables* temp;
    
    for (temp = all_tables; temp; previous=temp, temp=temp->next);
    temp = (sym_tables*)malloc(sizeof(sym_tables));
    previous->next=temp;
    temp->symtab = symtab;
    temp->func_header=func_header;
    temp->fb=fd->funcbody;

    
    if (fd->param != NULL) {
        id_token* id;
        is_type* type;
        while (*param_list != NULL) {
            id = (id_token*)malloc(sizeof(id_token));
            type = (is_type*)malloc(sizeof(is_type));
            id = (*param_list)->name;
            type->name = (*param_list)->type;
            type->type = (*param_list)->type;
            check_token(symtab, id, type, 1);
            param_list = &(*param_list)->next;
        }
    }

    //check_funcbody(symtab, fd->funcbody);
}

void check_funcbody(table_element** all_symtab, is_funcbody* fb) {

    if (fb == NULL)
        return;

    check_vars_stat(all_symtab, fb->vars_list);
}

void check_vars_stat(table_element** all_symtab, is_vars_statements* ivs) {

    if (ivs == NULL)
        return;

    for(; ivs != NULL; ivs = ivs->next)
        check_vars_stat_aux(all_symtab, ivs->ivsa);
}

void check_vars_stat_aux(table_element** all_symtab, is_vars_statements_aux* ivsa) {

    if (ivsa == NULL)
        return;
    
    check_vardecl(all_symtab, ivsa->vdlist);
    check_statement(all_symtab, ivsa->statement_list);
}

void check_func_invoc(table_element** all_symtab, is_funcinvoc* ifi) {

    if (ifi == NULL)
        return;

    expr_error = 0;
    if (ifi->invoc_list != NULL) {
        if (ifi->invoc_list->expr != NULL)
            check_expr1(all_symtab, ifi->invoc_list->expr);
        if (ifi->invoc_list->fiaux2 != NULL) {
            is_funcinvoc_aux2* temp = ifi->invoc_list->fiaux2;
            for (; temp; temp = temp->next)
                if (temp->expr != NULL)
                    check_expr1(all_symtab, temp->expr);
        }
    }

    if (expr_error == 1)
        return;

    if (ifi->invoc_list == NULL) {
        table_element* aux = search_func(ifi->id);
        if (aux == NULL) {
            error_flag = 1;
            expr_error = 1;
            printf("Line %d, column %d: Cannot find symbol %s()\n", ifi->id->line, ifi->id->col, ifi->id->id);
        } else {
            if (aux->params != NULL) {
                error_flag = 1;
                expr_error = 1;
                printf("Line %d, column %d: Cannot find symbol %s()\n", ifi->id->line, ifi->id->col, ifi->id->id);
            }
        }
    } else {
        table_element* aux = search_func(ifi->id);
    
        if (aux == NULL) {
            error_flag = 1;
            expr_error = 1;
            printf("Line %d, column %d: Cannot find symbol %s(", ifi->id->line, ifi->id->col, ifi->id->id);
            check_funcinvoc_aux1(all_symtab, ifi->invoc_list, ifi->id->line, ifi->id->col, ifi->id->id);
        } else {
            if(same_params_from_call_and_func(ifi->invoc_list, aux, func_name_semantics, func_type_semantics) == false) {
                error_flag = 1;
                expr_error = 1;
                printf("Line %d, column %d: Cannot find symbol %s(", ifi->id->line, ifi->id->col, ifi->id->id);

                is_funcinvoc_aux2* temp = ifi->invoc_list->fiaux2;
                operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
                *op_type = (operation_type*)malloc(sizeof(operation_type));

                check_type_expr1(ifi->invoc_list->expr, func_name_semantics, func_type_semantics, op_type, 1);
                printf("%s", (*op_type)->var_type);
                if (ifi->invoc_list->fiaux2 != NULL)
                    printf(",");

                for (; temp; temp = temp->next) {
                    check_type_expr1(temp->expr, func_name_semantics, func_type_semantics, op_type, 1);
                    printf("%s", (*op_type)->var_type);
                    
                    if (temp->next != NULL)
                        printf(",");
                }
                printf(")\n");

                free(*op_type);
                free(op_type);
            }
        }
    }
}

void check_funcinvoc_aux1(table_element** all_symtab, is_funcinvoc_aux1* ifi1, int row, int col, char* name) {

    if (ifi1 == NULL)
        return;

    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    check_type_expr1(ifi1->expr, func_name_semantics, func_type_semantics, op_type, 0);
    if (ifi1->fiaux2 != NULL) {
        printf("%s,", (*op_type)->var_type);
        check_funcinvoc_aux2(all_symtab, ifi1->fiaux2);
    } else {
        printf("%s)\n", (*op_type)->var_type);
    }
    free(op_type);
}

void check_funcinvoc_aux2(table_element** all_symtab, is_funcinvoc_aux2* ifi2) {

    if (ifi2 == NULL)
        return;

    is_funcinvoc_aux2* temp = ifi2;
    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    for(; temp != NULL; temp = temp->next) {
        check_type_expr1(temp->expr, func_name_semantics, func_type_semantics, op_type, 0);
        printf("%s", (*op_type)->var_type);
        if (temp->next != NULL)
            printf(",");
    }
    printf(")\n");
    free(op_type);
}

void check_parseargs(table_element** all_symtab, is_parseargs* ipa) {

    if (ipa == NULL)
        return;

    check_expr1(all_symtab, ipa->exp);
    check_id(ipa->id);

    if (ipa->id != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));

        table_element* aux_elem = search_el(ipa->id, func_name_semantics);
        check_type_expr1(ipa->exp, func_name_semantics, func_type_semantics, op_type, 1);

        char* type = NULL;
        char* aux_type = NULL;
        
        if (aux_elem != NULL)
            type = aux_elem->type;
       
        aux_type = (*op_type)->var_type;

        if (type == NULL)
            type = "undef";
        if (aux_type == NULL)
            aux_type = "undef";

        if (strcmp(type, "int") == 0 && strcmp(aux_type, "int") == 0)
            return;

        error_flag = 1;
        printf("Line %d, column %d: Operator strconv.Atoi cannot be applied to types %s, %s\n", ipa->op->line, ipa->op->col, type, aux_type); 
    }
}


void check_statement(table_element** all_symtab, is_statement* is) {

    if (is == NULL)
        return;
    
    check_aux1(all_symtab, is->aux1);
    check_aux3(all_symtab, is->aux3);
    check_aux4(all_symtab, is->aux4, is->line, is->col, is->kw);
    check_aux5(all_symtab, is->aux5, is->line, is->col);
    check_expr1(all_symtab, is->expr);
    check_func_invoc(all_symtab, is->funcinvoc);
    check_parseargs(all_symtab, is->parseargs);
    check_id(is->id);

    if(is->kw != NULL){
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr1(is->expr,func_name_semantics, func_type_semantics, op_type, 1);
        if((*op_type)->var_type != NULL){
            if(strcmp(is->kw,"If")==0){
                if(strcmp((*op_type)->op_type,"bool")!=0 && strcmp((*op_type)->var_type, "bool") != 0) {
                    printf("Line %d, column %d: Incompatible type %s in if statement\n", is->line, is->col+1, (*op_type)->var_type);
                    error_flag = 1;
                }
            }
        }
    }

    if (is->id != NULL) {
        char* type = NULL;
        table_element* aux_elem = search_el(is->id, func_name_semantics);
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));

        if (aux_elem != NULL) {
            if (aux_elem->is_func == 1) {
                if (aux_elem->type != NULL && aux_elem->return_type != NULL && strcmp(aux_elem->type, aux_elem->return_type) == 0 && strcmp(aux_elem->return_type, func_type_semantics) == 0)
                    type = aux_elem->type;
            } else
                type = aux_elem->type;
        }

        //printf("procura\n");
        check_type_expr1(is->expr, func_name_semantics, func_type_semantics, op_type, 1);
        
        char* aux_type = (*op_type)->op_type; 

        if (type == NULL)
            type = "undef";
        if (aux_type == NULL)
            aux_type = "undef";

        if (strcmp(aux_type, "int_float32") == 0)
            aux_type = (*op_type)->var_type;

        //printf("%s - %s %s\n", is->id->id, type, aux_type);

        if (strcmp(aux_type, "int_float32_signal") == 0 && (strcmp(type, "int") == 0 || strcmp(type, "float32") == 0 || strcmp(type, "bool") == 0))
            return;

        /* if (strcmp(type, "float32") == 0 && strcmp(aux_type, "int") == 0)
            return; */
        
        if (strcmp(aux_type, type) != 0 || strcmp(aux_type,"undef") == 0) {
            printf("Line %d, column %d: Operator = cannot be applied to types %s, %s\n", is->line, is->col, type, aux_type);
            error_flag = 1;
        } 
    }
}

void check_aux1(table_element** all_symtab,is_statement_aux1* aux){
    if (aux == NULL)
        return;

    is_statement_aux1 *temp = aux;
    
    for (; temp != NULL; temp = temp->next)
        check_aux2(all_symtab, temp->saux2);

}

void check_aux2(table_element** all_symtab, is_statement_aux2* aux) {
    if (aux == NULL)
        return;
    
    check_statement(all_symtab, aux->stat);
}

void check_aux3(table_element** all_symtab, is_statement_aux3* aux){
    if (aux == NULL)
        return;
    
    check_aux1(all_symtab, aux->saux1);
}

void check_aux4(table_element** all_symtab, is_only_expr* aux, int line, int col, char* kw) {
    
    if (aux == NULL)
        return;

    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr1(aux->expr,func_name_semantics, func_type_semantics, op_type, 0);
    
    if (strcmp(kw, "For") == 0) {
        if((strcmp((*op_type)->var_type,"int")==0 || strcmp((*op_type)->var_type,"float32")==0) && strcmp((*op_type)->op_type, "bool") != 0) {
            error_flag = 1;
            printf("Line %d, column %d: Incompatible type %s in for statement\n", line, col+4, (*op_type)->var_type);
        }
    } else if (strcmp(kw, "Return") == 0) {
        char* return_type = return_all_types_from_expr(aux->expr, func_name_semantics, func_type_semantics, 1)->var_type;
        if (strcmp(return_type, func_type_semantics) != 0) {
            error_flag = 1;
            printf("Line %d, column %d: Incompatible type %s in return statement\n", line, col+1, return_type);
        }
    }
    
    check_expr1(all_symtab, aux->expr);
}

void check_aux5(table_element** all_symtab, is_statement_aux5* aux, int line, int col) {

    if (aux == NULL)
        return;

    if (aux->type->name != NULL) {
        if (strcmp(aux->type->name, "undef") == 0) {
            error_flag = 1;
            printf("Line %d, column %d: Incompatible type %s in fmt.Println statement\n", line, col+1, aux->type->name);
        }
    } else {
        char* return_type = return_all_types_from_expr(aux->exp, func_name_semantics, func_type_semantics, 1)->var_type;
        if (strcmp(return_type, "undef") == 0) {
            error_flag = 1;
            printf("Line %d, column %d: Incompatible type %s in fmt.Println statement\n", line, col+1, return_type);
        }
    }
    
    check_expr1(all_symtab, aux->exp);
    //check_id(aux->id);
}

void check_expr1(table_element** all_symtab, is_expr1* aux){
    if (aux == NULL)
        return;

    is_expr1* temp = aux;

    for (; temp != NULL; temp = temp->next)
        check_expr2(all_symtab, temp->expr2);

    if (aux->next != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        
        check_type_expr1(aux, func_name_semantics, func_type_semantics, op_type, 0);
        char* aux_type = (*op_type)->var_type;
        
        if (aux_type != NULL && strcmp(aux_type, "undef") != 0) {

            if (strcmp(aux_type, "bool") == 0)
                return;

            error_flag = 1;
            printf("Line %d, column %d: Operator || cannot be applied to types %s, %s\n", aux->expr_or->keyword->line, aux->expr_or->keyword->col, aux_type, aux_type);
        }

        else {
            
            char* aux_type_2 = "undef";
            check_type_expr1(aux->next, func_name_semantics, func_type_semantics, op_type, 0);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type = "bool";
            else
                aux_type = (*op_type)->var_type;

            check_type_expr2(aux->expr2, func_name_semantics, func_type_semantics, op_type, 0);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type_2 = "bool";
            else
                aux_type_2 = (*op_type)->var_type;

            if (strcmp(aux_type, "bool") == 0 && strcmp(aux_type_2, "bool") == 0)
                return;

            error_flag = 1;
            printf("Line %d, column %d: Operator || cannot be applied to types %s, %s\n", aux->expr_or->keyword->line, aux->expr_or->keyword->col, aux_type, aux_type_2);
        }
    }

}

void check_expr2(table_element** all_symtab, is_expr2* aux){

    if (aux == NULL)
        return;

    is_expr2* temp = aux;

    for (; temp != NULL; temp = temp->next)
        check_expr3(all_symtab, temp->expr3);
    
    if (aux->next != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr2(aux, func_name_semantics, func_type_semantics, op_type, 0);
        char* aux_type = (*op_type)->var_type;
        
        if (aux_type != NULL && strcmp(aux_type, "undef") != 0) {
            
            if (strcmp(aux_type, "bool") == 0)
                return;

            error_flag = 1;
            printf("Line %d, column %d: Operator && cannot be applied to types %s, %s\n", aux->expr_and->keyword->line, aux->expr_and->keyword->col, aux_type, aux_type);
        } else {
            char* aux_type_2 = "undef";
            check_type_expr2(aux->next, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type = "bool";
            else
                aux_type = (*op_type)->var_type;

            check_type_expr3(aux->expr3, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type_2 = "bool";
            else
                aux_type_2 = (*op_type)->var_type;

            if (strcmp(aux_type, "bool") == 0 && strcmp(aux_type_2, "bool") == 0)
                return;

            error_flag = 1;
            printf("Line %d, column %d: Operator && cannot be applied to types %s, %s\n", aux->expr_and->keyword->line, aux->expr_and->keyword->col, aux_type, aux_type_2);
        }
    }
}

void check_expr3(table_element** all_symtab, is_expr3* aux){
    if (aux == NULL)
        return;

    is_expr3* temp = aux;

    for (; temp != NULL; temp = temp->next)
        check_expr4(all_symtab, temp->expr4);
    
    if (aux->next != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr3(aux, func_name_semantics, func_type_semantics, op_type, 0);
        char* aux_type = (*op_type)->var_type;
        
        char* operator;

        if (strcmp(aux->expr_comp->op, "Lt") == 0)
            operator = "<";
        else if (strcmp(aux->expr_comp->op, "Gt") == 0)
            operator = ">";
        else if (strcmp(aux->expr_comp->op, "Ge") == 0)
            operator = ">=";
        else if (strcmp(aux->expr_comp->op, "Le") == 0)
            operator = "<=";
        else if (strcmp(aux->expr_comp->op, "Eq") == 0)
            operator = "==";
        else if (strcmp(aux->expr_comp->op, "Ne") == 0)
            operator = "!=";

        if (aux_type == NULL)
            aux_type = "undef";

        if (strcmp(aux_type, "int") == 0 || strcmp(aux_type, "float32") == 0)
            return;
        
        if (strcmp(aux_type, "undef") != 0) {

            if (strcmp(aux_type, "bool") == 0 && (strcmp(operator, "==") == 0 || strcmp(operator, "!=") == 0))
                return;

            error_flag = 1;
            printf("Line %d, column %d: Operator %s cannot be applied to types %s, %s\n", aux->expr_comp->keyword->line, aux->expr_comp->keyword->col, operator, aux_type, aux_type);
        } else {
            char* aux_type_2 = "undef";
            check_type_expr3(aux->next, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type = "bool";
            else
                aux_type = (*op_type)->var_type;

            check_type_expr4(aux->expr4, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type_2 = "bool";
            else
                aux_type_2 = (*op_type)->var_type;

            if (strcmp(aux_type, "bool") == 0 && strcmp(aux_type_2, "bool") == 0 && (strcmp(operator, "==") == 0 || strcmp(operator, "!=") == 0))
                return;

            if ((strcmp(aux_type, "int") == 0 && strcmp(aux_type_2, "int") == 0) || (strcmp(aux_type, "float32") == 0 && strcmp(aux_type_2, "float32") == 0))
                return;

            error_flag = 1;
            printf("Line %d, column %d: Operator %s cannot be applied to types %s, %s\n", aux->expr_comp->keyword->line, aux->expr_comp->keyword->col, operator, aux_type, aux_type_2);
        } 
    }
}

void check_expr4(table_element** all_symtab, is_expr4* aux){
    if (aux == NULL)
        return;

    is_expr4* temp = aux;

    for (; temp != NULL; temp = temp->next)
        check_expr5(all_symtab, temp->expr5);
        
    if (aux->next != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr4(aux, func_name_semantics, func_type_semantics, op_type, 0);
        char* aux_type = (*op_type)->var_type;
        
        char* operator;

        if (strcmp(aux->expr_add_sub->op, "Add") == 0)
            operator = "+";
        else if (strcmp(aux->expr_add_sub->op, "Sub") == 0)
            operator = "-";

        if (aux_type == NULL)
            aux_type = "undef";

        if (strcmp(aux_type, "int") == 0 || strcmp(aux_type, "float32") == 0)
            return;

        if (strcmp(aux_type, "string") == 0 && strcmp(operator, "+") == 0)
            return;    
        
        if (aux_type != NULL && strcmp(aux_type, "undef") != 0) {
            error_flag = 1;
            printf("Line %d, column %d: Operator %s cannot be applied to types %s, %s\n", aux->expr_add_sub->keyword->line, aux->expr_add_sub->keyword->col, operator, aux_type, aux_type);
        } else {
            char* aux_type_2 = "undef";
            check_type_expr4(aux->next, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type = "bool";
            else
                aux_type = (*op_type)->var_type;

            check_type_expr5(aux->expr5, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type_2 = "bool";
            else
                aux_type_2 = (*op_type)->var_type;

            if (strcmp(aux_type, aux_type_2) == 0 && (strcmp(aux_type, "int") == 0 || strcmp(aux_type, "float32") == 0))
                return;

            if (strcmp(aux_type, "string") == 0 && strcmp(aux_type_2, "string") == 0 && strcmp(operator, "+") == 0)
                return;

            error_flag = 1;
            printf("Line %d, column %d: Operator %s cannot be applied to types %s, %s\n", aux->expr_add_sub->keyword->line, aux->expr_add_sub->keyword->col, operator, aux_type, aux_type_2);
        } 
    }
}

void check_expr5(table_element** all_symtab, is_expr5* aux){
    if (aux == NULL)
        return;

    is_expr5* temp = aux;

    for (; temp != NULL; temp = temp->next)
        check_expr6(all_symtab, temp->expr6);

    if (aux->next != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr5(aux, func_name_semantics, func_type_semantics, op_type, 1);
        char* aux_type = (*op_type)->var_type;
        
        char* operator;

        if (strcmp(aux->expr_mul_div->op, "Mul") == 0)
            operator = "*";
        else if (strcmp(aux->expr_mul_div->op, "Div") == 0)
            operator = "/";
        else if (strcmp(aux->expr_mul_div->op, "Mod") == 0)
            operator = "//";

        if (aux_type == NULL)
            aux_type = "undef";

        if (strcmp(aux_type, "int") == 0 || strcmp(aux_type, "float32") == 0)
            return;
        
        if (aux_type != NULL && strcmp(aux_type, "undef") != 0) {
            error_flag = 1;
            printf("Line %d, column %d: Operator %s cannot be applied to types %s, %s\n", aux->expr_mul_div->keyword->line, aux->expr_mul_div->keyword->col, operator, aux_type, aux_type);
        } else {
            char* aux_type_2 = "undef";
            check_type_expr5(aux->next, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type = "bool";
            else
                aux_type = (*op_type)->var_type;

            check_type_expr6(aux->expr6, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type_2 = "bool";
            else
                aux_type_2 = (*op_type)->var_type;

            error_flag = 1;
            printf("Line %d, column %d: Operator %s cannot be applied to types %s, %s\n", aux->expr_mul_div->keyword->line, aux->expr_mul_div->keyword->col, operator, aux_type, aux_type_2);
        }
    }    
}

void check_expr6(table_element** all_symtab, is_expr6* aux){

    if (aux == NULL){
        return;
    }

    is_expr6* temp = aux;

    for (; temp != NULL; temp = temp->next){
        check_expr_aux1(all_symtab, temp->expr_aux_1);
    }
    if (aux->next != NULL) { // Nao entra neste bloco
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr6(aux, func_name_semantics, func_type_semantics, op_type, 1);
        char* aux_type = (*op_type)->var_type;
        char* operator;

        if (strcmp(aux->expr_signal->op, "Not") == 0)
            operator = "!";
        else if (strcmp(aux->expr_signal->op, "Minus") == 0)
            operator = "-";
        else if (strcmp(aux->expr_signal->op, "Plus") == 0)
            operator = "+";


        if (aux_type == NULL)
            aux_type = "undef";

        if ((strcmp(aux_type, "int") == 0 || strcmp(aux_type, "float32") == 0) && strcmp(operator, "!") != 0)
            return;

        if (strcmp(aux_type, "bool") == 0 && strcmp(operator, "!") == 0)
            return;
        
        if (strcmp(aux_type, "undef") != 0) {
            if (strcmp(operator, "!") == 0 && strcmp(aux_type, "int") == 0)
                expr_error = 1;
            error_flag = 1;
            printf("Line %d, column %d: Operator %s cannot be applied to type %s\n", aux->expr_signal->keyword->line, aux->expr_signal->keyword->col, operator, aux_type);
        } else {
            char* aux_type_2 = "undef";
            check_type_expr6(aux->next, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type = "bool";
            else
                aux_type = (*op_type)->var_type;

            check_type_expr_aux(aux->expr_aux_1, func_name_semantics, func_type_semantics, op_type, 1);
            if (strcmp("bool",(*op_type)->op_type) == 0)
                aux_type_2 = "bool";
            else
                aux_type_2 = (*op_type)->var_type;

            if ((strcmp(aux_type_2, "int") == 0 || strcmp(aux_type_2, "float32") == 0) && strcmp(operator, "!") != 0)
                return;

            if (strcmp(aux_type_2, "bool") == 0 && strcmp(operator, "!") == 0)
                return;

            error_flag = 1;
            printf("Line %d, column %d: Operator %s cannot be applied to type %s\n", aux->expr_signal->keyword->line, aux->expr_signal->keyword->col, operator, aux_type_2);
        }
    }
}

void check_expr_aux1(table_element** all_symtab, is_expr_aux1* aux){
    
    if (aux == NULL)
        return;

    check_id(aux->id);
    check_expr1(all_symtab, aux->expr);
    check_func_invoc(all_symtab, aux->funcinvoc);
}



table_element* check_parameter_list(is_param_list* ipl) {

    is_param_list* aux;
    table_element* head = NULL;
    table_element* aux_param;
    table_element* param;

    for (aux = ipl; aux != NULL; aux = aux->next) {
        if (aux->param->type->name != NULL) {
            param = (table_element*)malloc(sizeof(table_element));
            param->type=aux->param->type->name;
            param->name=aux->param->id;
            param->is_param = 1;
            param->is_used=1;
            param->params=NULL;
            param->next=NULL;

            if (head==NULL)
                head = param;
            else {
                for(aux_param=head; aux_param->next; aux_param=aux_param->next);
                aux_param->next=param;
            }
        }
    }
    return head;
}

void check_token(table_element** all_symtab, id_token* id, is_type* type, int i) {

    table_element* new_elem;

    if (id != NULL) {
        if (type != NULL){
            new_elem = insert_el(all_symtab, id, type->name,i);
        }
        else
            new_elem = insert_el(all_symtab, id, NULL,i);

        if (new_elem == NULL) {
            if(!isdigit(*id->id)) {
                error_flag = 1;
                printf("Line %d, column %d: Symbol %s already defined\n", id->line, id->col, id->id);
            }
            return;
        }
    }
}

int check_func(table_element** all_symtab, id_token* id, is_type* type, table_element** params, char* return_type) {

    table_element *new_elem;

    if (id != NULL) {
        if (params != NULL && type != NULL) {
            new_elem = insert_func(all_symtab, id, type->name, params, return_type);
        } else if (params != NULL && type == NULL)
            new_elem = insert_func(all_symtab, id, NULL, params, return_type);
        else if (type != NULL && params == NULL)
            new_elem = insert_func(all_symtab, id, type->name, NULL, return_type);
        else
            new_elem = insert_func(all_symtab, id, NULL, NULL, return_type);


        if (new_elem == NULL) {
            error_flag = 1;
            printf("Line %d, column %d: Symbol %s already defined\n", id->line, id->col, id->id);
            return -1;
        }
    }
    return 0;
}

void check_id(id_token* id) {

    if (id == NULL || isdigit(id->id[0]) || id->id[0] == '.')
        return;

    table_element* aux = search_el(id, func_name_semantics);

    if (aux == NULL) {
        error_flag = 1;
        printf("Line %d, column %d: Cannot find symbol %s\n", id->line, id->col, id->id);
    }
}
