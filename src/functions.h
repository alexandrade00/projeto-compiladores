#include "structures.h"
#include "symbol_table.h"
#include "code_generator.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

extern strings_array** string_array;

id_token *create_token(char *id, int line, int col);
is_program* insert_program(is_declaration_list* idl);
is_declaration_list* insert_declaration_list(is_declaration_list* head, is_decl* decl);
is_decl* insert_declaration(is_vardecl* vd, is_funcdecl* fdl);
is_vardecl* insert_vardecl(is_varspec* vspec);
is_varspec* insert_varspec(id_token* id, is_varspec_list* list, is_type* type);
is_varspec_list* insert_varspec_list(is_varspec_list* head, id_token* id);
is_type* insert_type(char* type, char* name);
is_funcdecl* insert_funcdecl(id_token* id, is_param_aux *param, is_type_func *type, is_funcbody *func);
is_param_aux* insert_param_aux(is_param* funcParam);
is_param* insert_param(id_token* id, is_type* type, is_param_list* pmlist);
is_type_func* insert_type_func(is_type* type);
is_param_list* insert_param_list(is_param_list*head, is_param_list_aux* param);
is_param_list_aux* insert_param_list_aux(id_token* id, is_type* type);
is_funcbody* insert_funcbody(is_vars_statements* varsstat);
is_vars_statements_aux* insert_vars_statements_aux(is_vardecl* vd, is_statement* stat);
is_vars_statements* insert_vars_statements(is_vars_statements* head, is_vars_statements_aux* ivsa);
is_statement* insert_statement(id_token* id, is_statement_aux1* statlist, is_expr1* expr, is_funcinvoc* fi, is_parseargs* pa, is_statement_aux3* aux3, is_only_expr* aux4, is_statement_aux5* aux5, char* kw, id_token* pos);
is_statement_aux1* insert_statement_aux1(is_statement_aux1* head, is_statement_aux2* state2);
is_statement_aux2* insert_statement_aux2(is_statement* stat);
is_statement_aux3* insert_statement_aux3(is_statement_aux1* stat1);
is_statement_aux5* insert_statement_aux5(is_expr1* expr, id_token* id, int aux);
is_expr1* insert_expr1(is_expr1* head, is_keyword* kw, is_expr2* expr2);
is_expr2* insert_expr2(is_expr2* head, is_keyword* kw, is_expr3* expr3);
is_expr3* insert_expr3(is_expr3* head, is_keyword* kw, is_expr4* expr4);
is_expr4* insert_expr4(is_expr4* head, is_keyword* kw, is_expr5* expr5);
is_expr5* insert_expr5(is_expr5* head, is_keyword* kw, is_expr6* expr6);
is_expr6* insert_expr6(is_expr6* head, is_keyword* kw, is_expr_aux1* expr_aux_1);
is_expr_aux1* insert_expr_aux1(id_token* id, is_funcinvoc* fi, is_expr1* exp1, int num);
is_only_expr* insert_only_expr(is_expr1* expr);
is_funcinvoc* insert_funcinvoc(id_token* id, is_funcinvoc_aux1* func_list);
is_funcinvoc_aux1* insert_funcinvoc_aux1(is_expr1* expr, is_funcinvoc_aux2* fiaux2);
is_funcinvoc_aux2* insert_funcinvoc_aux2(is_funcinvoc_aux2* head, is_expr1* expr);
is_parseargs* insert_parseargs(id_token* id, is_expr1* exp, id_token* op);
is_keyword* insert_keyword(id_token* kw, char* op);
is_only_expr* insert_only_expr(is_expr1* expr);

void print_parseargs(is_parseargs* pa, int depth);
void print_only_expr(is_only_expr* st4, int depth);
void print_char(char* str, int depth, int aux, char* type);
void print_program(is_program* ip, int depth);
void print_declaration_list(is_declaration_list* dl, int depth);
void print_declaration(is_decl* decl, int depth);
void print_vardecl(is_vardecl* vdl, int depth);
void print_varspec(is_varspec* vs, int depth);
void print_varspec_list(is_varspec_list* vslist, is_type* type, int depth);
void print_funcdecl(is_funcdecl* ifd, int depth);
void print_type(is_type* tp, int depth);
void print_type_func(is_type_func* itf, int depth);
void print_id(id_token* id, int depth, int aux, char* type);
void print_param_aux(is_param_aux* ipa, int depth);
void print_param(is_param* ip, int depth);
void print_param_list(is_param_list* ipl, int depth);
void print_param_list_aux(is_param_list_aux* ipla, int depth);
void print_funcbody(is_funcbody* ifb, int depth);
void print_vars_list(is_vars_statements* ivs, int depth);
void print_vars_list_aux(is_vars_statements_aux* ivsa, int depth);
void print_statement(is_statement* is, int depth);
void print_statement_aux1(is_statement_aux1* is_st, int depth);
void print_statement_aux2(is_statement_aux2* is_st2, int depth);
void print_statement_aux3(is_statement_aux3* is_st3, int depth);
void print_statement_aux5(is_statement_aux5* is_st5, int depth, int aux);
void print_func_inv(is_funcinvoc* finv, int depth);
void print_func_inv_aux1(is_funcinvoc_aux1* is_fc1, int depth);
void print_func_inv_aux2(is_funcinvoc_aux2* is_fc2, int depth);
void print_expr1(is_expr1* expr1, int depth);
void print_expr2(is_expr2* expr2, int depth);
void print_expr3(is_expr3* expr3, int depth);
void print_expr4(is_expr4* expr4, int depth);
void print_expr5(is_expr5* expr5, int depth);
void print_expr6(is_expr6* expr6, int depth);
void print_expr_aux1(is_expr_aux1* exp_aux_1, int depth);
void print_keyword(is_keyword* kw, int depth, char* type);


void free_declaration_list(is_declaration_list *idl);
void free_declaration(is_decl *id);
void free_vardec(is_vardecl *ivd);
void free_varspec(is_varspec *ivs);
void free_spec_list(is_varspec_list *ivl);
void free_varspec_type(is_type *it);
void free_func_decl(is_funcdecl* fd);
void free_type_func(is_type_func* it);
void free_parameters(is_param* ip);
void free_parameters_aux1(is_param_list *ip);
void free_parameters_aux2(is_param_list_aux *ipl);
void free_funcbody(is_funcbody* fb);
void free_vars_statements(is_vars_statements* ivs);
void free_vars_statements_aux(is_vars_statements_aux* ivsa);
void free_statement_list(is_statement* is);
void free_statement_aux1(is_statement_aux1* is1);
void free_statement_aux2(is_statement_aux2* is2);
void free_statement_aux3(is_statement_aux3* is3);
void free_statement_aux4(is_only_expr* expr);
void free_statement_aux5(is_statement_aux5* is5);
void free_parseargs(is_parseargs* ip);
void free_expr1(is_expr1* exp);
void free_expr2(is_expr2* exp);
void free_expr3(is_expr3* exp);
void free_expr4(is_expr4* exp);
void free_expr5(is_expr5* exp);
void free_expr6(is_expr6* exp);
void free_expr_aux1(is_expr_aux1* iea);
void free_id(id_token* is);
void free_func_Inv(is_funcinvoc* inv);
void free_func_inv_aux2(is_funcinvoc_aux2* f_inv_2);
void free_func_inv_aux1(is_funcinvoc_aux1* func_inv_aux1);
void free_func_inv(is_funcinvoc* func_inv);

int count_num_stat(is_statement_aux1* is);



void free_program(is_program* ip);
void free_declaration_list(is_declaration_list *idl);
void free_declaration(is_decl *id);
void free_vardec(is_vardecl *ivd);
void free_varspec(is_varspec *ivs);
void free_spec_list(is_varspec_list *ivl);
void free_type(is_type *it);
void free_func_decl(is_funcdecl* fd);
void free_type_func(is_type_func* it);
void free_parameters(is_param* ip);
void free_parameters_aux1(is_param_list *ip);
void free_parameters_aux2(is_param_list_aux *ipl);
void free_funcbody(is_funcbody* fb);
void free_vars_statements(is_vars_statements* ivs);
void free_vars_statements_aux(is_vars_statements_aux* ivsa);
void free_statement_list(is_statement* is);
void free_statement_aux1(is_statement_aux1* is1);
void free_statement_aux2(is_statement_aux2* is2);
void free_statement_aux3(is_statement_aux3* is3);
void free_statement_aux4(is_only_expr* expr);
void free_statement_aux5(is_statement_aux5* is5);


// --------------------RETURN VAR TYPE FROM EXPR----------------------
operation_type* return_all_types_from_expr(is_expr1* expr, char* funcname, char* func_type, bool get_wrong_types);
void check_type_expr1(is_expr1* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types);
void check_type_expr2(is_expr2* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types);
void check_type_expr3(is_expr3* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types);
void check_type_expr4(is_expr4* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types);
void check_type_expr5(is_expr5* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types);
void check_type_expr6(is_expr6* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types);
void check_type_expr_aux (is_expr_aux1* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types);
void check_func_invoc_expr(is_funcinvoc_aux2* fiaux2, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types);


//----------------------FUNC INVOC UTILS---------------------------------
bool same_params_from_call_and_func(is_funcinvoc_aux1* call_func, table_element* table_func, char* funcname, char* func_type);
char* get_return_type_func(is_funcbody* funcbody, char* funcname, char* func_type);


//--------------------------REGISTERS TABLE-------------------------
void add_reg_table(table** head, int reg, char* var_name, char* var_type);
table* search_reg_table(table* head, int reg);
int search_reg_table_by_var_name(table* head, char* var_name);
void free_table(table** reg_table);


//-------------------------STRINGS AUXILIARIES-----------------------
void insert_string_array(strings_array** head, char* string);

void insert_string_list(str_tams** head, int str_num, int tam, char* str_name);
int search_string_tam(str_tams* head, int str_num);
int search_string_num(str_tams* head, char* str);
