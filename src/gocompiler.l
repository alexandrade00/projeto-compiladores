%{
#include <string.h>
#include "structures.h"
#include "functions.h"
#include "var.h"
#include "y.tab.h"

int rows=1, columns=1, rows_aux=0, start_column=1, need_semicolon=0;
%}

letter					[A-Za-z_]
decimal_digit			[0-9]
octal_digit				[0-7]
hex_digit				[0-9A-Fa-f]
exponent				("e"|"E")("+"|"-")?{decimal_digit}+
escape_char             \\(f|n|r|t|\\|\")

%X OCTAL STRINGL COMMENT DETECT_ESCAPE_CHAR SEMICOLONL END_OF_FILE CHECK_END_OF_FILE
%%
";"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "SEMICOLON\n"); columns+=yyleng; return SEMICOLON;}
","                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "COMMA\n"); columns+=yyleng; return COMMA;}
"_"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "BLANKID\n"); columns+=yyleng; return BLANKID;}
"="                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "ASSIGN\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return ASSIGN;}
"*"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "STAR\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return STAR;}
"/"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "DIV\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return DIV;}
"-"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "MINUS\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return MINUS;}
"+"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "PLUS\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return PLUS;}
"=="                    {if (strcmp(flag, "-l")==0) fprintf(yyout, "EQ\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return EQ;}
">"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "GT\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return GT;}
">="                    {if (strcmp(flag, "-l")==0) fprintf(yyout, "GE\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return GE;}
"{"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "LBRACE\n"); columns+=yyleng; return LBRACE;}
"<"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "LT\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return LT;}
"<="                    {if (strcmp(flag, "-l")==0) fprintf(yyout, "LE\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return LE;}
"("                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "LPAR\n"); columns+=yyleng; return LPAR;}
"["                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "LSQ\n"); columns+=yyleng; return LSQ;}
"%"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "MOD\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return MOD;}
"!"                     {if (strcmp(flag, "-l")==0) fprintf(yyout, "NOT\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return NOT;}
"!="                    {if (strcmp(flag, "-l")==0) fprintf(yyout, "NE\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return NE;}
"&&"                    {if (strcmp(flag, "-l")==0) fprintf(yyout, "AND\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return AND;}
"||"                    {if (strcmp(flag, "-l")==0) fprintf(yyout, "OR\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return OR;}
"}"                   	{if (strcmp(flag, "-l")==0) fprintf(yyout, "RBRACE\n"); columns+=yyleng; BEGIN SEMICOLONL; return RBRACE;}
")"                   	{if (strcmp(flag, "-l")==0) fprintf(yyout, "RPAR\n"); columns+=yyleng; BEGIN SEMICOLONL; return RPAR;}
"]"                   	{if (strcmp(flag, "-l")==0) fprintf(yyout, "RSQ\n"); columns+=yyleng; BEGIN SEMICOLONL; return RSQ;}
"package"\n?            {if (strcmp(flag, "-l")==0) fprintf(yyout, "PACKAGE\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return PACKAGE;}
"return"                {if (strcmp(flag, "-l")==0) fprintf(yyout, "RETURN\n"); columns+=yyleng; BEGIN SEMICOLONL;yylval.idToken = create_token(strdup(yytext), rows, columns); return RETURN;}
"else"\n?               {if (strcmp(flag, "-l")==0) fprintf(yyout, "ELSE\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return ELSE;}
"for"\n?                {if (strcmp(flag, "-l")==0) fprintf(yyout, "FOR\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return FOR;}
"if"\n?                 {if (strcmp(flag, "-l")==0) fprintf(yyout, "IF\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;}; yylval.idToken = create_token(strdup(yytext), rows, columns); return IF;}
"var"\n?                {if (strcmp(flag, "-l")==0) fprintf(yyout, "VAR\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return VAR;}
"int"\n?                {if (strcmp(flag, "-l")==0) fprintf(yyout, "INT\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return INT;}
"float32"\n?            {if (strcmp(flag, "-l")==0) fprintf(yyout, "FLOAT32\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return FLOAT32;}
"bool"\n?               {if (strcmp(flag, "-l")==0) fprintf(yyout, "BOOL\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return BOOL;}
"string"\n?             {if (strcmp(flag, "-l")==0) fprintf(yyout, "STRING\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return STRING;}
"fmt.Println"           {if (strcmp(flag, "-l")==0) fprintf(yyout, "PRINT\n"); columns+=yyleng; yylval.idToken = create_token(strdup(yytext), rows, columns); return PRINT;}
"strconv.Atoi"          {if (strcmp(flag, "-l")==0) fprintf(yyout, "PARSEINT\n"); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; return PARSEINT;}
"func"\n?               {if (strcmp(flag, "-l")==0) fprintf(yyout, "FUNC\n"); if(yytext[yyleng-1]=='\n') {rows++; columns=1;} else {columns+=yyleng;} return FUNC;}
"os.Args"               {if (strcmp(flag, "-l")==0) fprintf(yyout, "CMDARGS\n"); columns+=yyleng; return CMDARGS;}
("++"|"--"|"break"|"default"|"interface"|"select"|"case"|"defer")    |
("go"|"map"|"struct"|"chan"|"goto"|"switch"|"const"|"fallthrough"|"range"|"type"|"continue"|"import")\n?            {yylval.idToken = create_token(strdup(yytext), rows, columns);  if(yytext[yyleng-1]=='\n') {yytext[yyleng-1]=0; rows++; columns=1;} else {columns+=yyleng;} if (strcmp(flag, "-l")==0) fprintf(yyout, "RESERVED(%s)\n",yytext); return RESERVED;}
"//".*					;
"/*"																												{start_column=columns; columns+=yyleng; BEGIN COMMENT;}
{letter}({letter}|{decimal_digit})*   					 															{if (strcmp(flag, "-l")==0) fprintf(yyout, "ID(%s)\n", yytext); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; BEGIN SEMICOLONL; return ID;}
"0" 																												{yyless(0); BEGIN OCTAL;}
"0"("x"|"X"){hex_digit}+																							{if (strcmp(flag, "-l")==0) fprintf(yyout, "INTLIT(%s)\n", yytext); yylval.idToken = create_token(strdup(yytext), rows, columns);  columns+=yyleng; BEGIN SEMICOLONL; return INTLIT;}
[1-9]{decimal_digit}*           																					{if (strcmp(flag, "-l")==0) fprintf(yyout, "INTLIT(%s)\n", yytext); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; BEGIN SEMICOLONL; return INTLIT;}
({decimal_digit}+"."({decimal_digit}{exponent}?)*|{decimal_digit}+"."?{exponent}|"."{decimal_digit}+{exponent}?)	{if (strcmp(flag, "-l")==0) fprintf(yyout, "REALLIT(%s)\n", yytext); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; BEGIN SEMICOLONL; return REALLIT;}
\"						{yyless(0); start_column=columns; columns+=yyleng; BEGIN STRINGL;}
[" "\t\f]               {columns+=yyleng;}
\n                      {BEGIN CHECK_END_OF_FILE;}
.						{fprintf(yyout, "Line %d, column %d: illegal character (%s)\n", rows, columns, yytext); columns+=yyleng;}


<STRINGL>[^\"nfrt\\]*\\[^\"nfrt\\]?[^\"nfrt\\]*\n?	{yyless(0);  BEGIN DETECT_ESCAPE_CHAR;}
<STRINGL>\"({escape_char}|[^\"\n\r\\])*\"			{if (strcmp(flag, "-l")==0) fprintf(yyout, "STRLIT(%s)\n", yytext); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; BEGIN SEMICOLONL; return STRLIT;}                      
<STRINGL>({escape_char}|[^\"\n\r\\])*?\n			{fprintf(yyout, "Line %d, column %d: unterminated string literal\n", rows, start_column); rows++; columns=1; BEGIN 0;}
<STRINGL><<EOF>>									{fprintf(yyout, "Line %d, column %d: unterminated string literal\n", rows, start_column); yyterminate();}
<STRINGL>.											{columns+=yyleng;}

<OCTAL>{octal_digit}+			{if (strcmp(flag, "-l")==0) fprintf(yyout, "INTLIT(%s)\n", yytext); yylval.idToken = create_token(strdup(yytext), rows, columns); columns+=yyleng; BEGIN SEMICOLONL; return INTLIT;}
<OCTAL>{decimal_digit}+			{fprintf(yyout, "Line %d, column %d: invalid octal constant (%s)\n", rows, columns, yytext); columns+=yyleng; BEGIN 0;}
<OCTAL>.						{yyless(0); BEGIN 0;}

<COMMENT>"*/"(" "|\t|\f)*\n?	{if (strcmp(flag, "-l")==0 && need_semicolon==1 && yytext[yyleng-1]=='\n') fprintf(yyout, "SEMICOLON\n"); if(yytext[yyleng-1]=='\n') {yyless(yyleng); rows+=rows_aux+1; rows_aux=0; columns=1;} else {yyless(2); columns+=yyleng;} BEGIN 0; if (need_semicolon==1 && yytext[yyleng-1]=='\n') {need_semicolon=0;return SEMICOLON;}}
<COMMENT>.						{columns+=yyleng;}
<COMMENT>\n						{if (strcmp(flag, "-l")==0 && need_semicolon==1) fprintf(yyout, "SEMICOLON\n"); columns=1; rows_aux++; if (need_semicolon==1) {need_semicolon=0; return SEMICOLON;}}
<COMMENT><<EOF>>				{if (strcmp(flag, "-l")==0 && need_semicolon==1) fprintf(yyout, "SEMICOLON\n"); fprintf(yyout, "Line %d, column %d: unterminated comment\n", rows, start_column); BEGIN CHECK_END_OF_FILE; if (need_semicolon==1) return SEMICOLON;}

<DETECT_ESCAPE_CHAR>{escape_char}	{columns+=yyleng;}
<DETECT_ESCAPE_CHAR>\\.?			{fprintf(yyout, "Line %d, column %d: invalid escape sequence (%s)\n", rows, columns, yytext); columns+=yyleng;}
<DETECT_ESCAPE_CHAR>\"				{columns+=yyleng; BEGIN 0;}
<DETECT_ESCAPE_CHAR>\"\n			{rows++; columns=1; BEGIN 0;}
<DETECT_ESCAPE_CHAR>\n				{fprintf(yyout, "Line %d, column %d: unterminated string literal\n", rows, start_column); rows++; columns=1; BEGIN 0;}
<DETECT_ESCAPE_CHAR>.				{columns+=yyleng;}
<DETECT_ESCAPE_CHAR><<EOF>>			{fprintf(yyout, "Line %d, column %d: unterminated string literal\n", rows, start_column); yyterminate();}

<SEMICOLONL>((" "|\t|\f)*\n|(" "|\t|\f)*"//")		{if (strcmp(flag, "-l")==0) fprintf(yyout, "SEMICOLON\n"); yyless(0); BEGIN 0; return SEMICOLON;}
<SEMICOLONL>(" "|\t|\f)*"/*"					    {need_semicolon=1; yyless(0); BEGIN 0;}
<SEMICOLONL>.						                {yyless(0); BEGIN 0;}
<SEMICOLONL><<EOF>>					                {if (strcmp(flag, "-l")==0) fprintf(yyout, "SEMICOLON\n"); rows--; BEGIN CHECK_END_OF_FILE; return SEMICOLON;}

<CHECK_END_OF_FILE><<EOF>>          {rows++; columns=1; yyterminate();}
<CHECK_END_OF_FILE>(.|\n)           {yyless(0); rows++; columns=1; BEGIN 0;}

%%
void yyerror (const char *s) {
        printf ("Line %d, column %d: %s: %s\n",rows,columns-(int) strlen(yytext), s,yytext);
}

int yywrap() {
	return 1;
}
