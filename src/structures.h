#ifndef STRUCTURES_H
#define STRUCTURES_H

typedef struct _s23 is_expr_aux3;
typedef struct _s17 is_statement;
typedef struct _s27 is_expr1;
typedef struct _s33 is_expr2;
typedef struct _s23 is_expr_aux1;
typedef struct _s24 is_funcinvoc;
typedef struct _s30 is_keyword;
typedef struct _s15 is_type;

typedef struct _s39 {
    char* var_type;
    char* op_type;
    int is_func;
    char* var_name;
} operation_type;

typedef struct _s38 {
    char *id;
    int line;
    int col;
} id_token;

typedef struct _s23 {
    id_token* id;
    is_type* type;
    is_funcinvoc* funcinvoc;
    is_expr1* expr;
    int num;
} is_expr_aux1;

typedef struct _s37 {
    is_keyword* expr_signal;
    is_expr_aux1* expr_aux_1;
    struct _s37* next;
} is_expr6;

typedef struct _s36 {
    is_keyword* expr_mul_div;
    is_expr6* expr6;
    struct _s36* next;
} is_expr5;

typedef struct _s35 {
    is_keyword* expr_add_sub;
    is_expr5* expr5;
    struct _s35* next;
} is_expr4;

typedef struct _s34 {
    is_keyword* expr_comp;
    is_expr4* expr4;
    struct _s34* next;
} is_expr3;

typedef struct _s33 {
    is_keyword* expr_and;
    is_expr3* expr3;
    struct _s33* next;
} is_expr2;

typedef struct _s27 {
    is_keyword* expr_or;
    is_expr2* expr2;
    struct _s27* next;
} is_expr1;

typedef struct _s32{
    int aux;
}is_num;

typedef struct _s31 {
    id_token* id;
    is_expr1* exp;
    id_token* op;
} is_parseargs;

typedef struct _s30 {
    id_token* keyword;
    char* op;
} is_keyword;

typedef struct _s26 {
    is_expr1* expr;
    struct _s26* next;
} is_funcinvoc_aux2;

typedef struct _s25 {
    is_expr1* expr;
    is_funcinvoc_aux2* fiaux2;
} is_funcinvoc_aux1;

typedef struct _s24 {
    id_token* id;
    is_funcinvoc_aux1* invoc_list;
} is_funcinvoc;

typedef struct _s22 {
    is_expr1* expr;
} is_only_expr;

typedef struct _s21 {
    is_expr1* exp;
    id_token* id;
    is_type* type;
    int aux;
} is_statement_aux5;

typedef struct _s19 {
    is_statement* stat;
} is_statement_aux2;

typedef struct _s18 {
    is_statement_aux2* saux2;
    struct _s18* next;
} is_statement_aux1;

typedef struct _s20 {
    is_statement_aux1* saux1;
} is_statement_aux3;

typedef struct _s17 {
    id_token* id;
    is_expr1* expr;
    is_funcinvoc* funcinvoc;
    is_parseargs* parseargs;
    is_statement_aux1* aux1;
    is_statement_aux3* aux3;
    is_only_expr* aux4;
    is_statement_aux5* aux5;
    int line;
    int col;
    char* kw;
} is_statement;

typedef struct _s16 {
    id_token* id;
    struct _s16* next;
} is_varspec_list;

typedef struct _s15 {
    char* type;
    char* name;
} is_type;

typedef struct _s14{
    id_token* id;
    is_varspec_list* vslist;
    is_type* type;
} is_varspec;


typedef struct _s13 {
    is_varspec* varspec;
} is_vardecl;

typedef struct _s12 {
    is_vardecl* vdlist;
    is_statement* statement_list;
} is_vars_statements_aux;

typedef struct _s11 {
    is_vars_statements_aux* ivsa;
    struct _s11* next;
} is_vars_statements;

typedef struct _s10 {
    is_vars_statements* vars_list;
} is_funcbody;

typedef struct _s9 {
    id_token* id;
    is_type* type;
} is_param_list_aux;

typedef struct _s8 {
    is_param_list_aux* param; 
    struct _s8* next;
} is_param_list;

typedef struct _s7 {
    id_token* id;
    is_type* type;
    is_param_list* pmlist;
} is_param;

typedef struct _s6{
    is_type* type;
} is_type_func;

typedef struct _s5 {
    is_param* param;
} is_param_aux;

typedef struct _s4 {
    id_token* id;
    is_param_aux* param;
    is_type_func* type;
    is_funcbody* funcbody;
} is_funcdecl;

typedef struct _s3 {
    is_vardecl* vd;
    is_funcdecl* fd;
} is_decl;

typedef struct _s2 {
    is_decl* decl;
    struct _s2* next;
} is_declaration_list;

typedef struct _s1 {
    is_declaration_list* dlist;
} is_program;

#endif