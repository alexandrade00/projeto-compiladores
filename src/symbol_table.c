#include "symbol_table.h"
#include "semantics.h"
#include "structures.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Insere um novo identificador na cauda de uma lista ligada de simbolo
table_element *insert_el(table_element** symtab, id_token *token, char *type, int i) {

    table_element *newSymbol = (table_element *)malloc(sizeof(table_element));
    table_element *aux;
    table_element *previous;

    newSymbol->name = token;
    newSymbol->type = type;
    newSymbol->is_func = 0;
    newSymbol->next = NULL;
    newSymbol->is_param = i;
    newSymbol->is_used = 0;

    if (i == 1) {
        newSymbol->is_used = 1;
    }

	if(*symtab != NULL) {	//Procura cauda da lista e verifica se simbolo ja existe (NOTA: assume-se uma tabela de simbolos globais!)
		for(aux=*symtab; aux; previous=aux, aux=aux->next) {
			if(strcmp(aux->name->id, token->id)==0) {
                aux->is_used = 1;
				return NULL;
            }
        }
		previous->next=newSymbol;	//adiciona ao final da lista
	}
	else
		*symtab=newSymbol;		
	
	return newSymbol;
}

table_element *insert_func(table_element** symtab, id_token *token, char *type, table_element** params, char* return_type) {

    table_element *newSymbol = (table_element *)malloc(sizeof(table_element));
    table_element *aux;
    table_element *previous;

    newSymbol->name = token;

    if (type != NULL)
        newSymbol->type = type;
    else
        newSymbol->type = NULL;

    if (params==NULL)
        newSymbol->params=NULL;
    else {
        newSymbol->params = params;
    }

    newSymbol->return_type = return_type;

    newSymbol->is_func=1;
    newSymbol->is_used=0;
    newSymbol->next = NULL;

    if(*symtab) {	//Procura cauda da lista e verifica se simbolo ja existe (NOTA: assume-se uma tabela de simbolos globais!)
		for(aux=*symtab; aux; previous=aux, aux=aux->next) {
			if(strcmp(aux->name->id, token->id)==0) {
                aux->is_used = 1;
				return NULL;
            }
        }
		
		previous->next=newSymbol;	//adiciona ao final da lista
	} else
		*symtab=newSymbol;		
	
	return newSymbol;
}

void show_table() {

    table_element *aux;
    table_element *aux_func;
    sym_tables* tables = all_tables;
    table_element *aux_params;
    bool global = 1;

    while(tables != NULL) {
        aux = *tables->symtab;
        if (global == 1) {
            printf("===== Global Symbol Table =====\n");
            global = 0; 
        } else {
            printf("\n");
            aux_func = *tables->func_header;
            if(aux != NULL && aux->is_param == 1) {
                table_element *aux_param = aux;

                printf("===== Function %s(", aux_func->name->id);
                
                while(aux_param != NULL) {
                    if (aux_param->is_param == 1)
                        printf("%s", aux_param->type);
                    if (aux_param->next != NULL && aux_param->next->is_param == 1)
                        printf(",");
                    else
                        break;
                    aux_param = aux_param->next;
                }

                printf(") Symbol Table =====\n");
            }
            else 
                printf("===== Function %s() Symbol Table =====\n", aux_func->name->id);

             printf("return\t");
            if (aux_func != NULL) {
                if (aux_func->type!=NULL)
                    printf("\t%s\n", aux_func->type);
                else
                    printf("\tnone\n");;
            } else
                printf("\tnone\n");
        }
        
        while (aux != NULL) {
            if (aux->is_func == 0){
                if(aux->is_param !=0)
                    printf("%s\t\t%s\tparam\n", aux->name->id, aux->type);
                else
                    printf("%s\t\t%s\n", aux->name->id, aux->type);
            }
            else if (aux->is_func == 1) {
                printf("%s\t(", aux->name->id);
                if (aux->params != NULL) {
                    for (aux_params = *aux->params; aux_params != NULL; aux_params = aux_params->next) {
                        
                        printf("%s", aux_params->type);
                        
                        if (aux_params->next != NULL)
                            printf(",");
                    }
                }
                if (aux->type!=NULL)
                    printf(")\t%s\n", aux->type);
                else
                    printf(")\tnone\n");
            }
            aux=aux->next;
        }
        tables=tables->next;
    }
    printf("\n");
}

// Procura um identificador, devolve NULL caso nao exista
table_element *search_el(id_token *token, char *func_name) {

    if (token == NULL)
        return NULL;

    table_element *aux;
    table_element *aux_param;
    table_element *func_header;
    sym_tables* table_aux = all_tables->next;

    if (func_name != NULL) {
        while(table_aux != NULL) {
            func_header = *table_aux->func_header;
            if (strcmp(func_header->name->id, func_name) == 0) {
                for (aux = *table_aux->symtab; aux; aux = aux->next) {
                    if (strcmp(aux->name->id, token->id) == 0 && token->line > aux->name->line) {
                        aux->is_used = 1;
                        return aux;
                    }
                    else if (aux->params != NULL) {
                        for (aux_param = *aux->params; aux_param; aux_param = aux_param->next) {
                            if (strcmp(aux_param->name->id, token->id) == 0 && token->line > aux_param->name->line) {
                                aux_param->is_used = 1;
                                return aux_param;
                            }
                        }
                    }
                }
            }
            table_aux = table_aux->next;
        }
    }

    for (aux = *all_tables->symtab; aux; aux = aux->next) {
        if (strcmp(aux->name->id, token->id) == 0 /* && token->line > aux->name->line */ && aux->is_func == 0) {
            aux->is_used = 1;
            return aux;
        }
        else if (aux->params != NULL) {
            for (aux_param = *aux->params; aux_param; aux_param = aux_param->next) {                
                if (strcmp(aux_param->name->id, token->id) == 0 /* && token->line > aux->name->line */ && aux->is_func == 0) {
                    aux_param->is_used = 1;
                    return aux_param;
                }
            }
        }
    }

    return NULL;
}

table_element *search_el_local(id_token *token, char *func_name) {

    if (token == NULL)
        return NULL;

    table_element *aux;
    table_element *aux_param;
    table_element *func_header;
    sym_tables* table_aux = all_tables->next;

    if (func_name != NULL) {
        while(table_aux != NULL) {
            func_header = *table_aux->func_header;
            if (strcmp(func_header->name->id, func_name) == 0) {
                for (aux = *table_aux->symtab; aux; aux = aux->next) {
                    if (strcmp(aux->name->id, token->id) == 0 && token->line > aux->name->line) {
                        aux->is_used = 1;
                        return aux;
                    }
                    else if (aux->params != NULL) {
                        for (aux_param = *aux->params; aux_param; aux_param = aux_param->next) {
                            if (strcmp(aux_param->name->id, token->id) == 0 && token->line > aux_param->name->line) {
                                aux_param->is_used = 1;
                                return aux_param;
                            }
                        }
                    }
                }
            }
            table_aux = table_aux->next;
        }
    }
    return NULL;
}

table_element *search_func(id_token *token) {

    table_element *aux;
    table_element* aux_param;

    for (aux = *all_tables->symtab; aux; aux = aux->next) {
        if (strcmp(aux->name->id, token->id) == 0) {
            aux->is_used = 1;
            return aux;
        }
        else if (aux->params != NULL) {
            for (aux_param = *aux->params; aux_param; aux_param = aux_param->next) {
                if (strcmp(aux_param->name->id, token->id) == 0) {
                    aux_param->is_used = 1;
                    return aux_param;
                }
            }
        }
    }
    return NULL;
}

void print_func_params_type(char* name) {
    
    table_element* aux_param;
    table_element *aux = *all_tables->symtab;

    while (aux != NULL) {
        if (aux->is_func == 1 && strcmp(aux->name->id, name) == 0) {
            if (aux->params != NULL) {
                aux_param = *aux->params;

                while(aux_param != NULL) {
                    if (aux_param->is_param!=0)
                        printf("%s", aux_param->type);
                    if (aux_param->next != NULL && aux_param->is_param!=0)
                        printf(",");
                    else
                        return;
                    aux_param = aux_param->next;
                }
            }
            return; 
        }
        aux=aux->next;
    }   
}

bool unused_variables() {

    if (all_tables == NULL || all_tables->next == NULL)
        return 0;

    sym_tables* aux_table = all_tables->next;
    table_element* sym_table;

    while (aux_table != NULL) {
        sym_table = *aux_table->symtab;

        while (sym_table != NULL) {
            if (sym_table->is_func == 0 && sym_table->is_used == 0) {
                printf("Line %d, column %d: Symbol %s declared but never used\n", sym_table->name->line, sym_table->name->col, sym_table->name->id);
                return 1;
            }
            sym_table = sym_table->next;
        }

        aux_table = aux_table->next;
    }
    return 0;
}