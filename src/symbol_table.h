#include "structures.h"
#include <stdbool.h>

#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

typedef struct _t1 {
  id_token *name;
  char *type;
  bool is_used;
  bool is_func;
  bool is_param;
  char* return_type;
  struct _t1 **params;
  struct _t1 *next;
} table_element;


typedef struct _t2 {
  table_element** func_header;
  table_element** symtab;
  is_funcbody* fb;
  struct _t2 *next;
} sym_tables;


table_element *insert_el(table_element** all_symtab, id_token *token, char *type, int i);
table_element *insert_func(table_element** symtab, id_token *token, char *type, table_element** params, char* return_type);
table_element *search_el(id_token *token, char *func_name);
table_element *search_func(id_token *token);
table_element *search_el_local(id_token *token, char *func_name);
void show_table();
void show_table_local();
void print_func_params_type(char* func_name);
bool unused_variables();

#endif