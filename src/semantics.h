#include "structures.h"
#include "symbol_table.h"

extern sym_tables* all_tables;
extern bool error_flag;

void check_program(is_program *p);
void check_declaration_list(table_element** all_symtab, is_declaration_list* id);
void check_declaration(table_element** all_symtab, is_decl* id);
void check_vardecl(table_element** all_symtab, is_vardecl* ivd);
void check_varspec(table_element** all_symtab, is_varspec* ivs);
void check_varspec_list(table_element** all_symtab, is_varspec_list* ivsl, is_type* type);
void check_funcdecl(table_element** all_symtab, is_funcdecl* fd);
void check_funcbody(table_element** all_symtab, is_funcbody* fb);
void check_vars_stat(table_element** all_symtab, is_vars_statements* ivs);
void check_vars_stat_aux(table_element** all_symtab, is_vars_statements_aux* ivsa);
table_element* check_parameter_list(is_param_list* ipl);
void check_token(table_element** all_symtab, id_token* id, is_type* type, int i);
int check_func(table_element** all_symtab, id_token* id, is_type* type, table_element** params, char* return_type);
void check_statement(table_element** all_symtab, is_statement* is);
void check_aux1(table_element** all_symtab,is_statement_aux1* aux);
void check_aux2(table_element** all_symtab, is_statement_aux2* aux);
void check_aux3(table_element** all_symtab, is_statement_aux3* aux);
void check_aux4(table_element** all_symtab, is_only_expr* aux, int line, int, char* kw);
void check_aux5(table_element** all_symtab, is_statement_aux5* aux, int line, int col);
void check_expr1(table_element** all_symtab, is_expr1* aux);
void check_expr2(table_element** all_symtab, is_expr2* aux);
void check_expr3(table_element** all_symtab, is_expr3* aux);
void check_expr4(table_element** all_symtab, is_expr4* aux);
void check_expr5(table_element** all_symtab, is_expr5* aux);
void check_expr6(table_element** all_symtab, is_expr6* aux);
void check_expr_aux1(table_element** all_symtab, is_expr_aux1* aux);
void check_id(id_token* id);
void check_func_invoc(table_element** all_symtab, is_funcinvoc* ifi);
void check_funcinvoc_aux1(table_element** all_symtab, is_funcinvoc_aux1* ifi1,int row, int col, char* name);
void check_funcinvoc_aux2(table_element** all_symtab, is_funcinvoc_aux2* ifi2);