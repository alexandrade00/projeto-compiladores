#include "structures.h"
#include "symbol_table.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#ifndef CODE_GENERATOR_H
#define CODE_GENERATOR_H

typedef struct _g1 {
    int register_num;
    char* var_name;
    char* var_type;
    struct _g1* next;
} table;

typedef struct _g2 {
    char* string;
    struct _g2* next;
} strings_array;

typedef struct _g3 {
    int str_num;
    int str_len;
    char* string;
    struct _g3* next;
} str_tams;

typedef struct _g4 {
    char* var_name;
    bool value;
    struct _g4* next;
} bool_values;

#endif

void generate_program(is_program* ip);
void generate_declaration_list(is_declaration_list* dl);
void generate_declaration(is_decl* decl);
void generate_vardecl(is_vardecl* vdl);
void generate_varspec(is_varspec* vs);
void generate_varspec_list(is_varspec_list* vslist, is_type* type);
void generate_funcdecl(is_funcdecl* ifd);
char* generate_print_type_func(is_type_func* itf);
char* generate_print_type(is_type* tp);
void generate_param_aux(is_param_aux* ipa);
void generate_param(is_param* ip);
void generate_param_list(is_param_list* ipl);
void generate_param_list_aux(is_param_list_aux* ipla);
void generate_funcbody(is_funcbody* ifb);
void generate_vars_list(is_vars_statements* ivs);
void generate_vars_list_aux(is_vars_statements_aux* ivsa);
void generate_statement(is_statement* is);
void generate_expr1(is_expr1* expr1);
void generate_expr2(is_expr2* expr2);
void generate_expr3(is_expr3* expr3);
void generate_expr4(is_expr4* expr4);
void generate_expr5(is_expr5* expr5);
void generate_expr6(is_expr6* expr6);
void generate_expr_aux1(is_expr_aux1* exp_aux_1);
void check_op_expr1(is_expr1* aux, int registo1, int registo2);
void check_op_expr2(is_expr2* expr, int registo1, int registo2);
void check_op_expr3(is_expr3* aux, int registo1, int registo2);
void check_op_expr4(is_expr4* aux, int registo1, int registo2);
void check_op_expr5(is_expr5* aux, int registo1, int registo2);
void check_op_expr6(is_expr6* expr6, int reg1);
void generate_only_expr(is_only_expr* st4);
void generate_statement_aux1(is_statement_aux1* is_st);
void generate_statement_aux2(is_statement_aux2* is_st2);
void generate_statement_aux3(is_statement_aux3* is_st3, char* _ifcont);
void generate_statement_aux5(is_statement_aux5* is_st5);
void generate_func_inv(is_funcinvoc* finv);
void generate_func_inv_aux1(is_funcinvoc_aux1* finv_1, table** params_regs);
void generate_func_inv_aux2(is_funcinvoc_aux2* is_fc2, table** params_regs);
void generate_parseargs(is_parseargs* pa);
void generate_labels(is_statement* is);
void generate_functions();
void generate_string_declaration();

int count_ifs(is_statement* is);
int counter_if(is_statement_aux1* state1, int counter);
void check_return_inside_if_else(is_statement_aux1* state1);
