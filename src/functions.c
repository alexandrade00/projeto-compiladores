#include "structures.h"
#include "functions.h"
#include "semantics.h"
#include "var.h"
# include "code_generator.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char* func_name;
char* func_type_print;
strings_array** string_array;

id_token *create_token(char *id, int line, int col) {

  id_token *idt = (id_token*)malloc(sizeof(id_token));
  
  idt->id = id;
  idt->line = line;
  idt->col = col;
  
  return idt;
}

is_program* insert_program(is_declaration_list* idl) {

    is_program* ip=(is_program*)malloc(sizeof(is_program));

	ip->dlist=idl;

	return ip;
}


is_declaration_list* insert_declaration_list(is_declaration_list* head, is_decl* decl) {
    
    is_declaration_list* idl=(is_declaration_list*)malloc(sizeof(is_declaration_list));
    is_declaration_list* temp;

    idl->decl=decl;
    idl->next=NULL;

    if (head==NULL)
        return idl;

    for(temp=head; temp->next; temp=temp->next);
    temp->next=idl;

    return head;
}


is_decl* insert_declaration(is_vardecl* vd, is_funcdecl* fdl) {

    is_decl* decl = (is_decl*)malloc(sizeof(is_decl));

    decl->vd=vd;
    decl->fd=fdl;

    return decl;
}



is_vardecl* insert_vardecl(is_varspec* vspec) {

    is_vardecl* vd = (is_vardecl*)malloc(sizeof(is_vardecl));

    vd->varspec=vspec;

    return vd;
}

is_varspec* insert_varspec(id_token* id, is_varspec_list* list, is_type* type) {

    is_varspec* vs = (is_varspec*)malloc(sizeof(is_varspec));
;
    vs->id=id;
    vs->vslist=list;
    vs->type=type;

    return vs;
}

is_varspec_list* insert_varspec_list(is_varspec_list* head, id_token* id) {

    is_varspec_list* vsl = (is_varspec_list*)malloc(sizeof(is_varspec_list));
    is_varspec_list* temp;

    vsl->id=id;
    vsl->next=NULL;

    if(head==NULL)
        return vsl;

    for(temp=head; temp->next; temp=temp->next);
    temp->next=vsl;

    return head;
}



is_type* insert_type(char* type, char* name) {

    is_type* it = (is_type*)malloc(sizeof(is_type));

    it->type=type;
    it->name=name;

    return it;
}

is_funcdecl* insert_funcdecl(id_token* id, is_param_aux *param, is_type_func *type, is_funcbody *func) {

    is_funcdecl* fd = (is_funcdecl*)malloc(sizeof(is_funcdecl));

    fd->id=id;
    fd->param=param;
    fd->type=type;
    fd->funcbody=func;

    return fd;
}

is_param_aux* insert_param_aux(is_param* funcParam) {

    is_param_aux* pma = (is_param_aux*)malloc(sizeof(is_param_aux));

    pma->param=funcParam;

    return pma;
}

is_param* insert_param(id_token* id, is_type* type, is_param_list* pmlist) {

    is_param* pm = (is_param*)malloc(sizeof(is_param));

    pm->id=id;
    pm->type=type;
    pm->pmlist=pmlist;

    return pm;
}

is_type_func* insert_type_func(is_type* type) {

    is_type_func* tf = (is_type_func*)malloc(sizeof(is_type_func));

    tf->type=type;

    return tf;
}

is_param_list* insert_param_list(is_param_list*head, is_param_list_aux* param) {

    is_param_list* fparam = (is_param_list*)malloc(sizeof(is_param_list));
    is_param_list* temp;

    fparam->param=param;
    fparam->next=NULL;

    if (head==NULL) 
        return fparam;

    for(temp=head; temp->next; temp=temp->next);
    temp->next=fparam;

    return head;
}

is_param_list_aux* insert_param_list_aux(id_token* id, is_type* type) {

    is_param_list_aux* ipma = (is_param_list_aux*)malloc(sizeof(is_param_list_aux));

    ipma->id=id;
    ipma->type=type;

    return ipma;
}

is_funcbody* insert_funcbody(is_vars_statements* varsstat) {

    is_funcbody* fb = (is_funcbody*)malloc(sizeof(is_funcbody));

    fb->vars_list=varsstat;

    return fb;
}

is_vars_statements_aux* insert_vars_statements_aux(is_vardecl* vd, is_statement* stat) {

    is_vars_statements_aux* ivsa = (is_vars_statements_aux*)malloc(sizeof(is_vars_statements_aux));

    ivsa->vdlist=vd;
    ivsa->statement_list=stat;

    return ivsa;
}

is_vars_statements* insert_vars_statements(is_vars_statements* head, is_vars_statements_aux* ivsa) {

    is_vars_statements* vs = (is_vars_statements*)malloc(sizeof(is_vars_statements));
    is_vars_statements* temp;

    vs->ivsa=ivsa;
    vs->next=NULL;

    if(head==NULL)
        return vs;

    for(temp=head; temp->next; temp=temp->next);
    temp->next=vs;
    
    return head;
}

is_statement* insert_statement(id_token* id, is_statement_aux1* statlist, is_expr1* expr, is_funcinvoc* fi, is_parseargs* pa, is_statement_aux3* aux3, is_only_expr* aux4, is_statement_aux5* aux5, char* kw, id_token* pos) {

    is_statement* stat = (is_statement*)malloc(sizeof(is_statement));

    if (pos != NULL) {
        stat->col=pos->col;
        stat->line=pos->line;
    }
    stat->kw=kw;
    stat->id=id;
    stat->expr=expr;
    stat->funcinvoc=fi;
    stat->parseargs=pa;
    stat->aux1=statlist;
    stat->aux3=aux3;
    stat->aux4=aux4;
    stat->aux5=aux5;

    return stat;
}

is_statement_aux1* insert_statement_aux1(is_statement_aux1* head, is_statement_aux2* state2) {

    is_statement_aux1* stat_list = (is_statement_aux1*)malloc(sizeof(is_statement_aux1));
    is_statement_aux1* temp;

    stat_list->saux2=state2;
    stat_list->next=NULL;

    if (head==NULL)
        return stat_list;

    for(temp=head; temp->next; temp=temp->next);
    temp->next=stat_list;
    
    return head;
}

is_statement_aux2* insert_statement_aux2(is_statement* stat) {

    is_statement_aux2* stat2 = (is_statement_aux2*)malloc(sizeof(is_statement_aux2));

    stat2->stat=stat;

    return stat2;
}

is_statement_aux3* insert_statement_aux3(is_statement_aux1* stat1) {

    is_statement_aux3* state3 = (is_statement_aux3*)malloc(sizeof(is_statement_aux3));

    state3->saux1=stat1;

    return state3;
}

is_statement_aux5* insert_statement_aux5(is_expr1* expr, id_token* id, int aux) {

    is_statement_aux5* state5 = (is_statement_aux5*)malloc(sizeof(is_statement_aux5));
    state5->type = (is_type*)malloc(sizeof(is_type));

    if (aux == 3) {
        state5->type->name="string";
        if (string_array == NULL)
            string_array = (strings_array**)malloc(sizeof(strings_array));
        insert_string_array(string_array, id->id);
    }
    else
        state5->type->name=NULL;

    state5->exp=expr;
    state5->id=id;
    state5->aux=aux;
    return state5;
}

is_keyword* insert_keyword(id_token* kw, char* op) {     

    is_keyword* keyword = (is_keyword*)malloc(sizeof(is_keyword));     
    
    keyword->keyword = kw;
    keyword->op=op;   
    
    return keyword; 
}

is_funcinvoc* insert_funcinvoc(id_token* id, is_funcinvoc_aux1* func_list) {

    is_funcinvoc* fi = (is_funcinvoc*)malloc(sizeof(is_funcinvoc));

    fi->id=id;
    fi->invoc_list=func_list;

    return fi;
}

is_funcinvoc_aux1* insert_funcinvoc_aux1(is_expr1* expr, is_funcinvoc_aux2* fiaux2) {

    is_funcinvoc_aux1* fiaux1 = (is_funcinvoc_aux1*)malloc(sizeof(is_funcinvoc_aux1));

    fiaux1->expr=expr;
    fiaux1->fiaux2=fiaux2;

    return fiaux1;
}

is_funcinvoc_aux2* insert_funcinvoc_aux2(is_funcinvoc_aux2* head, is_expr1* expr) {
    
    is_funcinvoc_aux2* filist = (is_funcinvoc_aux2*)malloc(sizeof(is_funcinvoc_aux2));
    is_funcinvoc_aux2* temp;

    filist->expr=expr;
    filist->next=NULL;

    if (head==NULL)
        return filist;

    for(temp=head; temp->next; temp=temp->next);
    temp->next=filist;
    
    return head;
}

is_expr1* insert_expr1(is_expr1* head, is_keyword* kw, is_expr2* expr2) {

    is_expr1* expr1 = (is_expr1*)malloc(sizeof(is_expr1));

    expr1->expr_or=kw;
    expr1->expr2=expr2;
    expr1->next=head;
    
    return expr1;
}

is_expr2* insert_expr2(is_expr2* head, is_keyword* kw, is_expr3* expr3) {

    is_expr2* expr2 = (is_expr2*)malloc(sizeof(is_expr2));

    expr2->expr_and=kw;
    expr2->expr3=expr3;
    expr2->next=head;
    
    return expr2;
}

is_expr3* insert_expr3(is_expr3* head, is_keyword* kw, is_expr4* expr4) {

    is_expr3* expr3 = (is_expr3*)malloc(sizeof(is_expr3));

    expr3->expr_comp=kw;
    expr3->expr4=expr4;
    expr3->next=head;
    
    return expr3;
}

is_expr4* insert_expr4(is_expr4* head, is_keyword* kw, is_expr5* expr5) {
    
    is_expr4* expr4 = (is_expr4*)malloc(sizeof(is_expr4));
    
    expr4->expr_add_sub=kw;
    expr4->expr5=expr5;
    expr4->next=head;
    
    return expr4;
}

is_expr5* insert_expr5(is_expr5* head, is_keyword* kw, is_expr6* expr6) {

    is_expr5* expr5 = (is_expr5*)malloc(sizeof(is_expr5));

    expr5->expr_mul_div=kw;
    expr5->expr6=expr6;
    expr5->next=head;
    
    return expr5;
}

is_expr6* insert_expr6(is_expr6* head, is_keyword* kw, is_expr_aux1* expr_aux_1) {

    is_expr6* expr6 = (is_expr6*)malloc(sizeof(is_expr6));

    expr6->expr_signal=kw;
    expr6->expr_aux_1=expr_aux_1;
    expr6->next=head;
    
    return expr6;
}

is_expr_aux1* insert_expr_aux1(id_token* id, is_funcinvoc* fi, is_expr1* exp1, int num) {

    is_expr_aux1* expr_aux1 = (is_expr_aux1*)malloc(sizeof(is_expr_aux1));
    is_type* type = (is_type*)malloc(sizeof(is_type));

    switch (num) {
        case 1:
            type->name = "int";
            break;
        case 2:
            type->name = "float32";
            break;
        default:
            type = NULL;
    }

    expr_aux1->id=id;
    expr_aux1->type=type;
    expr_aux1->funcinvoc=fi;
    expr_aux1->expr=exp1;
    expr_aux1->num=num;

    return expr_aux1;
}

is_only_expr* insert_only_expr(is_expr1* expr) {

    is_only_expr* oe = (is_only_expr*)malloc(sizeof(is_only_expr));

    oe->expr=expr;

    return oe;
}

is_parseargs* insert_parseargs(id_token* id, is_expr1* exp, id_token* op) {

    is_parseargs* parseargs = (is_parseargs*)malloc(sizeof(is_parseargs));

    parseargs->id=id;
    parseargs->exp=exp;
    parseargs->op=op;

    return parseargs;
}


void print_program(is_program* ip, int depth) {

    if (ip==NULL)
        return;

    if (strcmp(flag, "-t") == 0 || strcmp(flag, "-s") == 0) {
        printf("Program\n");

        depth++;
        print_declaration_list(ip->dlist, depth);
    }
}


void print_declaration_list(is_declaration_list* dl, int depth) {

    if (dl == NULL)
        return;
    
    print_declaration(dl->decl, depth);
    print_declaration_list(dl->next, depth);
}

void print_declaration(is_decl* decl, int depth) {

    if (decl == NULL)
        return;

    print_vardecl(decl->vd, depth);
    print_funcdecl(decl->fd, depth);
}


void print_vardecl(is_vardecl* vdl, int depth) {

    if (vdl == NULL)
        return;

    for (int i=0; i<depth*2; i++)
        printf(".");
    printf("VarDecl\n");
    depth++;
    print_varspec(vdl->varspec, depth);
    depth--;
}

void print_varspec(is_varspec* vs, int depth) {

    if (vs == NULL)
        return;

    print_type(vs->type, depth);
    print_id(vs->id, depth, -1, NULL);
    print_varspec_list(vs->vslist, vs->type, depth);
}

void print_varspec_list(is_varspec_list* vslist, is_type* type, int depth) {

    if (vslist == NULL)
        return;

    depth--;
    for (int i=0; i<depth*2; i++)
        printf(".");
    printf("VarDecl\n");
    depth++;
    print_type(type, depth);
    print_id(vslist->id, depth, -1, NULL);
    print_varspec_list(vslist->next, type, depth);
}


/*
Var aux:
-2 - (Type)
-1 - Only ID
0 - Int
1 - IntLit
2 - RealLit
3 - StrLit
4 - FuncHeadParam && Int
5 - FuncHeadParam && IntLit
6 - FuncHeadParam && RealLit
7 - FuncHeadParam && StrLit
*/
void print_id(id_token* id, int depth, int aux, char* type) {
    
    if (id == NULL)
        return;

    sym_tables* table_aux = all_tables;
    table_element* aux1;
    
    while(table_aux != NULL){
         aux1 = search_el(id, func_name);
         table_aux = table_aux->next;
    }

    for (int i=0; i<depth*2; i++)
        printf(".");

    switch (aux) {
        case 0:
            if (aux1 == NULL) {
                if (type == NULL)
                     printf("Id(%s)\n", id->id);
                else if (strcmp(type, "")==0)
                    printf("Id(%s)\n", id->id);
                else{
                    if(strcmp(flag,"-s")==0)
                        printf("Id(%s) - %s\n", id->id, type);
                    else if(strcmp(flag,"-t")==0)
                        printf("Id(%s)\n",id->id);
                }
            } else {
                if (aux1->is_param == 1) {
                    if(strcmp(flag,"-s")==0)
                        printf("Id(%s) - %s\n", id->id, type);
                    else if(strcmp(flag,"-t")==0)
                        printf("Id(%s)\n",id->id);
                    aux1->is_param = 0;
                } 
                else {
                    if(strcmp(flag,"-s")==0)
                        printf("Id(%s) - %s\n", id->id, aux1->type);
                    else if(strcmp(flag,"-t")==0)
                        printf("Id(%s)\n",id->id);
                }
            }
            break;
        case 1:
            if(strcmp(flag, "-s")==0)
                printf("IntLit(%s) - int\n",id->id);
            else if(strcmp(flag, "-t")==0)
                printf("IntLit(%s)\n",id->id);
            break;

        case 2:
            if(strcmp(flag, "-s")==0)
                printf("RealLit(%s) - float32\n",id->id);
            else if(strcmp(flag, "-t")==0)
                printf("RealLit(%s)\n",id->id);
            break;
        case 3:
            if(strcmp(flag, "-s")==0)
                printf("StrLit(%s) - string\n", id->id);
            else if(strcmp(flag, "-t")==0)
                printf("StrLit(%s)\n", id->id);
            break;
        case 4:
        case 5:
            if(strcmp(flag, "-s")==0)
                printf("Id(%s) - int\n", id->id);
            else if(strcmp(flag, "-t")==0)
                printf("Id(%s)\n", id->id);
            break;
        case 6:
            if(strcmp(flag, "-s")==0)
                printf("Id(%s) - float32\n", id->id);
            else if(strcmp(flag, "-t")==0)
                printf("Id(%s)\n", id->id);
            break;
        case 7:
            if(strcmp(flag, "-s")==0)
                printf("Id(%s) - string\n", id->id);
            else if(strcmp(flag, "-t")==0)
                printf("Id(%s)\n", id->id);
            break;
        case -1:
            printf("Id(%s)\n", id->id);
            break;
        case -2:
            if(strcmp(flag,"-s")==0){
            printf("Id(%s) - (", id->id);
            print_func_params_type(type);
            printf(")\n");
            }
            else if(strcmp(flag,"-t")==0)
                printf("Id(%s)\n",id->id);
            break;
    }

}


void print_char(char* str, int depth, int aux, char* type) {
    //passar aux a 0 para uma string e diferente de 0 para um comando
    if (str == NULL || strcmp(str, "Assign") == 0)
        return;

    for (int i=0; i<depth*2; i++)
        printf(".");

    if(aux==0){
        if(strcmp(flag, "-s")==0)
            printf("StrLit(%s) - string\n",str);
        else if(strcmp(flag, "-t")==0)
            printf("StrLit(%s)\n",str);
    }
    else if(aux!=0 &&
        (strcmp(str, "Mul")==0 ||
        strcmp(str, "Sub")==0 ||
        strcmp(str, "Eq")==0 || 
        strcmp(str, "Lt")==0 ||
        strcmp(str, "Gt")==0 ||
        strcmp(str, "Le")==0 ||
        strcmp(str, "Ge")==0 ||
        strcmp(str, "Or")==0 ||
        strcmp(str, "And")==0 ||
        strcmp(str, "Ne")==0 ||
        strcmp(str, "Div")==0 ||
        strcmp(str, "Mod")==0 ||
        strcmp(str, "Add")==0)){

        if(strcmp(flag, "-s")==0)
            printf("%s - %s\n", str, type);
        else if(strcmp(flag, "-t")==0)
            printf("%s - %s\n", str, type);
        }
    else if (aux != 0 &&
        (strcmp(str, "Print") == 0 ||
        strcmp(str,"If")==0 ||
        strcmp(str,"For")==0 ||
        strcmp(str,"Return")==0))

        printf("%s\n", str);

    else{
         if(strcmp(flag, "-s")==0)
            printf("Id(%s) - %s\n", str, type);
         else if(strcmp(flag, "-t")==0)
            printf("Id(%s)\n", str);
    }
}

void print_funcdecl(is_funcdecl* ifd, int depth) {

    if (ifd == NULL)
        return;

    for (int i=0; i<depth*2; i++)
        printf(".");
    printf("FuncDecl\n");
    depth++;

    for (int i=0; i<depth*2; i++)
        printf(".");
    printf("FuncHeader\n");
    depth++;

    func_name = ifd->id->id;
    if (ifd->type != NULL)
        func_type_print = ifd->type->type->name;
    else
        func_type_print = "none";
    print_id(ifd->id, depth, -1, "");
    print_type_func(ifd->type, depth);
    print_param_aux(ifd->param, depth);
    depth--;
    for (int i=0; i<depth*2; i++)
        printf(".");
    printf("FuncBody\n");
    depth++;
    print_funcbody(ifd->funcbody, depth);
}

void print_type_func(is_type_func* itf, int depth) {

    if (itf == NULL)
        return;

    print_type(itf->type, depth);
}

void print_type(is_type* tp, int depth) {
    
    if (tp == NULL)
        return;

    for (int i=0; i<depth*2; i++)
        printf(".");
    
    int i = 0;
    char ch;
    while(tp->name[i]) {
        ch = tp->name[i];
        if (i == 0)
            putchar(toupper(ch));
        else
            putchar(ch);
        i++;
    }
    printf("\n");
}

void print_param_aux(is_param_aux* ipa, int depth) {

    if (ipa == NULL) {
        for (int i=0; i<depth*2; i++)
            printf(".");
        printf("FuncParams\n");
        return;
    }

    for (int i=0; i<depth*2; i++)
        printf(".");
    printf("FuncParams\n");
    depth++;
    for (int i=0; i<depth*2; i++)
        printf(".");
    printf("ParamDecl\n");
    depth++;
    print_param(ipa->param, depth);
    depth--;
}

void print_param(is_param* ip, int depth) {

    if (ip == NULL)
        return;

    print_type(ip->type, depth);
    print_id(ip->id, depth, -1, "");
    print_param_list(ip->pmlist, depth);
}

void print_param_list(is_param_list* ipl, int depth) {

    if (ipl == NULL)
        return;

    print_param_list_aux(ipl->param, depth);
    print_param_list(ipl->next, depth);
}

void print_param_list_aux(is_param_list_aux* ipla, int depth) {

    if (ipla == NULL)
        return;

    depth--;
    for (int i=0; i<depth*2; i++)
        printf(".");
    printf("ParamDecl\n");
    depth++;

    print_type(ipla->type, depth);
    print_id(ipla->id, depth, -1, "");
}

void print_funcbody(is_funcbody* ifb, int depth) {

    if (ifb == NULL)
        return;

    print_vars_list(ifb->vars_list, depth);
}

void print_vars_list(is_vars_statements* ivs, int depth) {

    if (ivs == NULL)
        return;

    print_vars_list_aux(ivs->ivsa, depth);
    print_vars_list(ivs->next, depth);
}

void print_vars_list_aux(is_vars_statements_aux* ivsa, int depth) {

    if (ivsa == NULL)
        return;

    print_vardecl(ivsa->vdlist, depth);
    print_statement(ivsa->statement_list, depth);
}

int count_num_stat(is_statement_aux1* is) {
    int num=0;
    while(is!=NULL) {
        if (is->saux2->stat->expr!=NULL
        || is->saux2->stat->aux1!=NULL
        || is->saux2->stat->aux3!=NULL
        || is->saux2->stat->aux4!=NULL
        || is->saux2->stat->aux5!=NULL
        || is->saux2->stat->parseargs!=NULL
        || is->saux2->stat->id!=NULL
        || is->saux2->stat->funcinvoc!=NULL) {
            num++;

            if (is->saux2->stat->aux1!=NULL) {
                num--;
                if (count_num_stat(is->saux2->stat->aux1) > 0)
                    num++; 
            }

            if (is->saux2->stat->aux3!=NULL) {
                num--;
                if (is->saux2->stat->aux3->saux1!=NULL) {
                    if (count_num_stat(is->saux2->stat->aux3->saux1) > 0)
                        num++; 
                }
            }
        }

        is=is->next;
    }
    return num;
}

void print_statement(is_statement* is, int depth) {
    
    if (is == NULL)
        return;

    if (is->id != NULL) {
        if (is->expr != NULL) {
            char* type = NULL;
            table_element* aux1;
            
            aux1 = search_el(is->id, func_name);
            if (aux1 != NULL)
                type = aux1->type;
            
            for (int i=0; i<depth*2; i++)
                printf(".");

            if(strcmp(flag,"-s") == 0) {
                if (type != NULL)
                    printf("Assign - %s\n", type);
                else
                    printf("Assign\n");
            }
            
            if(strcmp(flag,"-t") == 0)
                printf("Assign\n");
            
            depth++;
            print_id(is->id, depth, 0, type);
        }
    }

    print_char(is->kw, depth, 1, NULL);
    
    if (is->kw != NULL && is->kw != NULL) {
        if (strcmp(is->kw, "If")==0 ||
            strcmp(is->kw, "Return")==0 ||
            strcmp(is->kw, "Print")==0 ||
            strcmp(is->kw, "For")==0)
            depth++;
    }

    print_expr1(is->expr, depth);
    print_only_expr(is->aux4, depth);    

    if (is->aux1 != NULL) {
        int num_stat = count_num_stat(is->aux1);

        if (num_stat > 1) {
            for (int i=0; i<depth*2; i++)
                printf(".");
            printf("Block\n");
            depth++;

        } else if (is->kw != NULL) {
            if (strcmp(is->kw, "If")==0 || strcmp(is->kw, "For")==0) {
                for (int i=0; i<depth*2; i++)
                    printf(".");
                printf("Block\n");
                depth++;
            }
        }

        print_statement_aux1(is->aux1, depth);

        depth--;
        if (is->kw != NULL && strcmp(is->kw, "If")==0) {
            for (int i=0; i<depth*2; i++)
                printf(".");
            printf("Block\n");
        }
    } else if (is->kw != NULL) {
        if (strcmp(is->kw, "If")==0 || strcmp(is->kw, "For")==0) {

            for (int i=0; i<depth*2; i++)
                printf(".");
            printf("Block\n");
            depth++;

            print_statement_aux1(is->aux1, depth);

            depth--;
            if (is->kw!=NULL && strcmp(is->kw, "If")==0) {
                for (int i=0; i<depth*2; i++)
                    printf(".");
                printf("Block\n");
            }
        }
    }

    print_statement_aux3(is->aux3, depth);
    print_statement_aux5(is->aux5, depth,1);
    print_func_inv(is->funcinvoc, depth);
    print_parseargs(is->parseargs, depth);
}

void print_statement_aux1(is_statement_aux1* is_st, int depth){
    if (is_st == NULL)
        return;

    print_statement_aux2(is_st->saux2, depth);
    print_statement_aux1(is_st->next, depth);

}

void print_statement_aux2(is_statement_aux2* is_st2, int depth){
    if (is_st2 == NULL)
        return;
    
    print_statement(is_st2->stat, depth);
}

void print_statement_aux3(is_statement_aux3* is_st3, int depth){
     if (is_st3 == NULL)
        return;

    depth++;
    print_statement_aux1(is_st3->saux1, depth);
}

void print_only_expr(is_only_expr* st4, int depth) {

    if (st4 == NULL)
        return;
    
    print_expr1(st4->expr ,depth);
}

void print_statement_aux5(is_statement_aux5* is_st5, int depth,int aux){

    if (is_st5 == NULL)
        return;

    print_expr1(is_st5->exp ,depth);
    print_id(is_st5->id, depth, is_st5->aux, "");
}

void print_func_inv(is_funcinvoc* finv, int depth) { 
    if (finv == NULL)
        return;
    
    table_element *aux = search_el(finv->id, func_name);
    char *type = NULL;
    
    if (aux == NULL) {
        type = "undefined";
        aux = search_func(finv->id);
        if (aux != NULL)
            type = aux->type;
    } else
        type = aux->type;

    for (int i=0; i<depth*2; i++)
        printf(".");
    if (type != NULL){
        if(strcmp(flag,"-s")==0)
            printf("Call - %s\n", type);
        else if(strcmp(flag,"-t")==0)
            printf("Call\n");
    }
    else
        printf("Call\n");
    depth++;

    print_id(finv->id, depth, -2, finv->id->id);
    print_func_inv_aux1(finv->invoc_list, depth);
}

void print_func_inv_aux1(is_funcinvoc_aux1* finv_1, int depth){

    if (finv_1 == NULL)
        return;
    
    print_expr1(finv_1->expr ,depth);
    print_func_inv_aux2(finv_1->fiaux2 ,depth);
}

void print_func_inv_aux2(is_funcinvoc_aux2* is_fc2, int depth){
    if (is_fc2 == NULL)
        return;

    print_expr1(is_fc2->expr ,depth);
    print_func_inv_aux2(is_fc2->next ,depth);
}

void print_parseargs(is_parseargs* pa, int depth) {

    if (pa == NULL)
        return;

    sym_tables* table_aux = all_tables;
    table_element* aux1;
    
    while(table_aux != NULL){
        aux1 = search_el(pa->id, func_name);
        table_aux = table_aux->next;
    }

    for (int i=0; i<depth*2; i++)
            printf(".");
        if(strcmp(flag,"-s")==0)
            printf("ParseArgs - %s\n", aux1->type);
        else if(strcmp(flag,"-t")==0)
            printf("ParseArgs\n");
    depth++;

    print_id(pa->id, depth,0, "");
    print_expr1(pa->exp, depth);
}

void print_expr1(is_expr1* expr1, int depth) {

    if (expr1 == NULL)
        return;
        
    if (expr1->expr_or != NULL) {
        print_keyword(expr1->expr_or, depth, NULL);
        depth++;
        print_expr1(expr1->next, depth);
        print_expr2(expr1->expr2, depth);
    } else {
        print_expr2(expr1->expr2, depth);
        print_expr1(expr1->next, depth);
    }
}

void print_expr2(is_expr2* expr2, int depth) {

    if (expr2 == NULL)
        return;

    if (expr2->expr_and != NULL) {
        print_keyword(expr2->expr_and, depth, NULL);
        depth++;
        print_expr2(expr2->next, depth);
        print_expr3(expr2->expr3, depth); 
    } else {
        print_expr3(expr2->expr3, depth);
        print_expr2(expr2->next, depth);
    }
}

void print_expr3(is_expr3* expr3, int depth) {

    if (expr3 == NULL)
        return;

    if (expr3->expr_comp != NULL) {
        print_keyword(expr3->expr_comp, depth, NULL);
        depth++;
        print_expr3(expr3->next, depth);
        print_expr4(expr3->expr4, depth);
    } else {
        print_expr4(expr3->expr4, depth);
        print_expr3(expr3->next, depth);
    }
}

void print_expr4(is_expr4* expr4, int depth) {

    if (expr4 == NULL)
        return;

    if (expr4->expr_add_sub != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr4(expr4->next, func_name, func_type_print, op_type, 1);
        print_keyword(expr4->expr_add_sub, depth, (*op_type)->var_type);
        depth++;
        print_expr4(expr4->next, depth);
        print_expr5(expr4->expr5, depth);
    } else {
        print_expr5(expr4->expr5, depth);
        print_expr4(expr4->next, depth);
    }
}

void print_expr5(is_expr5* expr5, int depth) {

    if (expr5 == NULL)
        return;
    
    if (expr5->expr_mul_div != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr5(expr5->next, func_name, func_type_print, op_type, 1);
        print_keyword(expr5->expr_mul_div, depth, (*op_type)->var_type);
        depth++;
        print_expr5(expr5->next, depth);
        print_expr6(expr5->expr6, depth);
    } else {
        print_expr6(expr5->expr6, depth);
        print_expr5(expr5->next, depth);
    }
}

void print_expr6(is_expr6* expr6, int depth) {

    if (expr6 == NULL)
        return;

    if (expr6->expr_signal != NULL) {
        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        check_type_expr6(expr6->next, func_name, func_type_print, op_type, 1);
        print_keyword(expr6->expr_signal, depth, (*op_type)->var_type);
        depth++;
        print_expr6(expr6->next, depth);
        print_expr_aux1(expr6->expr_aux_1, depth);
    } else {
        print_expr_aux1(expr6->expr_aux_1, depth);
        print_expr6(expr6->next, depth);
    }
}

void print_expr_aux1(is_expr_aux1* exp_aux_1, int depth) {

    if (exp_aux_1 == NULL)
        return;

    if (exp_aux_1->type != NULL)
        print_id(exp_aux_1->id, depth, exp_aux_1->num, exp_aux_1->type->name);
    else if (exp_aux_1->id != NULL) {

        table_element* aux_search = search_el(exp_aux_1->id, func_name);

        if (aux_search == NULL)
            print_id(exp_aux_1->id, depth, exp_aux_1->num, "undefined");
        else
            print_id(exp_aux_1->id, depth, exp_aux_1->num, aux_search->type);
            
    } else
        print_id(exp_aux_1->id, depth, exp_aux_1->num, "");
    
    print_func_inv(exp_aux_1->funcinvoc, depth);
    print_expr1(exp_aux_1->expr, depth);
}

void print_keyword(is_keyword* kw, int depth, char* type) {
    
    if (kw == NULL)
        return;

    for (int i=0; i<depth*2; i++)
        printf(".");

    if(strcmp(kw->op, "Eq")==0 || 
        strcmp(kw->op, "Lt")==0 ||
        strcmp(kw->op, "Ne")==0 ||
        strcmp(kw->op, "And")==0 ||
        strcmp(kw->op, "Or")==0 ||
        strcmp(kw->op, "Gt")==0 ||
        strcmp(kw->op, "Le")==0 ||
        strcmp(kw->op, "Not")==0 ||
        strcmp(kw->op, "Ge")==0){
        
        if(strcmp(flag,"-s")==0)
            printf("%s - bool\n", kw->op);
        else if(strcmp(flag,"-t")==0)
            printf("%s\n",kw->op);
        }
    else if((strcmp(kw->op, "Mul")==0 || 
        strcmp(kw->op, "Div")==0 ||
        strcmp(kw->op, "Mod")==0 ||
        strcmp(kw->op, "Add")==0 ||
        strcmp(kw->op, "Plus")==0 ||
        strcmp(kw->op, "Minus")==0 ||
        strcmp(kw->op, "Sub")==0)){
        
        if(strcmp(flag,"-s")==0)
            printf("%s - %s\n", kw->op, type);
        else if(strcmp(flag,"-t")==0)
            printf("%s\n",kw->op);
        }
    else
        printf("%s\n", kw->op);
}

void free_program(is_program* ip){
    free_declaration_list(ip->dlist);
}

void free_declaration_list(is_declaration_list *idl) {
  while(idl != NULL) {
    free_declaration(idl->decl);
    is_declaration_list *tmp = idl;
    idl = idl->next;
    free(tmp);
  }
}

void free_declaration(is_decl *id) {
    free_vardec(id->vd);
    free_func_decl(id->fd);
    //free_symtab(id->symtab);
    //free(id);
}

void free_vardec(is_vardecl *ivd) {
    if(ivd != NULL)
        free_varspec(ivd->varspec);
    free(ivd);
}

void free_varspec(is_varspec *ivs){
    free(ivs->id);
    free(ivs->type);
    free(ivs->vslist);
    free(ivs);
}

void free_spec_list(is_varspec_list *ivl) {
  while(ivl != NULL) {
    is_varspec_list *tmp = ivl;
    ivl = ivl->next;
    free(tmp);
  }
}

void free_type(is_type *it){
    
    free(it->name);
    free(it->type);
    free(it);
}

void free_func_decl(is_funcdecl* fd){
    free_funcbody(fd->funcbody);
    //free_parameters(fd->param->param);
    free_type_func(fd->type);
    free(fd->id);
    free(fd);
}

void free_type_func(is_type_func* it){
    if(it != NULL){
    free(it->type->name);
    free(it->type->type);
    free(it->type);
    free(it);
    }
}

void free_parameters(is_param* ip){
    if(ip != NULL){
    free(ip->id);
    free(ip->type);
    free_parameters_aux1(ip->pmlist);
    }
}

void free_parameters_aux1(is_param_list *ip){
     while(ip != NULL) {
        free_parameters_aux2(ip->param);
        is_param_list *tmp = ip;
        ip = ip->next;
        free(tmp);
    }
}

void free_parameters_aux2(is_param_list_aux *ipl){
    free(ipl->id);
    free(ipl->type);
    free(ipl);
}

void free_funcbody(is_funcbody* fb){
    free_vars_statements(fb->vars_list);
}

void free_vars_statements(is_vars_statements* ivs){
     while(ivs != NULL) {
        free_vars_statements_aux(ivs->ivsa);
        is_vars_statements *tmp = ivs;
        ivs = ivs->next;
        free(tmp);
    }
}

void free_vars_statements_aux(is_vars_statements_aux* ivsa){
    if(ivsa != NULL){
        free_vardec(ivsa->vdlist);
        free_statement_list(ivsa->statement_list);
    }
}

void free_statement_list(is_statement* is){
    free_statement_aux1(is->aux1);
    free_statement_aux3(is->aux3);
    free_statement_aux4(is->aux4);
    free_statement_aux5(is->aux5);
    free_expr1(is->expr);
    free_func_Inv(is->funcinvoc);
    free(is->id);
    free_parseargs(is->parseargs);
}


void free_statement_aux1(is_statement_aux1* is1){
    while(is1 != NULL) {
        free_statement_aux2(is1->saux2);
        is_statement_aux1 *tmp = is1;
        is1 = is1->next;
        free(tmp);
    }
}

void free_statement_aux2(is_statement_aux2* is2){
    free_statement_list(is2->stat);
}

void free_statement_aux3(is_statement_aux3* is3){
    if(is3 != NULL)
        free_statement_aux1(is3->saux1);
}

void free_statement_aux4(is_only_expr* expr){ //Duvidosa
    if(expr!= NULL)
        free_expr1(expr->expr);    
}

void free_statement_aux5(is_statement_aux5* is5){
    if(is5 != NULL)
        free_expr1(is5->exp);
}


void free_parseargs(is_parseargs* ip){
    if(ip!=NULL){
        free_expr1(ip->exp);
        free(ip->id);
    }
}

void free_expr1(is_expr1* exp){
    while(exp != NULL) {
        free(exp->expr_or);
        free_expr2(exp->expr2);
        is_expr1 *tmp = exp;
        exp = exp->next;
        free(tmp);
    }
}

void free_expr2(is_expr2* exp){
    while(exp != NULL) {
        free(exp->expr_and);
        free_expr3(exp->expr3);
        is_expr2 *tmp = exp;
        exp = exp->next;
        free(tmp);
    }
}

void free_expr3(is_expr3* exp){
    while(exp != NULL) {
        free(exp->expr_comp);
        free_expr4(exp->expr4);
        is_expr3 *tmp = exp;
        exp = exp->next;
        free(tmp);
    }
}

void free_expr4(is_expr4* exp){
    while(exp != NULL) {
        free(exp->expr_add_sub);
        free_expr5(exp->expr5);
        is_expr4 *tmp = exp;
        exp = exp->next;
        free(tmp);
    }
}

void free_expr5(is_expr5* exp){
    while(exp != NULL) {
        free(exp->expr_mul_div);
        free_expr6(exp->expr6);
        is_expr5 *tmp = exp;
        exp = exp->next;
        free(tmp);
    }
}

void free_expr6(is_expr6* exp){
    while(exp != NULL) {
        free(exp->expr_signal);
        free_expr_aux1(exp->expr_aux_1);
        is_expr6 *tmp = exp;
        exp = exp->next;
        free(tmp);
    }
}

void free_expr_aux1(is_expr_aux1* iea){
    free_expr1(iea->expr);
    free_func_Inv(iea->funcinvoc);
    free(iea->id);
}

void free_func_Inv(is_funcinvoc* func_inv){
    if(func_inv != NULL){
    free_func_inv_aux1(func_inv->invoc_list);
    free(func_inv->id->id);
    free(func_inv->id);
    }
}

void free_func_inv_aux1(is_funcinvoc_aux1* func_inv_aux1){
    free_expr1(func_inv_aux1->expr);
    free_func_inv_aux2(func_inv_aux1->fiaux2);
}

void free_func_inv_aux2(is_funcinvoc_aux2* f_inv_2){
    while(f_inv_2 != NULL) {
        free_expr1(f_inv_2->expr);
        is_funcinvoc_aux2 *tmp = f_inv_2;
        f_inv_2 = f_inv_2->next;
        free(tmp);
    }
}


operation_type* return_all_types_from_expr(is_expr1* expr, char* funcname, char* func_type, bool get_wrong_types) {

    if (expr == NULL)
        return NULL;

    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    check_type_expr1(expr, funcname, func_type, op_type, get_wrong_types);

    return *op_type;
}

void check_type_expr1(is_expr1* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types) {

    if (expr == NULL)
        return;
    
    if (expr->next == NULL) {
        check_type_expr2(expr->expr2, funcname, func_type, op_type, get_wrong_types);
    }
    else if(expr->next != NULL && expr->expr2 != NULL) {
        char* aux = NULL;
        check_type_expr1(expr->next, funcname, func_type, op_type, get_wrong_types);
        aux = (*op_type)->var_type;
        check_type_expr2(expr->expr2,funcname, func_type, op_type, get_wrong_types);
        (*op_type)->op_type = "bool";
        if (strcmp(aux, (*op_type)->var_type) != 0)
            (*op_type)->var_type = "undef";
    }
}

void check_type_expr2(is_expr2* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types) {

    if (expr == NULL)
        return;

    if (expr->next == NULL) {
        check_type_expr3(expr->expr3, funcname, func_type, op_type, get_wrong_types);
    }
    else if(expr->next != NULL && expr->expr3 != NULL) {
        char* aux = NULL;
        check_type_expr2(expr->next, funcname, func_type, op_type, get_wrong_types);
        aux = (*op_type)->var_type;
        check_type_expr3(expr->expr3,funcname, func_type, op_type, get_wrong_types);
        (*op_type)->op_type = "bool";
        if (strcmp(aux, (*op_type)->var_type) != 0)
            (*op_type)->var_type = "undef";
    }
}

void check_type_expr3(is_expr3* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types) {

    if (expr == NULL)
        return;

    if (expr->next == NULL) {
        check_type_expr4(expr->expr4, funcname, func_type, op_type, get_wrong_types);
    }
    else if(expr->next != NULL && expr->expr4 != NULL) {
        char* aux = NULL;
        check_type_expr3(expr->next, funcname, func_type, op_type, get_wrong_types);
        aux = (*op_type)->var_type;
        check_type_expr4(expr->expr4,funcname, func_type, op_type, get_wrong_types);
        (*op_type)->op_type = "bool";
        if (strcmp(aux, (*op_type)->var_type) != 0)
            (*op_type)->var_type = "undef";
    }
}

void check_type_expr4(is_expr4* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types) {

    if (expr == NULL)
        return;

    if (expr->next == NULL) {
        check_type_expr5(expr->expr5, funcname, func_type, op_type, get_wrong_types);
    }
    else if(expr->next != NULL && expr->expr5 != NULL) {
        char* aux = NULL;
        check_type_expr4(expr->next, funcname, func_type, op_type, get_wrong_types);
        aux = (*op_type)->var_type;
        check_type_expr5(expr->expr5,funcname, func_type, op_type, get_wrong_types);
        (*op_type)->op_type = "int_float32";
        if (strcmp(aux, (*op_type)->var_type) != 0 || (strcmp(aux, "bool") == 0 && strcmp((*op_type)->var_type, "bool") == 0)) {
            (*op_type)->var_type = "undef";
        }
    }
}


void check_type_expr5(is_expr5* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types) {

    if (expr == NULL)
        return;

    if (expr->next == NULL) {
        check_type_expr6(expr->expr6, funcname, func_type, op_type, get_wrong_types);
    }
    else if(expr->next != NULL && expr->expr6 != NULL) {
        char* aux = NULL;
        check_type_expr5(expr->next, funcname, func_type, op_type, get_wrong_types);
        aux = (*op_type)->var_type;
        check_type_expr6(expr->expr6,funcname, func_type, op_type, get_wrong_types);
        (*op_type)->op_type = "int_float32";
        if (strcmp(aux, (*op_type)->var_type) != 0 || (strcmp(aux, "bool") == 0 && strcmp((*op_type)->var_type, "bool") == 0))
            (*op_type)->var_type = "undef";
    }
}


void check_type_expr6(is_expr6* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types) {

    if (expr == NULL)
        return;

    if (expr->next == NULL) {
        check_type_expr_aux(expr->expr_aux_1, funcname, func_type, op_type, get_wrong_types);
    }
    else if(expr->next != NULL && expr->expr_signal != NULL) {
        char* aux = NULL;
        check_type_expr6(expr->next, funcname, func_type, op_type, get_wrong_types);
        aux = (*op_type)->var_type;
        check_type_expr_aux(expr->expr_aux_1,funcname, func_type, op_type, get_wrong_types);
        (*op_type)->op_type = "int_float32_signal";
        if (strcmp(aux, (*op_type)->var_type) != 0)
            (*op_type)->var_type = "undef";
    }
}

void check_type_expr_aux (is_expr_aux1* expr, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types) {

    if (expr == NULL)
        return;

    if (expr->id != NULL) {
        char* type = NULL;
        table_element* search_aux;
        
        search_aux = search_el(expr->id, funcname);

        if (search_aux != NULL) {
            type = search_aux->type;
        }
        
        if (expr->type != NULL && type == NULL) {
            type = expr->type->name;
        }

        if (type == NULL) {
            type = "undef";
        }
        
        (*op_type)->var_name = expr->id->id;
        (*op_type)->op_type = "int_float32";
        (*op_type)->var_type = type;
    } else if (expr->expr != NULL) {
        check_type_expr1(expr->expr, funcname, func_type, op_type, get_wrong_types);
    } else if (expr->funcinvoc != NULL) {
        (*op_type)->is_func = 1;
        if (expr->funcinvoc->id != NULL) {
            table_element* search_aux = search_func(expr->funcinvoc->id);
            (*op_type)->op_type = "int_float32";
            (*op_type)->var_name = expr->funcinvoc->id->id;
            if (search_aux != NULL) {
                if (search_aux->is_func == 1) {
                    if (search_aux->type != NULL && search_aux->return_type != NULL && strcmp(search_aux->type, search_aux->return_type) == 0 && strcmp(search_aux->return_type, func_type) == 0) {
                        (*op_type)->op_type = "int_float32";
                        (*op_type)->var_type= search_aux->type;
                        return;
                    } else if (get_wrong_types == 1) {
                        (*op_type)->var_type= search_aux->type;
                    } else {
                        (*op_type)->var_type = "undef";
                    }
                } else {
                    if (search_aux->type != NULL)
                        (*op_type)->var_type = search_aux->type;
                    else
                        (*op_type)->var_type = "undef";
                }
            } else {
                search_aux = search_func(expr->funcinvoc->id);
                if (search_aux != NULL) {
                    if (strcmp(search_aux->type, search_aux->return_type) == 0 && strcmp(search_aux->return_type, func_type) == 0)
                        (*op_type)->var_type = search_aux->type;
                    else {
                        if (expr->type != NULL) {
                            (*op_type)->var_type = expr->type->name;
                        } else             
                            (*op_type)->var_type= "undef";
                    }
                } else {
                    if (expr->type != NULL) {
                        (*op_type)->var_type = expr->type->name;
                    } else             
                        (*op_type)->var_type= "undef";
                }
            }
        } else if (expr->funcinvoc->invoc_list != NULL) {
            if (expr->funcinvoc->invoc_list->expr != NULL)
                check_type_expr1(expr->funcinvoc->invoc_list->expr, funcname, func_type, op_type, get_wrong_types);
            else if (expr->funcinvoc->invoc_list->fiaux2 != NULL)
                check_func_invoc_expr(expr->funcinvoc->invoc_list->fiaux2, funcname, func_type, op_type, get_wrong_types);
        } else {
            if (expr->type != NULL) {
                (*op_type)->op_type = expr->type->name;
            } else             
                (*op_type)->var_type= "undef";
        }
    }
}

void check_func_invoc_expr(is_funcinvoc_aux2* fiaux2, char* funcname, char* func_type, operation_type** op_type, bool get_wrong_types) {

    if (fiaux2 == NULL)
        return;

    check_type_expr1(fiaux2->expr, funcname, func_type, op_type, get_wrong_types);
    (*op_type)->op_type = "int_float32";

    if (fiaux2->next == NULL)
        return;

     is_funcinvoc_aux2* temp = fiaux2->next;
     char* type_aux = (*op_type)->var_type;

    while (temp != NULL) {
        check_type_expr1(temp->expr, funcname, func_type, op_type, get_wrong_types);
        if (strcmp(type_aux, (*op_type)->var_type) != 0) {
            (*op_type)->var_type = "undef";
            return;
        }
        temp = temp->next;
    }
}


bool same_params_from_call_and_func(is_funcinvoc_aux1* call_func, table_element* table_func, char* funcname, char* func_type) {

    if (call_func == NULL || table_func == NULL)
        return false;

    int count_params_call_func = 0;
    int count_params_table_func = 0;

    table_element* aux_table_params;

    if (table_func->params !=  NULL) {
        aux_table_params = *table_func->params;

        for(; aux_table_params; aux_table_params = aux_table_params->next)
            count_params_table_func++;
    }

    if (call_func->fiaux2 == NULL && call_func->expr != NULL) {
        count_params_call_func++;
    }
    else if (call_func->fiaux2 != NULL) {
        count_params_call_func++;
        is_funcinvoc_aux2* temp = call_func->fiaux2;

        for(; temp != NULL; temp = temp->next)
            count_params_call_func++;
    }

    if (count_params_call_func != count_params_table_func)
        return false;

    aux_table_params = *table_func->params;
    is_funcinvoc_aux2* temp = call_func->fiaux2;
    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    if (count_params_table_func > 1) {
        check_type_expr1(call_func->expr, funcname, func_type, op_type, 1);
        if (strcmp((*op_type)->var_type, aux_table_params->type) != 0 && strcmp((*op_type)->op_type, "bool") != 0)
            return false;
        aux_table_params = aux_table_params->next;

        for (int i=0; i<count_params_table_func-1; i++) {

            check_type_expr1(temp->expr, funcname, func_type, op_type, 1);

            if (strcmp((*op_type)->var_type, aux_table_params->type) != 0 && strcmp((*op_type)->op_type, "bool") != 0)
                return false;

            aux_table_params = aux_table_params->next;
            temp = temp->next;
        }
    } else {
        check_type_expr1(call_func->expr, funcname, func_type, op_type, 1);
        
        if (strcmp((*op_type)->var_type, aux_table_params->type) != 0 && strcmp((*op_type)->op_type, "bool") != 0)
            return false;
    }

    return true;
}


char* get_return_type_func(is_funcbody* funcbody, char* funcname, char* func_type) {

    if (funcbody == NULL || funcbody->vars_list == NULL)
        return "undef";

    is_vars_statements* aux_ivsa = funcbody->vars_list;

    for(; aux_ivsa; aux_ivsa = aux_ivsa->next) {
        if (aux_ivsa->ivsa != NULL && aux_ivsa->ivsa->statement_list != NULL && aux_ivsa->ivsa->statement_list->kw != NULL) {
            if (strcmp(aux_ivsa->ivsa->statement_list->kw, "Return") == 0 && aux_ivsa->ivsa->statement_list->aux4 != NULL) {
                return return_all_types_from_expr(aux_ivsa->ivsa->statement_list->aux4->expr, funcname, func_type, 0)->var_type;
            }
        }
    }

    return "undef";
}


void free_table(table** reg_table) {

    if (*reg_table == NULL)
        return;

    table* tmp;

    while (*reg_table != NULL) {
        tmp = *reg_table;
        *reg_table = (*reg_table)->next;
        free(tmp);
    }
}

void add_reg_table(table** head, int reg, char* var_name, char* var_type) {

    if (head == NULL)
        return;

    table* new_reg = (table*)malloc(sizeof(table));
    table* aux = NULL;
    table* previous = NULL;

    new_reg->register_num = reg;
    new_reg->var_name = var_name;
    new_reg->var_type = var_type;
    new_reg->next = NULL;

    if (*head == NULL) {
        *head = new_reg;
        return;
    }

    for (aux = *head; aux; previous=aux, aux=aux->next);
    previous->next=new_reg;
}


table* search_reg_table(table* head, int reg) {

    if (head == NULL)
        return NULL;

    table *temp = head;


    for (; temp; temp = temp->next) {
        if (temp->register_num == reg)
            return temp;
    }

    return NULL;
}

int search_reg_table_by_var_name(table* head, char* var_name) {

    if (head == NULL)
        return -1;

    table *temp;

    for (temp = head; temp; temp = temp->next) {
        if (strcmp(temp->var_name, var_name) == 0)
            return temp->register_num;
    }

    return -1;
}


void insert_string_array(strings_array** head, char* string) {

    strings_array* temp_string = (strings_array*)malloc(sizeof(strings_array));

    temp_string->string = string;
    temp_string->next = NULL;

    if (*head == NULL) {
        *head = temp_string;
        return;
    }

    strings_array* temp_array;
    for (temp_array = *head; temp_array->next; temp_array = temp_array->next);
    temp_array->next = temp_string;
}

void insert_string_list(str_tams** head, int str_num, int tam, char* str_name) {

    if (head == NULL)
        return;

    str_tams* new_str = (str_tams*)malloc(sizeof(str_tams));
    str_tams* aux = NULL;
    str_tams* previous = NULL;

    new_str->str_num = str_num;
    new_str->str_len = tam;
    new_str->string = str_name;
    new_str->next = NULL;

    if (*head == NULL) {
        *head = new_str;
        return;
    }

    for (aux = *head; aux; previous=aux, aux=aux->next);
    previous->next=new_str;
}


int search_string_tam(str_tams* head, int str_num) {

    if (head == NULL)
        return -1;

    str_tams* temp;

    for (temp=head; temp; temp=temp->next)
        if (temp->str_num == str_num)
            return temp->str_len;

    return -1;
}


int search_string_num(str_tams* head, char* str) {

    if (head == NULL)
        return -1;

    str_tams* temp;

    for (temp=head; temp; temp=temp->next)
        if (strcmp(str, temp->string) == 0)
            return temp->str_num;

    return -1;
}
