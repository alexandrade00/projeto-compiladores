#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "functions.h"
#include "semantics.h"
#include "structures.h"
#include "var.h"
#include "ctype.h"
#include "string.h"

#define MAX_LABELS_COUNTER 30

int count_reg = 1;
int count_if = 1;
int count_loop = 1;
int count_str = 1;
char* last_type;
bool main_returned = 0;
bool inside_func = 0;
bool returned_called = 0;
char* func_name_gen;
char* func_type;
table** reg_table;
str_tams** str_tam;

void generate_program(is_program* ip) {

    if (ip==NULL)
        return;

    returned_called = 0;

    reg_table = (table**)malloc(sizeof(table));
    *reg_table = NULL;

    str_tam = (str_tams**)malloc(sizeof(str_tams));
    *str_tam = NULL;

    //show_table(all_tables);

    generate_string_declaration();
    count_str = 1;
    generate_declaration_list(ip->dlist);
    generate_functions();
}


void generate_declaration_list(is_declaration_list* dl) {

    if (dl == NULL)
        return;
    
    generate_declaration(dl->decl);
    generate_declaration_list(dl->next);
}

void generate_declaration(is_decl* decl) {

    if (decl == NULL)
        return;

    generate_vardecl(decl->vd);
    generate_funcdecl(decl->fd);
}


void generate_vardecl(is_vardecl* vdl) {

    if (vdl == NULL || returned_called == 1)
        return;

    generate_varspec(vdl->varspec);
}

void generate_varspec(is_varspec* vs) {

    if (vs == NULL || returned_called == 1)
        return;
    
    table_element* aux = search_el(vs->id, NULL);

    if(aux != NULL && inside_func == 0) {
        if(strcmp(vs->type->name,"int")==0)
            printf("@%s = common global i32 0\n",vs->id->id);
        else if(strcmp(vs->type->name,"float32")==0)
            printf("@%s = common global double 0.000000e+00\n",vs->id->id);    
        else if(strcmp(vs->type->name,"bool")==0)
            printf("@%s = common global i1 0\n",vs->id->id);
        else if(strcmp(vs->type->name,"string")==0)
            printf("@%s = internal constant [1 x i8] c\"\\00\"\n",vs->id->id);
    } else {
        char* type = NULL;
        if(strcmp(vs->type->name, "int")==0) {
            printf("    %%%d = alloca i32\n", count_reg);
            printf("    store i32 0, i32* %%%d\n", count_reg);
            type = "i32";
        } else if(strcmp(vs->type->name, "float32")==0) {
            printf("    %%%d = alloca double\n", count_reg);
            printf("    store double 0.0, double* %%%d\n", count_reg);
            type = "double";    
        } else if(strcmp(vs->type->name, "bool")==0) {
            printf("    %%%d = alloca i1\n", count_reg);
            printf("    store i1 0, i1* %%%d\n", count_reg);
            type = "i1";
        } else if(strcmp(vs->type->name, "string")==0) {
            printf("    %%%d = alloca i8\n", count_reg++);
            printf("    store i8 0, i8* %%%d\n", count_reg-1);
            printf("    %%%d = alloca i8*\n", count_reg);
            printf("    store i8* %%%d, i8** %%%d\n", count_reg-1, count_reg);
            type = "i8*";
        }
        add_reg_table(reg_table, count_reg, vs->id->id, type);
        count_reg++;
    }
    generate_varspec_list(vs->vslist, vs->type);
}

void generate_varspec_list(is_varspec_list* vslist, is_type* type) {

    if (vslist == NULL || returned_called == 1)
        return;

    for (; vslist; vslist = vslist->next) {

        if(search_el(vslist->id, NULL) != NULL && inside_func == 0) {
            if(strcmp(type->name,"int")==0)
                printf("@%s = common global i32 0\n",vslist->id->id); 
            else if(strcmp(type->name,"float32")==0)
                printf("@%s = common global double 0.000000e+00\n",vslist->id->id); 
            else if(strcmp(type->name,"bool")==0)
                printf("@%s = common global i1 0\n",vslist->id->id);
            else if(strcmp(type->name,"string")==0)
                printf("@%s = internal constant [1 x i8] c\"\\00\"\n", vslist->id->id);
        } else {
            if(strcmp(type->name, "int")==0) {
                printf("    %%%d = alloca i32\n", count_reg);
                printf("    store i32 0, i32* %%%d\n", count_reg);
            } else if(strcmp(type->name, "float32")==0) {
                printf("    %%%d = alloca double\n", count_reg);
                printf("    store double 0.0, double* %%%d\n", count_reg);
            } else if(strcmp(type->name, "bool")==0) {
                printf("    %%%d = alloca i1\n", count_reg);
                printf("    store i1 0, i1* %%%d\n", count_reg);
            } else if(strcmp(type->name, "string")==0) {
                printf("    %%%d = alloca i8\n", count_reg++);
                printf("    store i8 0, i8* %%%d\n", count_reg-1);
                printf("    %%%d = alloca i8*\n", count_reg);
                printf("    store i8* %%%d, i8** %%%d\n", count_reg-1, count_reg);
            }
            add_reg_table(reg_table, count_reg, vslist->id->id, type->name);
            count_reg++;
        }
    }
}

void generate_funcdecl(is_funcdecl* ifd) {

    if (ifd == NULL)
        return;

    count_reg = 1;
    count_if = 1;
    count_loop = 1;
    main_returned = 0;
    returned_called = 0;
    inside_func = 1;
    reg_table = (table**)malloc(sizeof(table));
    *reg_table = NULL;

    if(ifd->id != NULL) {
        func_name_gen = ifd->id->id;
        if (ifd->type != NULL)
            func_type = ifd->type->type->name;
        else
            func_type = "none";

        char* type = generate_print_type_func(ifd->type);
        if(strcmp(type,"float32")==0)
            type = "double";
        else if(strcmp(type,"int")==0)
            type = "i32";
        else if(strcmp(type,"bool")==0)
            type = "i1";
        else if(strcmp(type,"string")==0)
            type = "i8*";

        if (strcmp(ifd->id->id, "main") == 0) {
            printf("\ndefine i32 @main(i32 %%argc, i8** %%argv");
        } else {
            printf("\ndefine %s @%s(", type, func_name_gen);
            generate_param_aux(ifd->param);
        }
        printf(") {\n");
    }

    if(ifd->param != NULL) {
        count_reg--;
        char* type;
        if(strcmp(ifd->param->param->type->name,"int")==0)
            type = "i32";
        else if(strcmp(ifd->param->param->type->name, "float32")==0)
            type = "double";
        else if(strcmp(ifd->param->param->type->name, "bool")==0)
            type = "i1";
        else if(strcmp(ifd->param->param->type->name, "string")==0)
            type = "i8*";

        printf("    %%%d = alloca %s\n", count_reg, type);
        printf("    store %s %%%s, %s* ", type, ifd->param->param->id->id, type);
        printf("%%%d\n", count_reg);
        add_reg_table(reg_table, count_reg, ifd->param->param->id->id, type);
        count_reg++;

        is_param_list *temp = ifd->param->param->pmlist;
        for(; temp; temp=temp->next) {
            if(strcmp(temp->param->type->name,"int")==0)
                type = "i32";
            else if(strcmp(temp->param->type->name, "float32")==0)
                type = "double";
            else if(strcmp(temp->param->type->name, "bool")==0)
                type = "i1";
            else if(strcmp(temp->param->type->name, "string")==0)
                type = "i8*";

            printf("    %%%d = alloca %s\n", count_reg, type);
            printf("    store %s %%%s, %s* ", type, temp->param->id->id, type);
            printf("%%%d\n", count_reg);
            add_reg_table(reg_table, count_reg, temp->param->id->id, type);
            count_reg++;
        }
        
    }

    generate_funcbody(ifd->funcbody);
    if (strcmp(ifd->id->id, "main") == 0)
        printf("    ret i32 0\n");
    if (main_returned == 0 && strcmp(ifd->id->id, "main") != 0) {
        if (ifd->type == NULL)
            printf("    ret void\n");
        else if (strcmp(ifd->type->type->name, "int") == 0)
            printf("    ret i32 0\n");
        else if (strcmp(ifd->type->type->name, "float32") == 0)
            printf("    ret double 0.0\n");
        else if (strcmp(ifd->type->type->name, "bool") == 0)
            printf("    ret i1 0\n");
        else if (strcmp(ifd->type->type->name, "string") == 0)
            printf("    ret i8* 0\n");
        
    }
    
    printf("}\n");
    inside_func = 0;
}

char* generate_print_type_func(is_type_func* itf) {

    if (itf == NULL)
        return "void";
    char* type;
    type = generate_print_type(itf->type);
    return type;
}

char* generate_print_type(is_type* tp) {
    
    if (tp == NULL)
        return "";

    return tp->name;
}

void generate_param_aux(is_param_aux* ipa) {

    if (ipa == NULL) {
        return;
    }
    //ParamDecl
    generate_param(ipa->param);
}

void generate_param(is_param* ip) {

    if (ip == NULL)
        return;
    
    char* type = generate_print_type(ip->type);
    char* keyword;
    
    if(strcmp(type,"int")==0)
        keyword = "i32";
    else if(strcmp(type,"float32")==0)
        keyword = "double";
    else if(strcmp(type, "bool")==0)
        keyword = "i1";
    else if(strcmp(type, "string")==0)
        keyword = "i8*";

    printf("%s %%%s", keyword,ip->id->id);
    count_reg++;
    if (ip->pmlist != NULL) {
        printf(", ");
        generate_param_list(ip->pmlist);
    }
}

void generate_param_list(is_param_list* ipl) {

    if (ipl == NULL)
        return;

    generate_param_list_aux(ipl->param);
    if (ipl->next != NULL)
        printf(", ");
    generate_param_list(ipl->next);
}

void generate_param_list_aux(is_param_list_aux* ipla) {

    if (ipla == NULL)
        return;

    char* type = generate_print_type(ipla->type);
    char* keyword;
    
    if(strcmp(type,"int")==0)
        keyword = "i32";
    else if(strcmp(type,"float32")==0)
        keyword = "double";
    else if(strcmp(type, "bool")==0)
        keyword = "i1";
    else if(strcmp(type, "string")==0)
        keyword = "i8*";

    printf("%s %%%s",keyword,ipla->id->id);
}

void generate_funcbody(is_funcbody* ifb) {

    if (ifb == NULL) {
        return;
    }

    //count_reg = 1;
    generate_vars_list(ifb->vars_list);
}

void generate_vars_list(is_vars_statements* ivs) {

    if (ivs == NULL) {
        return;
    }

    generate_vars_list_aux(ivs->ivsa);
    generate_vars_list(ivs->next);    
}

void generate_vars_list_aux(is_vars_statements_aux* ivsa) {

    if (ivsa == NULL)
        return;

    generate_vardecl(ivsa->vdlist);
    generate_statement(ivsa->statement_list);
}

void generate_statement(is_statement* is) {
    
    if (is == NULL)
        return;

    if (is->id != NULL) {
        table_element* aux = search_el(is->id, func_name_gen);

        if (strcmp(is->kw, "Assign") == 0 && returned_called == 0) {
            generate_expr1(is->expr);
            int reg = search_reg_table_by_var_name(*reg_table, is->id->id);

            if (reg == -1 || aux->is_param == true) {
                char* type;
                reg = search_reg_table_by_var_name(*reg_table, aux->name->id);
                
                if(strcmp(aux->type,"int")==0)
                    type = "i32";
                else if(strcmp(aux->type, "float32")==0)
                    type = "double";
                else if(strcmp(aux->type, "bool")==0)
                    type = "i1";
                else if(strcmp(aux->type, "string")==0)
                    type = "i8*";

                
                if (aux->is_param == true) {
                    if (reg == -1) {
                        printf("    %%%d = alloca %s\n", count_reg, type);
                        printf("    store %s %%%d, %s* ", type, count_reg-1, type);
                        printf("%%%d\n", count_reg);
                        add_reg_table(reg_table, count_reg, aux->name->id, type);
                        count_reg++;
                    } else {
                        printf("    store %s %%%d, %s* ", type, count_reg-1, type);
                        printf("%%%d\n", reg);
                    }
                } else {
                    printf("    store %s %%%d, %s* ", type, count_reg-1, type);
                    printf("@%s\n", is->id->id);
                }

            } else {
                if(strcmp(aux->type,"int")==0)
                    printf("    store i32 %%%d, i32* %%%d\n", count_reg-1, reg);
                else if(strcmp(aux->type,"float32")==0)
                    printf("    store double %%%d, double* %%%d\n", count_reg-1, reg);
                else if(strcmp(aux->type, "bool")==0)
                    printf("    store i1 %%%d, i1* %%%d\n", count_reg-1, reg);
            }
        }
    }

    if (is->kw != NULL && strcmp(is->kw, "Return") == 0) {

        main_returned = 1;

        if (is->aux4 == NULL)
            return;

        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));
        table* reg = NULL;
        int reg_num = -1;
        bool expr1_exist = false;

        if (is->aux4->expr != NULL) {
            generate_expr1(is->aux4->expr);
            expr1_exist = true;
        }

        check_type_expr1(is->aux4->expr, func_name_gen, func_type, op_type, 0);

        char* pointer_end1;
        errno = 0;
        strtol((*op_type)->var_name, &pointer_end1, 10);
        if (errno == 0 && pointer_end1 != (*op_type)->var_name && *pointer_end1 == 0)
            (*op_type)->var_type = "int";
        else {
            char* pointer_end2;
            errno = 0;
            strtold((*op_type)->var_name, &pointer_end2);
            if (errno == 0 && pointer_end2 != (*op_type)->var_name && *pointer_end2 == 0)
                (*op_type)->var_type = "float32";
        }   
        if (strcmp((*op_type)->var_type, "int") != 0 && strcmp((*op_type)->var_type, "float32") != 0) {
            reg_num = search_reg_table_by_var_name(*reg_table, (*op_type)->var_name);
            if (reg_num != -1)
                reg = search_reg_table(*reg_table, reg_num);
        }
        

        id_token* func = (id_token*)malloc(sizeof(id_token));
        func->id = func_name_gen;
        table_element* temp_func = search_func(func);

        if (reg != NULL && reg->var_type != NULL) {
            printf("    ret %s %%%d\n", reg->var_type, count_reg-1);
            last_type = reg->var_type;
        } else if (temp_func->type == NULL) {
            printf("    ret void\n");
        } else if (expr1_exist == false) {
            if (strcmp(temp_func->type, "int") == 0) {
                printf("    ret i32 %%%s\n", (*op_type)->var_name);
                last_type = "i32";
            } else if (strcmp(temp_func->type, "float32") == 0) {
                printf("    ret double %%%s\n", (*op_type)->var_name);
                last_type = "double";
            } else if (strcmp(temp_func->type, "bool") == 0) {
                printf("    ret i1 %%%s\n", (*op_type)->var_name);
                last_type = "i1";
            } else if (strcmp(temp_func->type, "string") == 0) {
                printf("    ret i8* %%%s\n", (*op_type)->var_name);
                last_type = "i8*";
            }
        } else {
            if (strcmp(temp_func->type, "int") == 0) {
                printf("    ret i32 %%%d\n", count_reg-1);
                last_type = "i32";
            } else if (strcmp(temp_func->type, "float32") == 0) {
                printf("    ret double %%%d\n", count_reg-1);
                last_type = "double";
            } else if (strcmp(temp_func->type, "bool") == 0) {
                printf("    ret i1 %%%d\n", count_reg-1);
                last_type = "i1";
            } else if (strcmp(temp_func->type, "string") == 0) {
                printf("    ret i8* %%%d\n", count_reg-1);
                last_type = "i8*";
            }
        }
        if (returned_called == 1)
            count_reg++;
        returned_called = 1;
        return;

    } else if(is->kw != NULL && strcmp(is->kw,"If") == 0 && returned_called == 0) {

        generate_expr1(is->expr);
        char _then[MAX_LABELS_COUNTER], _else[MAX_LABELS_COUNTER];
        sprintf(_then, "then%d", count_if);
        sprintf(_else, "else%d", count_if);
        printf("    br i1 %%%d, label %%%s, label %%%s\n", count_reg-1, _then, _else);
        generate_labels(is);

    } else if (is->kw != NULL && strcmp(is->kw, "For") == 0 && returned_called == 0) {

        char _loop_param[MAX_LABELS_COUNTER];
        sprintf(_loop_param, "loop_param%d", count_loop);
        printf("    br label %%%s\n\n", _loop_param);
        printf("%s:\n", _loop_param);

        if(is->aux4 != NULL)
            generate_expr1(is->aux4->expr);

        char _loop[MAX_LABELS_COUNTER];
        char _loop_cont[MAX_LABELS_COUNTER];
        sprintf(_loop, "loop%d", count_loop);
        sprintf(_loop_cont, "loop_cont%d", count_loop++);
        printf("    br i1 %%%d, label %%%s, label %%%s\n\n", count_reg-1, _loop, _loop_cont);

        printf("%s:\n", _loop);
        generate_statement_aux1(is->aux1);
        printf("    br label %%%s\n\n", _loop_param);

        printf("%s:\n", _loop_cont);
    } else if (is->kw && strcmp(is->kw, "Print") == 0 && returned_called == 0) {
        if (is->aux5->id != NULL) {
            int str_num = search_string_num(*str_tam, is->aux5->id->id);
            int strlit_len = search_string_tam(*str_tam, str_num);
            printf("    %%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([%d x i8], [%d x i8]* @.str.%d, i64 0, i64 0))\n",
                count_reg++, strlit_len, strlit_len, str_num);

        } else if (is->aux5->exp) {
            generate_expr1(is->aux5->exp);
            if (strcmp(last_type, "i32") == 0) {
                printf("    %%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), %s %%%d)\n",
                count_reg, last_type, count_reg-1);
            } else if (strcmp(last_type, "i8*") == 0) {
                printf("    %%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_strings, i64 0, i64 0), i8* %%%d)\n",
                count_reg, count_reg-1);
            } else if (strcmp(last_type, "i1") == 0) {
                printf("    %%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @print_ints, i64 0, i64 0), %s %%%d)\n",
                count_reg, last_type, count_reg-1);
            } else {
                printf("    %%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @print_floats, i64 0, i64 0), double %%%d)\n",
                count_reg, count_reg-1);
            }
            count_reg++;
        }
    }
    
    if (returned_called == 0) {
        generate_func_inv(is->funcinvoc);
        generate_parseargs(is->parseargs);
    }
}

void generate_labels(is_statement* is) {
    char _then[MAX_LABELS_COUNTER], _else[MAX_LABELS_COUNTER], _ifcont[MAX_LABELS_COUNTER];
    sprintf(_then, "then%d", count_if);
    sprintf(_else, "else%d", count_if);
    sprintf(_ifcont, "ifcont%d", count_if++);

    printf("\n%s:\n", _then);
    generate_statement_aux1(is->aux1);
    check_return_inside_if_else(is->aux1);
    if (returned_called == 0)
        printf("    br label %%%s\n\n", _ifcont);

    if (is->aux3 != NULL) {
        printf("%s:\n", _else);
        //count_reg++;
        generate_statement_aux3(is->aux3, _ifcont);
        check_return_inside_if_else(is->aux3->saux1);
    } else {
        printf("%s:\n", _else);
        printf("    br label %%%s\n\n", _ifcont);

        printf("%s:\n", _ifcont);
        //count_reg++;
    }
    //count_reg++;
}

void check_return_inside_if_else(is_statement_aux1* state1) {

    for (; state1; state1 = state1->next) {
        if (state1->saux2->stat->aux4 != NULL)
            main_returned = 0;
    }
}

int count_ifs(is_statement* is) {

    int counter = 1;
    if (is->aux1 != NULL)
        counter = counter_if(is->aux1, counter);
    if (is->aux3 != NULL)
        counter = counter_if(is->aux3->saux1, counter);
    
    return counter;
}

int counter_if(is_statement_aux1* state1, int counter) {

    for (; state1; state1 = state1->next) {
        if (state1->saux2 != NULL && state1->saux2->stat->kw != NULL) {
            if (strcmp(state1->saux2->stat->kw, "If") == 0) {
                counter++;
                if (state1->saux2->stat->aux1 != NULL)
                    counter = counter_if(state1->saux2->stat->aux1, counter);
                if (state1->saux2->stat->aux3 != NULL)
                    counter = counter_if(state1->saux2->stat->aux3->saux1, counter);
            }
        }
    }

    return counter;
}

void generate_only_expr(is_only_expr* st4) {

    if (st4 == NULL)
        return;
    
    generate_expr1(st4->expr);
}

void generate_statement_aux1(is_statement_aux1* is_st) {

    if (is_st == NULL)
        return;

    generate_statement_aux2(is_st->saux2);
    generate_statement_aux1(is_st->next);

}

void generate_statement_aux2(is_statement_aux2* is_st2) {

    if (is_st2 == NULL)
        return;
    
    generate_statement(is_st2->stat);
}

void generate_statement_aux3(is_statement_aux3* is_st3, char* _ifcont) {
    
     if (is_st3 == NULL)
        return;

    generate_statement_aux1(is_st3->saux1);
    printf("    br label %%%s\n\n", _ifcont);

    printf("%s:\n", _ifcont);
}

void generate_statement_aux5(is_statement_aux5* is_st5) {

    if (is_st5 == NULL)
        return;

    generate_expr1(is_st5->exp);
}

void generate_func_inv(is_funcinvoc* finv) {

    if (finv == NULL)
        return;

    table_element* temp_func = search_func(finv->id);

    if(temp_func == NULL)
        return;

    if(temp_func->type != NULL) {
       
        table** params_regs =  (table**)malloc(sizeof(table));
        *params_regs = NULL;
        generate_func_inv_aux1(finv->invoc_list, params_regs);

        char * type;
        if(strcmp(temp_func->type,"int")==0)
            type = "i32";
        if(strcmp(temp_func->type,"float32")==0)
            type = "double";
        if(strcmp(temp_func->type,"bool")==0)
            type = "i1";
        if(strcmp(temp_func->type,"string")==0) {
            table_element* aux = search_el(temp_func->name, NULL);
            if (aux == NULL)
                type = "i8*";
            else
                type = "[1 x i8]";
        }

        printf("    %%%d = call %s @%s(", count_reg++, type, finv->id->id);
        last_type = type;
        
        if (temp_func->params != NULL) {
            table_element* params = *(temp_func->params);

            for (; params; params=params->next) {

                if(strcmp(params->type, "int")==0)
                    type = "i32";
                if(strcmp(params->type, "float32")==0)
                    type = "double";
                if(strcmp(params->type, "bool")==0)
                    type = "i1";
                if(strcmp(params->type, "string")==0) {
                    type = "i8*";
                }                

                printf("%s %%%d", type, (*params_regs)->register_num);
                
                if (params->next != NULL)
                    printf(", ");

                *params_regs = (*params_regs)->next; 
            }
        }
        printf(")\n");
    }
    else {

        table** params_regs =  (table**)malloc(sizeof(table));
        *params_regs = NULL;
        generate_func_inv_aux1(finv->invoc_list, params_regs);

        printf("    call void @%s(", finv->id->id);
        
        if (temp_func->params != NULL) {
            table_element* params = *(temp_func->params);
            char* type;

            for (; params; params=params->next) {

                if(strcmp(params->type, "int")==0)
                    type = "i32";
                if(strcmp(params->type, "float32")==0)
                    type = "double";
                if(strcmp(params->type, "bool")==0)
                    type = "i1";
                if(strcmp(params->type, "string")==0) {
                    type = "i8*";
                }
                last_type = type;             

                printf("%s %%%d", type, (*params_regs)->register_num);
                
                if (params->next != NULL)
                    printf(", ");

                *params_regs = (*params_regs)->next; 
            }
        }
        printf(")\n");
    }
}

void generate_func_inv_aux1(is_funcinvoc_aux1* finv_1, table** params_regs) {

    if (finv_1 == NULL)
        return;

    generate_expr1(finv_1->expr);
    add_reg_table(params_regs, count_reg-1, NULL, NULL);
    generate_func_inv_aux2(finv_1->fiaux2, params_regs);
}

void generate_func_inv_aux2(is_funcinvoc_aux2* is_fc2, table** params_regs) {

    if (is_fc2 == NULL)
        return;

    generate_expr1(is_fc2->expr);
    add_reg_table(params_regs, count_reg-1, NULL, NULL);
    generate_func_inv_aux2(is_fc2->next, params_regs);
}

void generate_parseargs(is_parseargs* pa) {

    if (pa == NULL)
        return;

    /* generate_expr1(pa->exp);
    int arg_index = count_reg-1; */
    int reg = search_reg_table_by_var_name(*reg_table, pa->exp->expr2->expr3->expr4->expr5->expr6->expr_aux_1->id->id);
    char var[512];
    if (reg == -1)
        strcpy(var, pa->exp->expr2->expr3->expr4->expr5->expr6->expr_aux_1->id->id);
    else {
        printf("    %%%d = load i32, i32* %%%d\n", count_reg++, reg);
        sprintf(var, "%%%d", count_reg-1);
    }

    printf("    %%%d = getelementptr i8*, i8** %%argv, i32 %s\n", count_reg, var);
    count_reg++;
    printf("    %%%d = load i8*, i8** %%%d\n", count_reg, count_reg-1);
    count_reg++;
    printf("    %%%d = call i32 @atoi(i8* %%%d)\n", count_reg, count_reg-1);
    count_reg++;

    table_element* aux = search_el(pa->id, func_name_gen);
    reg = search_reg_table_by_var_name(*reg_table, pa->id->id);

    if (reg == -1) {
        if(strcmp(aux->type, "int")==0)
            printf("    store i32 %%%d, i32* @%s\n", count_reg-1, pa->id->id);
        else if(strcmp(aux->type, "float32")==0)
            printf("    store double %%%d, double* @%s\n", count_reg-1, pa->id->id);
        else if(strcmp(aux->type, "bool")==0)
            printf("    store i1 %%%d, i1* @%s\n", count_reg-1, pa->id->id);
    } else {
        if(strcmp(aux->type,"int")==0)
            printf("    store i32 %%%d, i32* %%%d\n", count_reg-1, reg);
        else if(strcmp(aux->type,"float32")==0)
            printf("    store double %%%d, double* %%%d\n", count_reg-1, reg);
        else if(strcmp(aux->type, "bool")==0)
            printf("    store i1 %%%d, i1* %%%d\n", count_reg-1, reg);
    }
}

void generate_expr1(is_expr1* expr1) {
    
    if (expr1 == NULL)
        return;

    if (expr1->expr_or != NULL) {
        generate_expr1(expr1->next);
        int reg1 = count_reg-1;
        generate_expr2(expr1->expr2);
        int reg2 = count_reg-1;
        check_op_expr1(expr1, reg1, reg2);
    } else 
        generate_expr2(expr1->expr2);
}

void check_op_expr1(is_expr1* expr, int registo1, int registo2){

    if (expr == NULL)
        return;
        
    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    check_type_expr1(expr, func_name_gen, func_type, op_type, 0);

    char* pointer_end1;
    errno = 0;
    strtol((*op_type)->var_name, &pointer_end1, 10);
    if (errno == 0 && pointer_end1 != (*op_type)->var_name && *pointer_end1 == 0)
        (*op_type)->var_type = "int";
    else {
        char* pointer_end2;
        errno = 0;
        strtold((*op_type)->var_name, &pointer_end2);
        if (errno == 0 && pointer_end2 != (*op_type)->var_name && *pointer_end2 == 0)
            (*op_type)->var_type = "float32";
    }
        
    if (strcmp(expr->expr_or->op, "Or") == 0)
        printf("    %%%d = or i1 %%%d, %%%d\n", count_reg, registo1, registo2);
    count_reg++;
}

void generate_expr2(is_expr2* expr2) {

    if (expr2 == NULL)
        return;

    if (expr2->expr_and != NULL) {
        generate_expr2(expr2->next);
        int reg1 = count_reg-1;
        generate_expr3(expr2->expr3);
        int reg2 = count_reg-1; 
        check_op_expr2(expr2, reg1, reg2);
    } else {
        generate_expr3(expr2->expr3);
    }
}

void check_op_expr2(is_expr2* expr, int registo1, int registo2){

    if (expr == NULL)
        return;
        
    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    check_type_expr2(expr, func_name_gen, func_type, op_type, 0);

    char* pointer_end1;
    errno = 0;
    strtol((*op_type)->var_name, &pointer_end1, 10);
    if (errno == 0 && pointer_end1 != (*op_type)->var_name && *pointer_end1 == 0)
        (*op_type)->var_type = "int";
    else {
        char* pointer_end2;
        errno = 0;
        strtold((*op_type)->var_name, &pointer_end2);
        if (errno == 0 && pointer_end2 != (*op_type)->var_name && *pointer_end2 == 0)
            (*op_type)->var_type = "float32";
    }
        
    if (strcmp(expr->expr_and->op, "And") == 0)
        printf("    %%%d = and i1 %%%d, %%%d\n", count_reg, registo1, registo2);
    count_reg++;
}

void generate_expr3(is_expr3* expr3) {

    if (expr3 == NULL)
        return;
    
    if (expr3->expr_comp != NULL) {
        generate_expr3(expr3->next);
        int reg1 = count_reg-1;
        generate_expr4(expr3->expr4);
        int reg2 = count_reg-1;
        check_op_expr3(expr3, reg1, reg2);
    } else {
        generate_expr4(expr3->expr4);
    }
}


void check_op_expr3(is_expr3* expr3, int registo1, int registo2){

    if (expr3 == NULL)
        return;
        
    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    check_type_expr3(expr3, func_name_gen, func_type, op_type, 0);
    char* type = (*op_type)->var_type;

    if (strcmp(expr3->expr_comp->op, "Lt") == 0) {
        if (strcmp(last_type, "i32") == 0)
            printf("    %%%d = icmp slt i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        else if (strcmp(last_type, "double") == 0) {
            printf("    %%%d = fcmp olt double %%%d, %%%d\n", count_reg, registo1, registo2);
            count_reg++;
        }
    } else if (strcmp(expr3->expr_comp->op, "Gt") == 0) {
        if (strcmp(last_type, "i32") == 0)
            printf("    %%%d = icmp sgt i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        else if (strcmp(last_type, "double") == 0) {
            printf("    %%%d = fcmp ogt double %%%d, %%%d\n", count_reg, registo1, registo2);
            count_reg++;
        }
    } else if (strcmp(expr3->expr_comp->op, "Ge") == 0) {
        if (strcmp(last_type, "i32") == 0)
            printf("    %%%d = icmp sge i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        else if (strcmp(last_type, "double") == 0) {
            printf("    %%%d = fcmp oge double %%%d, %%%d\n", count_reg, registo1, registo2);
            count_reg++;
        }
    } else if (strcmp(expr3->expr_comp->op, "Le") == 0) {
        if (strcmp(last_type, "i32") == 0)
            printf("    %%%d = icmp sle i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        else if (strcmp(last_type, "double") == 0) {
            printf("    %%%d = fcmp ole double %%%d, %%%d\n", count_reg, registo1, registo2);
            count_reg++;
        }
    } else if (strcmp(expr3->expr_comp->op, "Eq") == 0) {
        if (strcmp(last_type, "i32") == 0)
            printf("    %%%d = icmp eq i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        else if (strcmp(last_type, "double") == 0) {
            printf("    %%%d = fcmp oeq double %%%d, %%%d\n", count_reg, registo1, registo2);
            count_reg++;    
        } else if (strcmp(type, "bool") == 0)
            printf("    %%%d = icmp eq i1 %%%d, %%%d\n", count_reg++, registo1, registo2);
        else if (strcmp(type, "string") == 0)
            printf("    %%%d = icmp eq i8* %%%d, %%%d\n", count_reg++, registo1, registo2);
    } else if (strcmp(expr3->expr_comp->op, "Ne") == 0) {
        if (strcmp(last_type, "i32") == 0)
            printf("    %%%d = icmp ne i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        else if (strcmp(last_type, "double") == 0) {
            printf("    %%%d = fcmp one double %%%d, %%%d\n", count_reg, registo1, registo2);
            count_reg++;
        } else if (strcmp(type, "bool") == 0)
            printf("    %%%d = icmp ne i1 %%%d, %%%d\n", count_reg++, registo1, registo2);
        else if (strcmp(type, "string") == 0)
            printf("    %%%d = icmp ne i8* %%%d, %%%d\n", count_reg++, registo1, registo2);
    }
}

void generate_expr4(is_expr4* expr4) {

    if (expr4 == NULL)
        return;

    if (expr4->expr_add_sub != NULL) {
        generate_expr4(expr4->next);
        int reg1 = count_reg-1;
        generate_expr5(expr4->expr5);
        int reg2 = count_reg-1;
        check_op_expr4(expr4,reg1,reg2);
    } else {
        generate_expr5(expr4->expr5);
    }
}

void check_op_expr4(is_expr4* expr4, int registo1, int registo2){

    if (expr4 == NULL)
        return;


    //for (; temp != NULL; temp = temp->next)
    //    check_expr5(all_symtab, temp->expr5);

    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    check_type_expr4(expr4, func_name_gen, func_type, op_type, 0);

    char* pointer_end1;
    errno = 0;
    strtol((*op_type)->var_name, &pointer_end1, 10);
    if (errno == 0 && pointer_end1 != (*op_type)->var_name && *pointer_end1 == 0)
        (*op_type)->var_type = "int";
    else {
        char* pointer_end2;
        errno = 0;
        strtold((*op_type)->var_name, &pointer_end2);
        if (errno == 0 && pointer_end2 != (*op_type)->var_name && *pointer_end2 == 0)
            (*op_type)->var_type = "float32";
    }
        
    if (strcmp(expr4->expr_add_sub->op, "Add") == 0){
        
        if(strcmp((*op_type)->var_type, "int") == 0){
            printf("    %%%d = add i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        }
        else if(strcmp((*op_type)->var_type, "float32") == 0){
            printf("    %%%d = fadd double %%%d, %%%d\n", count_reg++, registo1, registo2);
            last_type = "double";
        }
    }
    else if (strcmp(expr4->expr_add_sub->op, "Sub") == 0){
        
        if(strcmp((*op_type)->var_type,"int")==0){
            printf("    %%%d = sub i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        }
        else if(strcmp((*op_type)->var_type, "float32")==0){
            printf("    %%%d = fsub double %%%d, %%%d\n", count_reg++, registo1, registo2);
        }
    }
    
}

void generate_expr5(is_expr5* expr5) {

    if (expr5 == NULL)
        return;
    
    if (expr5->expr_mul_div != NULL) {
        generate_expr5(expr5->next);
        int reg1 = count_reg-1;
        generate_expr6(expr5->expr6);
        int reg2 = count_reg-1;
        check_op_expr5(expr5, reg1, reg2);
    } else {
        generate_expr6(expr5->expr6);
    }
}

void check_op_expr5(is_expr5* expr5, int registo1, int registo2){

    if (expr5 == NULL)
        return;

    //for (; temp != NULL; temp = temp->next)
    //    check_expr5(all_symtab, temp->expr5);

    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    check_type_expr5(expr5, func_name_gen, func_type, op_type, 1);
        

    if(strcmp(expr5->expr_mul_div->op,"Mul")==0){
        
        if(strcmp((*op_type)->var_type,"int")==0){
            printf("    %%%d = mul nsw i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        }
        else if(strcmp((*op_type)->var_type,"float32")==0){
            printf("    %%%d = fmul double %%%d, %%%d\n", count_reg++, registo1, registo2);
        }
    }
    else if (strcmp(expr5->expr_mul_div->op, "Div") == 0){
        
        if(strcmp((*op_type)->var_type,"int")==0){
            printf("    %%%d = sdiv i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        }
        else if(strcmp((*op_type)->var_type,"float32")==0){
            printf("    %%%d = fdiv double %%%d, %%%d\n", count_reg++, registo1, registo2);
        }
    }
    else if (strcmp(expr5->expr_mul_div->op, "Mod") == 0){
        if (strcmp((*op_type)->var_type,"int")==0){
            printf("    %%%d = srem i32 %%%d, %%%d\n", count_reg++, registo1, registo2);
        } else if (strcmp((*op_type)->var_type,"float32")==0){
            printf("    %%%d = frem double %%%d, %%%d\n", count_reg++, registo1, registo2);
        }
    }
    
}

void generate_expr6(is_expr6* expr6) {

    if (expr6 == NULL)
        return;

    if (expr6->expr_signal != NULL) {
        generate_expr6(expr6->next);
        check_op_expr6(expr6, count_reg-1);
    } else {
        generate_expr_aux1(expr6->expr_aux_1);
    }
}

void check_op_expr6(is_expr6* expr6, int registo1) {

    if (expr6 == NULL)
        return;

    operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
    *op_type = (operation_type*)malloc(sizeof(operation_type));

    check_type_expr6(expr6, func_name_gen, func_type, op_type, 1);

    if(strcmp(expr6->expr_signal->op, "Not")==0){
        
        if(strcmp((*op_type)->var_type,"bool")==0){
            last_type = "i1";
            printf("    %%%d = xor i1 %%%d, true\n", count_reg++, registo1);
        }
    }
    else if (strcmp(expr6->expr_signal->op, "Minus") == 0){
        
        if(strcmp((*op_type)->var_type,"int")==0){
            last_type = "i32";
            printf("    %%%d = sub i32 0, %%%d\n", count_reg++, registo1);
        }
        else if(strcmp((*op_type)->var_type,"float32")==0){
            last_type = "double";
            printf("    %%%d = fsub double 0.0, %%%d\n", count_reg++, registo1);
        }
    }  
}

void generate_expr_aux1(is_expr_aux1* exp_aux_1) {

    if (exp_aux_1 == NULL)
        return;   

    if (exp_aux_1->id != NULL) {

        operation_type** op_type = (operation_type**)malloc(sizeof(operation_type));
        *op_type = (operation_type*)malloc(sizeof(operation_type));

        check_type_expr_aux(exp_aux_1, func_name_gen, func_type, op_type, 0);

        char* pointer_end1;
        errno = 0;
        int num_i;
        if ((*op_type)->var_name[0] == '0' && ((*op_type)->var_name[1] == 'x' || (*op_type)->var_name[1] == 'X'))
            num_i = strtol((*op_type)->var_name, &pointer_end1, 16);
        else if ((*op_type)->var_name[0] == '0')
            num_i = strtol((*op_type)->var_name, &pointer_end1, 8);
        else
            num_i = strtol((*op_type)->var_name, &pointer_end1, 10);
        if (errno == 0 && pointer_end1 != (*op_type)->var_name && *pointer_end1 == 0) {
            (*op_type)->var_type = "int";
            printf("    %%%d = add i32 0, %d\n", count_reg, num_i);
            last_type = "i32";
        } else {
            char* pointer_end2;
            errno = 0;
            long double num_f = strtold((*op_type)->var_name, &pointer_end2);
            if (errno == 0 && pointer_end2 != (*op_type)->var_name && *pointer_end2 == 0) {
                (*op_type)->var_type = "float32";
                printf("    %%%d = fadd double 0.0, %0.8Lf\n", count_reg, num_f);
                last_type = "double";
            } else {
                table_element* aux = search_el_local(exp_aux_1->id, func_name_gen);

                if (aux == NULL){
                    aux = search_el(exp_aux_1->id, NULL);
                    char* type;
                    if (strcmp(aux->type, "int")==0)
                        type = "i32";
                    else if (strcmp(aux->type, "float32")==0)
                        type = "double";
                    else if (strcmp(aux->type, "bool")==0)
                        type = "i1";
                    else if (strcmp(aux->type, "string")==0)
                        type = "i8*";

                    last_type = type;
                    if (strcmp(aux->type, "string") == 0) {
                        printf("    %%%d = getelementptr [1 x i8], [1 x i8]* @%s, i32 1, i32 1\n", count_reg,aux->name->id);
                    } else
                        printf("    %%%d = load %s, %s* @%s\n", count_reg, type, type, aux->name->id);
                                  
                } else {
                    table_element* aux = search_el(exp_aux_1->id, func_name_gen);
                    char* type;
                    if (strcmp(aux->type, "int")==0)
                        type = "i32";
                    else if (strcmp(aux->type, "float32")==0)
                        type = "double";
                    else if (strcmp(aux->type, "bool")==0)
                        type = "i1";
                    else if (strcmp(aux->type, "string")==0)
                        type = "i8*";

                    last_type = type;

                    if (aux->is_param == true) {
                        int reg = search_reg_table_by_var_name(*reg_table, aux->name->id);
                        if (reg == -1) {
                            //printf("aqui\n");
                            printf("    %%%d = alloca %s\n", count_reg, type);
                            printf("    store %s %%%s, %s* %%%d\n", type, exp_aux_1->id->id, type, count_reg);
                            add_reg_table(reg_table, count_reg, aux->name->id, type);
                            count_reg++;
                            printf("    %%%d = load %s, %s* %%%d\n",count_reg, type, type, count_reg-1);
                        } else  {
                            printf("    %%%d = load %s, %s* %%%d\n",count_reg, type, type, reg);
                        }
                       
                    } else {
                        int reg = search_reg_table_by_var_name(*reg_table, exp_aux_1->id->id);
                        printf("    %%%d = load %s, %s* %%%d\n", count_reg, type, type, reg);
                    }
                    add_reg_table(reg_table, count_reg, exp_aux_1->id->id, type);
                }
            }
        }
        count_reg++;
    }
    

    generate_func_inv(exp_aux_1->funcinvoc);
    generate_expr1(exp_aux_1->expr);
}


void generate_functions() {

    printf("\n\ndeclare i32 @atoi(i8*)\n\ndeclare i32 @printf(i8*, ...)\n");
}

void generate_string_declaration() {

    printf("@print_ints = internal constant [4 x i8] c\"%%d\\0A\\00\"\n");
    printf("@print_floats = internal constant [8 x i8] c\"%%0.8lf\\0A\\00\"\n");
    printf("@print_strings = internal constant [4 x i8] c\"%%s\\0A\\00\"\n");
    printf("@print_bool_true = internal constant [6 x i8] c\"true\\0A\\00\"\n");
    printf("@print_bool_false = internal constant [7 x i8] c\"false\\0A\\00\"\n");

    if (string_array == NULL)
        return;

    strings_array* strings = *string_array;

    for (; strings; strings = strings->next) {
        
        char string_without_quotes[strlen(strings->string)];
        memcpy( string_without_quotes, &strings->string[1], strlen(strings->string)-2);
        string_without_quotes[strlen(strings->string)-2] = '\0';
        
        char* temp_string = strdup(string_without_quotes);

        bool backslash_flag = 0;
        int char_count = strlen(string_without_quotes);
        char new_string[2*char_count];
        new_string[0] = '\0';

        for (int i=0; i<strlen(string_without_quotes); i++) {
            if (temp_string[i] == '\\' && backslash_flag == 0) {
                backslash_flag = 1;
            } else if (temp_string[i] == '%') {
                strncat(new_string, "\\25\\25", 7);
                char_count++;
            } else {
                if (backslash_flag == 1) {
                    if (temp_string[i] == 't') {
                        strncat(new_string, "\\09", 4);
                    } else if (temp_string[i] == 'n') {
                        strncat(new_string, "\\0A", 4);
                    } else if (temp_string[i] == 'f') {
                        strncat(new_string, "\\0C", 4);
                    } else if (temp_string[i] == 'r') {
                        strncat(new_string, "\\0D", 4);
                    } else if (temp_string[i] == '\\') {
                        strncat(new_string, "\\5C", 4);
                    } else if (temp_string[i] == '\"') {
                        strncat(new_string, "\\22", 4);
                    }
                    char_count--;
                    backslash_flag = 0;
                } else
                    strncat(new_string, &temp_string[i], 1);
            }
        }
        strncat(new_string, "\\0A\\00\0", 7);
        char_count+=2;

        insert_string_list(str_tam, count_str, char_count, strings->string);

        printf("@.str.%d = internal constant [%i x i8] c\"%s\"\n", count_str++,
            char_count, new_string);
    }
}
